# README

This README contains the minimal information on the analysis pipeline for the paper entitled "Predicting clinical trajectories of major depressive disorder with generative embedding" published in NeuroImage:Clinical. The repository is meant to provide all code utilized for data analysis.

# Contributors / Roles
|                               |                                             |
| ----------------------------- | ------------------------------------------- |
| Project lead / analysis:      | Stefan Frässle (PhD)                        |
| Supervising Prof.:            | Prof. Klaas Enno Stephan (MD Dr. med., PhD) |
| Abbreviation:                 | NESDA                                       |
| Date:                         | March 10, 2020                              |

# Summary
This project uses generative embedding to classify different naturalistic clinical trajectories in major depressive disorder (MDD) patients from the NEtherlands Study of Depression and Anxiety (NESDA). Specifically, dynamic causal modeling (DCM) was used to infer effective (directed) connectivity among neuronal populations from functional magnetic resonance imaging (fMRI) data. DCM parameters were then used to span a generative score space and a linear support vector machine (SVM) was trained to distinguish the different clinical trajectories. Classification accuracy (balanced accuracy) was assessed using a nested leave-one-out cross validation where, in the inner loop, hyperparameters of the SVM were optimized. This repository contains all the necessary code to reproduce the results reported in the accompanying paper.

The project was conducted at the Translational Neuromodeling Unit (TNU) by the project lead (SF), in collaboration with the NESDA consortium.

**Note:** The code in this repository will not run out-of-the-box because no data is provided along with the scripts. This is because access to the NESDA dataset has to be obtained via the standard NESDA data access procedure (see https://www.nesda.nl/nesda-english/).

# Reference
Frässle S, Marquand AF, Schmaal L, Dinga R, Veltman DJ, van der Wee NJA, van Tol MJ, Schöbi S, Penninx BWJH, Stephan KE. Predicting clinical trajectories of major depressive disorder with generative embedding. *NeuroImage:Clinical* 26, 102213.

# Requirements
Software requirements of the project are MATLAB, Statistical Parametric Mapping (SPM12), several MATLAB toolboxes

