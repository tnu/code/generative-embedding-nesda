function estimate_FC_MainExperiment(subject,experiment,SPM_prep,include_DLPFC,basis_set)
% Computes functional connectivity measures among the extracted BOLD signal 
% time series, as a comparison for the DCM (effective connectivity)
% estimates.
% 
% Input:
%   subject      	-- subject to be analyzed
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   SPM_prep        -- (0) NESDA pipeline (1) SPM preprocessing
%   include_DLPFC   -- (0) only face emotion network, (1) include DLPFC
%   basis_set       -- basis set: (0) only HRF, (1) temporal derivative, (2) informed basis set
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2017
% Copyright 2017 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
exp_folders = {'FACES/','ToL/'};

% set the preprocessing folder
SPM_folder          = {'','SPM_preprocessing/'};
DLPFC_name          = {'','_DLPFC'};
basis_folder        = {'HRF','TD','IBS',''};


% set the path to the experiment
addpath(FilenameInfo.SPM_path)
foldername = fullfile(FilenameInfo.DataPath,exp_folders{experiment});


% set the default setting for including the DLPFC node in the DCMs
if ( isempty(include_DLPFC) && experiment == 1 )
    include_DLPFC = 0;
end

% get the subject list
Subject_List = [dir(fullfile(foldername,'1*')); dir(fullfile(foldername,'2*')); dir(fullfile(foldername,'3*'))];


% specify the subjects
if ( isempty(subject) )
    subject_analyze = 1:length(Subject_List);
else
    subject_analyze = subject;
end

% display progress
disp('Compute functional connectivity -----------------')

% iterate over subjects
for subject = subject_analyze

    % subject name
    Subject = Subject_List(subject).name;

    % display progress
    disp(['-- Subject ' Subject])

    % define the folder where the SPM.mat (DCM regressors) is stored
    FC_foldername = fullfile(foldername,Subject,'FirstLevel_FC');

    % define the folder where the VOIs are stored
    VOI_foldername = fullfile(foldername,Subject,'FirstLevel',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'timeseries_VOI_adjusted','NeuroSynth');

    % get the filenames of VOIs
    if ( include_DLPFC )
        VOI_files = [dir(fullfile(VOI_foldername,'VOI_DLPFC*t_local_1.mat')); dir(fullfile(VOI_foldername,'VOI_Amyg*t_local_1.mat')); dir(fullfile(VOI_foldername,'VOI_FFA*t_local_1.mat')); dir(fullfile(VOI_foldername,'VOI_OFA*t_local_1.mat'))];
    else
        VOI_files = [dir(fullfile(VOI_foldername,'VOI_Amyg*t_local_1.mat')); dir(fullfile(VOI_foldername,'VOI_FFA*t_local_1.mat')); dir(fullfile(VOI_foldername,'VOI_OFA*t_local_1.mat'))];
    end

    % compute the functional connectivity measures
    if ( ~isempty(VOI_files) )

        % load the VOI time series for all regions of interest
        for number_of_regions = 1:length(VOI_files)

            % load the files
            temp = load(fullfile(VOI_foldername,VOI_files(number_of_regions).name),'Y');

            % asign the data to a matrix
            if ( number_of_regions == 1 ) 
                Y = temp.Y;
            else
                Y(:,end+1) = temp.Y;
            end
        end

        % compute the cross-correlation (Pearson correlation coefficient)
        cross_corr = NaN(size(Y,2),size(Y,2));
        for int = 1:size(Y,2)
            for int2 = 1:size(Y,2)
                [r,~] = corrcoef(Y(:,int),Y(:,int2));
                cross_corr(int,int2) = r(1,2);
            end
        end

        % compute the cross-correlation (Pearson correlation coefficient)
        partial_corr = NaN(size(Y,2),size(Y,2));
        vector = 1:size(Y,2);
        for int = 1:size(Y,2)
            for int2 = 1:size(Y,2)
                r = partialcorr(Y(:,int),Y(:,int2),Y(:,vector(vector~=int & vector~=int2)));
                partial_corr(int,int2) = r;
            end
        end


        % save the results
        FC_measures.cross_corr   = cross_corr;
        FC_measures.partial_corr = partial_corr;

        
        % specify the results array for all subjects
        if ( subject == 1 )
            cross_corr_AllSubjects   = cell(size(cross_corr));
            partial_corr_AllSubjects = cell(size(partial_corr));
        end
        
        
        % asign the a values for the endogenous parameters in each subject
        for int = 1:size(cross_corr,1)
            for int2 = 1:size(cross_corr,2)
                cross_corr_AllSubjects{int,int2}    = [cross_corr_AllSubjects{int,int2}; cross_corr(int,int2)];
                partial_corr_AllSubjects{int,int2}  = [partial_corr_AllSubjects{int,int2}; partial_corr(int,int2)];
            end 
        end
        

        % create the foldername
        if ( ~exist(fullfile(FC_foldername,SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth'),'dir') )
            mkdir(fullfile(FC_foldername,SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth'))
        end

        % save the DCM
        if  ( ~isempty(FC_measures) )
            save(fullfile(FC_foldername,SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth',['FC_measures' DLPFC_name{include_DLPFC+1} '_local.mat']),'FC_measures');
        end
        
    else
        
        for int = 1:size(cross_corr_AllSubjects,1)
            for int2 = 1:size(cross_corr_AllSubjects,2)
                cross_corr_AllSubjects{int,int2}    = [cross_corr_AllSubjects{int,int2}; NaN];
                partial_corr_AllSubjects{int,int2}  = [partial_corr_AllSubjects{int,int2}; NaN];
            end 
        end
    end
    
    % asign the subject name
    AllSubjects{subject} = Subject;
end

% asign the results
FC_measures.cross_corr_AllSubjects   = cross_corr_AllSubjects;
FC_measures.partial_corr_AllSubjects = partial_corr_AllSubjects;
FC_measures.AllSubjects              = AllSubjects;


% save the results of the particular model
if ( ~exist(fullfile(foldername,'FC_GroupAnalysis',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal'),'dir') )
    mkdir(fullfile(foldername,'FC_GroupAnalysis',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal'))
end
save(fullfile(foldername,'FC_GroupAnalysis',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',['FC_measures' DLPFC_name{include_DLPFC+1} '_local.mat']),'FC_measures')

end