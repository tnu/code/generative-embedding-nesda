function evaluate_importance_weights_SVM_MainExperiment(FilenameInfo,model,experiment,version,disp_width,disp_perm,rotateText)
% Plots the feature weights (after transformation to patterns based on 
% Haufe et al (2014), Neuroimage, in order to assess their relevance for 
% classifcation between the different patient subgroups - namely fast 
% remission, gradualy improving, and chronic. The script uses support 
% vector machines (SVM).
%
% Input:
%   FilenameInfo    -- important definitions for the experiment
%   model           -- model to be analyzed
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   version         -- optional
%   disp_width      -- display the width of the lines for the Supp Fig
%   disp_perm       -- display the results from the permutation tests
%   rotateText      -- figure setting: (0) ps summary, (1) paper figure
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2016
% Copyright 2016 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% close all figures
close all


% load the paths if necessary
if ( isempty(FilenameInfo) )
    
    % get the path
    m_path = mfilename('fullpath');
    m_path = m_path(1:find(m_path=='/',1,'last'));

    try
        load(fullfile(m_path,'ConfigFile.mat'))
    catch err
        disp('Need to specify FilenameInfo containing paths!')
        rethrow(err)
    end
end


% default settings
SPM_prep        = 1;
include_DLPFC   = 0;
basis_set       = 2;


% how to compute "average" weight vector
mode = 1;

% set the experiment
exp_folder = {'FACES/','ToL/'};

% set folders
SPM_folder          = {'','SPM_preprocessing/'};
DLPFC_name          = {'','_DLPFC'};
basis_folder        = {'HRF','TD','IBS',''};
settings_folder    	= {'woDLPFC','wDLPFC'};

% add path
addpath(genpath(fullfile(FilenameInfo.MATLAB_path,'PBAR')))

% set the path of the experiment
exp_foldername	 = FilenameInfo.DataPath;

% set the SPM path and the path to the experiment
foldername = [exp_foldername exp_folder{experiment}];


% define the name of the different models
name = {'BMA','model_FF_INTER','model_FB_INTER','model_RC_INTER','model_FF_noINTER',...
    'model_FB_noINTER','model_RC_noINTER','model_noMOD'};


% specify the model folder name
if ( isempty(model) )
    model_folder  = 'Bayesian_model_selection_RandRest';
elseif ( model == 0 )
    model_number  = 0; 
    model_folder  = 'Bayesian_model_averaging_RandRest_single';
else
    model_number  = model;
    model_folder  = 'IndividualModel_RandRest';
end

% names of the group comparisons
group_names = {'CHR_REM','CHR_IMP','IMP_REM'};


% load the results of the generative embedding
if ( ~isempty(model) )
    Perm = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding_Permutation' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local.mat']));
else
    Perm = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding_PermutationBMS' DLPFC_name{include_DLPFC+1} '_local.mat']));
end

% load the actual classifcation results
if ( ~isempty(model) )
    Effect = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local.mat']));
else
    Effect = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbeddingBMS' DLPFC_name{include_DLPFC+1} '_local.mat']));
end


% define the results arrays
WeightsPerm         = cell(size(Perm.results,1),1);
Weights             = cell(size(Perm.results,1),1);
Weights_all         = cell(size(Perm.results,1),1);
p_val_NonParam      = cell(size(Perm.results,1),1);


% check if figure folder exists
if ( ~exist(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local']),'dir') )
    mkdir(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local']))
else
    
    % delete previous plots of necessary
    if ( exist(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_FeatureWeights.ps'),'file') )
        delete(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_FeatureWeights.ps'))
    end
    
    % delete previous plots of necessary
    if ( exist(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_FeatureWeights_AllFolds.ps'),'file') )
        delete(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_FeatureWeights_AllFolds.ps'))
    end
    
    % delete previous plots of necessary
    if ( exist(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'3D_DiscriminativeFeatures.ps'),'file') )
        delete(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'3D_DiscriminativeFeatures.ps'))
    end
    
end


% get the weights from the permutations
for group_comp = 1:size(Perm.results,1)
    for Nr_perm = 1:size(Perm.results,2)
        
        % weights of random permutations
        try
            if ( mode == 1 )
                WeightsPerm{group_comp}(Nr_perm,:) = mean(Perm.results(group_comp,Nr_perm).class.patterns);
            elseif ( mode == 2 )
                WeightsPerm{group_comp}(Nr_perm,:) = sum(abs(Perm.results(group_comp,Nr_perm).class.patterns));
            end
        catch
            WeightsPerm{group_comp}(Nr_perm,:) = NaN(1,size(Perm.results(1,1).class.patterns,2));
        end
    end
end


% obtain actual weights and comupte p-values
for group_comp = 1:size(Effect.results,2)
    
    % actual weights
    if ( mode == 1 )
        Weights{group_comp} = mean(Effect.results(group_comp).class.patterns);
    elseif ( mode == 2 )
        Weights{group_comp} = sum(abs(Effect.results(group_comp).class.patterns));
    end
    
    % evaluate the non-parameteric p-values
    for param = 1:length(Weights{group_comp})
        p_val_NonParam{group_comp}(param,1) = sum(WeightsPerm{group_comp}(:,param) > Weights{group_comp}(param))/size(WeightsPerm{group_comp},1);
    end
    
    % evaluate the displaying patterns (weights)
    Weights{group_comp} = abs(Weights{group_comp});
    
    % evaluate the patterns (weights) for each fold
    Weights_all{group_comp} = abs(Effect.results(group_comp).class.patterns);
    
    
    % specify colors
    color_code = [ones(20,1); 2*ones(14,1); 3*ones(14,1); 4*ones(14,1); 5*ones(14,1); 6*ones(2,1)];
    color_type = lines;%{[0.1 0.1 0.1],[0 0 1],[0 0.1 0.8],[0 0.1 0.6],[0 0.1 0.4],[0.5 0.5 0.5]};
    
    % plot the feature weights
    figure('units','normalized','outerposition',[0 0 1 1])
    s = Weights{group_comp};
    hold on
    col = [192 0 0]/255;
    barh(s, 'facecolor', col, 'edgecolor', 'none');
    axis ij;
    set(gca, 'box', 'off');
    set(gca, 'YTickLabel', Perm.name_connection);
    set(gca, 'YTick', 1:length(s));
    xlabel('z score')
    set(gca,'fontsize',8);
    title(strrep(group_names{group_comp},'_','\_'))
    p = get(gca,'position');
    p(3) = 0.2;
    set(gca,'position',p);
    h = gcf;
    set(h,'PaperOrientation','landscape');
    print(gcf, '-dpsc', fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_FeatureWeights'), '-fillpage', '-append')
    
    % larger font size on local machine (for creating figures for paper)
    add_font_size = [0 7];
    
    % plot the feature weights
    figure('units','normalized','outerposition',[0 0 1 1])
    theta = linspace(0,2*pi,length(s)+1);
    rho = [s s(1)];
    polarplot(theta,rho,'Color',col,'LineWidth',1)
    h = gcf;
    angles = linspace(0,360,length(s)+1);
    h.CurrentAxes.ThetaTick = angles(1:end-1);
    h.CurrentAxes.ThetaTickLabel = {''};
    for int = 1:length(s)
        index = find(Perm.name_connection{int} == '('); if ( isempty(index) ), index = length(Perm.name_connection{int})+1; end
        htext = text(theta(int),h.CurrentAxes.RLim(2)*1.01,Perm.name_connection{int}(1:index-1),'FontSize',8+add_font_size(rotateText+1),'Color',color_type(color_code(int),:));
        if ( rotateText == 0 || rad2deg(theta(int)) < 90 || rad2deg(theta(int)) > 270 )
            set(htext,'Rotation',rad2deg(theta(int)))
        else
            text_length = [38 38 38];
            set(htext,'Rotation',rad2deg(theta(int))+180)
            set(htext,'Position',[theta(int) h.CurrentAxes.RLim(2)*1.01+text_length(group_comp) 0])
        end
    end
    
    % write the parameter classes
    angle_temp = theta([10 29 39 54 68 77]);
    class_text = {'Endogenous','Modulatory (happy)','Modulatory (angry)','Modulatory (fear)','Modulatory (sad)','Driving'};
    for int = 1:6
        htext = text(angle_temp(int),h.CurrentAxes.RLim(2)+h.CurrentAxes.RLim(2)/4,class_text{int},'FontSize',10+add_font_size(rotateText+1),'Color',color_type(int,:));
        if int < 3
            set(htext,'Rotation',rad2deg(angle_temp(int))-95)
        else
            set(htext,'Rotation',rad2deg(angle_temp(int))+95)
        end
    end
    
    % write the group comparison
    text(theta(33),h.CurrentAxes.RLim(2)*1.8,strrep(group_names{group_comp},'_','\_'),'FontSize',12)
    
    set(h,'PaperOrientation','landscape');
    print(gcf, '-dpsc', fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_FeatureWeights'), '-fillpage', '-append')
    
    % get high connections
    high_connection(:,group_comp) = s > mean(s);
    
    
    % get the line widths
    if ( disp_width == 1 )
    
        % output the line widths for the supplementary plot
        mult_factor = max(s)/30;

        % output line widths
        for int = 1:length(Perm.name_connection)
            fprintf('%s: %f \n',Perm.name_connection{int},round(s(int)/mult_factor,2))
        end
        
    end
    
    
%     % plot the feature patterns (weights) for each fold
%     for nrFold = 1:size(Weights_all{group_comp},1)
%         
%         % get the current fold
%         s = Weights_all{group_comp}(nrFold,:);
%         
%         % plot the feature pattern
%         figure('units','normalized','outerposition',[0 0 1 1])
%         theta = linspace(0,2*pi,length(s)+1);
%         rho = [s s(1)];
%         polarplot(theta,rho,'Color',col,'LineWidth',1)
%         h = gcf;
%         angles = linspace(0,360,length(s)+1);
%         h.CurrentAxes.ThetaTick = angles(1:end-1);
%         h.CurrentAxes.ThetaTickLabel = {''};
%         for int = 1:length(s)
%             index = find(Perm.name_connection{int} == '('); if ( isempty(index) ), index = length(Perm.name_connection{int})+1; end
%             htext = text(theta(int),h.CurrentAxes.RLim(2)*1.01,Perm.name_connection{int}(1:index-1),'FontSize',8+add_font_size(rotateText+1),'Color',color_type(color_code(int),:));
%             if ( rotateText == 0 || rad2deg(theta(int)) < 90 || rad2deg(theta(int)) > 270 )
%                 set(htext,'Rotation',rad2deg(theta(int)))
%             else
%                 text_length = [38 38 38];
%                 set(htext,'Rotation',rad2deg(theta(int))+180)
%                 set(htext,'Position',[theta(int) h.CurrentAxes.RLim(2)*1.01+text_length(group_comp) 0])
%             end
%         end
% 
%         % write the parameter classes
%         angle_temp = theta([10 29 39 54 68 77]);
%         class_text = {'Endogenous','Modulatory (happy)','Modulatory (angry)','Modulatory (fear)','Modulatory (sad)','Driving'};
%         for int = 1:6
%             htext = text(angle_temp(int),h.CurrentAxes.RLim(2)+h.CurrentAxes.RLim(2)/4,class_text{int},'FontSize',10+add_font_size(rotateText+1),'Color',color_type(int,:));
%             if int < 3
%                 set(htext,'Rotation',rad2deg(angle_temp(int))-95)
%             else
%                 set(htext,'Rotation',rad2deg(angle_temp(int))+95)
%             end
%         end
% 
%         % write the group comparison
%         text(theta(33),h.CurrentAxes.RLim(2)*1.8,strrep(group_names{group_comp},'_','\_'),'FontSize',12)
% 
%         set(h,'PaperOrientation','landscape');
%         print(gcf, '-dpsc', fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_FeatureWeights_AllFolds'), '-fillpage', '-append')
%         close(h)
%     end
    
    % open figure
    figure('units','normalized','outerposition',[0 0 1 1])
    
    % plot the feature patterns (weights) for each fold
    for nrFold = 1:size(Weights_all{group_comp},1)
        
        % get the current fold
        s = Weights_all{group_comp}(nrFold,:);
        s = s/max(s);
        
        % plot the feature pattern
        theta = linspace(0,2*pi,length(s)+1);
        rho = [s s(1)];
        polarplot(theta,rho,'Color',[0.6 0.6 0.6],'LineWidth',0.2)
        hold on
        
    end
    
    h = gcf;
    angles = linspace(0,360,length(s)+1);
    h.CurrentAxes.ThetaTick = angles(1:end-1);
    h.CurrentAxes.ThetaTickLabel = {''};
    for int = 1:length(s)
        index = find(Perm.name_connection{int} == '('); if ( isempty(index) ), index = length(Perm.name_connection{int})+1; end
        htext = text(theta(int),h.CurrentAxes.RLim(2)*1.01,Perm.name_connection{int}(1:index-1),'FontSize',8+add_font_size(rotateText+1),'Color',color_type(color_code(int),:));
        if ( rotateText == 0 || rad2deg(theta(int)) < 90 || rad2deg(theta(int)) > 270 )
            set(htext,'Rotation',rad2deg(theta(int)))
        else
            text_length = [38 38 38];
            set(htext,'Rotation',rad2deg(theta(int))+180)
            set(htext,'Position',[theta(int) h.CurrentAxes.RLim(2)*1.01+text_length(group_comp) 0])
        end
    end

    % write the parameter classes
    angle_temp = theta([10 29 39 54 68 77]);
    class_text = {'Endogenous','Modulatory (happy)','Modulatory (angry)','Modulatory (fear)','Modulatory (sad)','Driving'};
    for int = 1:6
        htext = text(angle_temp(int),h.CurrentAxes.RLim(2)+h.CurrentAxes.RLim(2)/4,class_text{int},'FontSize',10+add_font_size(rotateText+1),'Color',color_type(int,:));
        if int < 3
            set(htext,'Rotation',rad2deg(angle_temp(int))-95)
        else
            set(htext,'Rotation',rad2deg(angle_temp(int))+95)
        end
    end

    % write the group comparison
    text(theta(33),h.CurrentAxes.RLim(2)*1.8,strrep(group_names{group_comp},'_','\_'),'FontSize',12)
    
    set(h,'PaperOrientation','landscape');
    print(gcf, '-dpsc', fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_FeatureWeights'), '-fillpage', '-append')
    
end


% plot the consistency of parameters across the two analysis
figure('units','normalized','outerposition',[0 0 1 1])
c = sum(high_connection(:,[1 3]),2);
hold on
col = [192 0 0]/255;
barh(c, 'facecolor', col, 'edgecolor', 'none');
axis ij;
set(gca, 'box', 'off');
set(gca, 'YTickLabel', Perm.name_connection);
set(gca, 'YTick', 1:length(c));
xlabel('# of occurences')
set(gca,'fontsize',8);
title('Consistency of features')
p = get(gca,'position');
p(3) = 0.2;
set(gca,'position',p);
h = gcf;
set(h,'PaperOrientation','landscape');
print(gcf, '-dpsc', fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_FeatureWeights'), '-fillpage', '-append')


% collated feature scores across valences (most important connections)
temp    = reshape(c(21:76),14,4);
cc      = sum(temp,2);
name_cc = Perm.name_connection(21:34);
for int = 1:length(name_cc), name_cc{int} = [name_cc{int}(1:end-6) 'all)']; end


% plot the consistency of parameters collated across valences
figure('units','normalized','outerposition',[0 0 1 1])
hold on
col = [192 0 0]/255;
barh(cc, 'facecolor', col, 'edgecolor', 'none');
axis ij;
set(gca, 'box', 'off');
set(gca, 'YTickLabel', name_cc);
set(gca, 'YTick', 1:length(name_cc));
xlabel('# of occurences')
set(gca,'fontsize',8);
title('Consistency of features')
p = get(gca,'position');
p(3) = 0.2;
set(gca,'position',p);
h = gcf;
set(h,'PaperOrientation','landscape');
print(gcf, '-dpsc', fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_FeatureWeights'), '-fillpage', '-append')


% output the features with the highest weights
for group_comp = 1:size(p_val_NonParam,1)
    
    % find the top weights
    [~,weights_indices] = sort(Weights{group_comp},'descend');
    
    % test dummy
    text_name = repmat('%s | ',1,10);
    text_name = text_name(1:end-3);
    
    % output
    fprintf(['\nGroup comparison: ' group_names{group_comp} '\n'])
    fprintf(['Important weights (Top 10): ' text_name '\n'],Perm.name_connection{weights_indices(1:10)})
    
end


% display results of permutation test
if ( disp_perm )

    % signficance threshold (marginally significant)
    p_val = 0.05;

    % output the significant and marginally significant effects
    for group_comp = 1:size(p_val_NonParam,1)

        % number of marginally significant effects
        NR_margSig = numel(find(p_val_NonParam{group_comp}<p_val));

        text_name = repmat('%s | ',1,NR_margSig);
        text_name = text_name(1:end-3);

        % output
        fprintf(['\nGroup comparison: ' group_names{group_comp} '\n'])
        fprintf(['Important weights (sig.): ' text_name '\n'],Perm.name_connection{find(p_val_NonParam{group_comp}<p_val)})

    end
end

end