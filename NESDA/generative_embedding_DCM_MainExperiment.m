function generative_embedding_DCM_MainExperiment(FilenameInfo,model,experiment,SPM_prep,classifier,include_DLPFC,basis_set,regress_covariates,version,plot_figure)
% Utilize the posterior parameter estimates (i.e., maximum-a-posteriori) as
% features for classifying between the different patient subgroups - namely
% fast remission, gradualy improving, and chronic. The script either uses
% support vector machines (SVM) in accordance with the classical approach
% used by Kay Brodersen, or it uses Gaussian Process Regression (GPC) to be
% consistent with the paper by Lianne.
%
% Input:
%   FilenameInfo        -- important definitions for the experiment
%   model               -- model to be analyzed
%   experiment          -- (1) FACES dataset, (2) Tower of London dataset
%   SPM_prep            -- (0) NESDA pipeline (1) SPM preprocessing
%   classifier          -- classifier: (1) GPC, (2) SVM
%   include_DLPFC       -- (0) not include DLPFC, (1) include DLPFC
%   basis_set           -- basis set: (0) only HRF, (1) temporal derivative, (2) informed basis set
%   regress_covariate	-- regress out covariates (default: 0)
%   version             -- version number (here: set to [])
%   plot_figure         -- show figures
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% if FilenameInfo not specified, try to load
if ( isempty(FilenameInfo) )
    
    % get the path
    m_path = mfilename('fullpath');
    m_path = m_path(1:find(m_path=='/',1,'last'));
    
    try
        load(fullfile(m_path,'ConfigFile.mat'))
    catch err
        disp('Need to specify FilenameInfo containing paths!')
        rethrow(err)
    end
end


% check version
if ( ~isempty(version) )
    version = [];
end

% set the experiment
exp_folder = {'FACES/','ToL/'};

% set folders
SPM_folder          = {'','SPM_preprocessing/'};
SPM_folder_text     = {'',' / SPMprep'};
DLPFC_name          = {'','_DLPFC'};
settings_folder    	= {'woDLPFC','wDLPFC'};
regress_name        = {'','_orth'};
basis_folder        = {'HRF','TD','IBS',''};

% define the SPM version
classifier_name  = {'GPC','SVM','FITCSVM'};

% add path
addpath(genpath(fullfile(FilenameInfo.MATLAB_path,'PBAR')))

% add path for classifiers
if ( classifier == 1 )
    addpath(genpath(fullfile(FilenameInfo.MATLAB_path,'PRoNTo')))
elseif ( classifier == 2 )
    addpath(genpath(fullfile(FilenameInfo.MATLAB_path,'liblinear-2.11')))
end

% set the path of the experiment
exp_foldername	 = FilenameInfo.Exp_path;

% set the SPM path and the path to the experiment
foldername = [FilenameInfo.DataPath exp_folder{experiment}];


% define the name of the different models
name = {'BMA','model_FF_INTER','model_FB_INTER','model_RC_INTER','model_FF_noINTER',...
    'model_FB_noINTER','model_RC_noINTER','model_noMOD'};


% define the different groups
group_names = {'CHR_REM','CHR_IMP','IMP_REM'};
group_comb  = {[3 1],[3 2],[2 1]};

% specify the model folder name
if ( isempty(model) )
    model_folder  = 'Bayesian_model_selection_RandRest';
elseif ( model == 0 )
    model_number  = 0; 
    model_folder  = 'Bayesian_model_averaging_RandRest_single';
else
    model_number  = model;
    model_folder  = 'IndividualModel_RandRest';
end

% get the classes of the subjects
temp                = load([exp_foldername 'LCGA_classes/LCGA_classes.mat']);
LCGA_classes        = temp.LCGA_classes;
AllSubjects_Schmaal = xlsread([exp_foldername 'information/AllSubjects_Faces.xlsx']);

% results folder
results_folder = 'DCM_GroupAnalysis';

% load the posterior parameter estimates
if ( ~isempty(model) && model_number ~= 0 )
    temp = load(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['PosteriorParameterEstimates_' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local.mat']));
elseif ( isempty(model) )
    file = dir(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,[settings_folder{include_DLPFC+1} '_local'],'PosteriorParameterEstimates*'));
    temp    = load(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,[settings_folder{include_DLPFC+1} '_local'],file(1).name));
    pxp_max = temp.results.pxp_max;
else
    temp = load(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,[settings_folder{include_DLPFC+1} '_local'],'BMS.mat'));
    
    % bring the results in the same format
    for s = 1:length(temp.BMS.DCM.ffx.bma.mEps)
        
        % endogenous connections
        for int = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.A,1)
            for int2 = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.A,2)
                temp.results.A_Matrix_AllSubjects{int,int2}(s) = temp.BMS.DCM.ffx.bma.mEps{s}.A(int,int2);
            end
        end
        
        % modulatory influences
        for int = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.B,1)
            for int2 = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.B,2)
                for int3 = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.B,3)
                    temp.results.B_Matrix_AllSubjects{int,int2,int3}(s) = temp.BMS.DCM.ffx.bma.mEps{s}.B(int,int2,int3);
                end
            end
        end
        
        % driving inputs
        for int = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.C,1)
            for int2 = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.C,2)
                temp.results.C_Matrix_AllSubjects{int,int2}(s) = temp.BMS.DCM.ffx.bma.mEps{s}.C(int,int2);
            end
        end
    end
    
    % asign the subjects included
    temp.results.AllSubjects = temp.BMS.DCM.ffx.AllSubjects;
    
end

% load the functional connectivity measures (if available)
if ( exist(fullfile(foldername,'FC_GroupAnalysis',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',['FC_measures' DLPFC_name{include_DLPFC+1} '_local.mat']),'file') )
    temp2 = load(fullfile(foldername,'FC_GroupAnalysis',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',['FC_measures' DLPFC_name{include_DLPFC+1} '_local.mat']));
end

% load the contrast image measures (if available)
if ( exist(fullfile(foldername, 'CI_GroupAnalysis/',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',['CI_measures' DLPFC_name{include_DLPFC+1} '_local.mat']),'file') )
    temp3  = load(fullfile(foldername, 'CI_GroupAnalysis/',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',['CI_measures' DLPFC_name{include_DLPFC+1} '_local.mat']));
end


% asign the data
Amatrix_allSubjects     = temp.results.A_Matrix_AllSubjects;
Bmatrix_allSubjects     = temp.results.B_Matrix_AllSubjects;
Cmatrix_allSubjects     = temp.results.C_Matrix_AllSubjects;
FC_allSubjects          = temp2.FC_measures.cross_corr_AllSubjects;
CI_allSubjects          = [temp3.data_CI{1}, temp3.data_CI{2}, temp3.data_CI{3}, temp3.data_CI{4}, temp3.data_CI{5}, temp3.data_CI{6}];
p_values                = NaN(1,3);
p_values2               = NaN(1,3);


% define a labels array
labels = zeros(1,length(temp.results.AllSubjects));

% dummy number of subjects
nr_subject = [];
found_subjects = [];

for int = 1:length(temp.results.AllSubjects)

    % find the index of the subject in the LCGA classes
    index_LCGA = find(LCGA_classes(:,1) == str2double(temp.results.AllSubjects{int}),1);

    if ( ~isempty(index_LCGA) )
        
        % get subjects that were also included in Schmal et al.
        index = find(AllSubjects_Schmaal == str2double(temp.results.AllSubjects{int}),1);
        
        if ( ~isempty(index) )
            if ( ~isempty(index_LCGA) && isfinite(Amatrix_allSubjects{1,1}(int)) )
                nr_subject(end+1)     = LCGA_classes(index_LCGA,1);
                found_subjects(end+1) = str2double(temp.results.AllSubjects{int});
            end

            % if the subject exists, assign the class label
            if ( isfinite(Amatrix_allSubjects{1,1}(int)) )
                labels(int) = LCGA_classes(index_LCGA,3);
            end
        end
    end
end

% find the index of the included subjects
initial_subjects = str2double(temp2.FC_measures.AllSubjects);
index_sub = NaN(length(found_subjects),1);
for int = 1:length(found_subjects)
    index_sub(int) = find(initial_subjects==found_subjects(int));
end

% mark included subjects
vector(index_sub,1) = 1;

% convert to logical
vector = vector == 1;


% restrict the FC and CI cell arrays to included subjects (only for BMA)
if ( model == 0 )
    
    % adjust A parameters
    for int = 1:size(FC_allSubjects,1)
        for int2 = 1:size(FC_allSubjects,2)
            FC_allSubjects{int,int2} = FC_allSubjects{int,int2}(vector);
        end
    end
    
    % adjust contrast images
    CI_allSubjects = CI_allSubjects(vector,:);
    
end


% display how many subjects were found
fprintf('\n')
disp(['Found: ' num2str(length(nr_subject)) ' out of ' num2str(length(AllSubjects_Schmaal)) ' subjects - Running: ' classifier_name{classifier} ' / ' basis_folder{basis_set+1}])

% display subject name of missing subject
if ( length(nr_subject) ~= length(AllSubjects_Schmaal) )
    missing_subjects = setdiff(AllSubjects_Schmaal,found_subjects);
    fprintf((~isempty(missing_subjects))+1,'Subjects missing:');
    for int = 1:length(missing_subjects)
        fprintf((~isempty(missing_subjects))+1,' %d',missing_subjects(int));
    end
    fprintf('\n')
end

% define labels cell
labels_all = cell(3,1);

% close all figures
close all

% open new figures
if ( plot_figure )
    figure(1)
    figure(2)
    figure(3)
end


% specify the true labels for a binary comparison of groups
for group_comp = 1:3
    
    % clear the data
    clear data
    
    % define all labels
    all_labels = 1:3;
    not_included = setdiff(all_labels,group_comb{group_comp});
    
    % define temporary label
    labels_temp = labels;
    labels_temp(labels_temp==not_included) = 0;
    vector_2 = labels_temp==group_comb{group_comp}(2);
    vector_1 = labels_temp==group_comb{group_comp}(1);
    labels_temp(vector_2) = 2;
    labels_temp(vector_1) = 1;
    
    % define the specifiy labels
    labels_group = labels_temp(labels_temp~=0);
    [labels_group, order] = sort(labels_group);
    labels_group = labels_group';

    % define the subjects that should be included
    subjects_analyze = find(labels_temp~=0);
    subjects_analyze = subjects_analyze(order);
    
    % get the subject names
    subjects_group_temp = temp.results.AllSubjects(subjects_analyze);
    subjects_group      = NaN(size(subjects_group_temp));
    for int = 1:length(subjects_group_temp)
        subjects_group(int) = str2double(subjects_group_temp{int});
    end

    % display the setting
    disp(['Group selection: ' group_names{group_comp} ' - Group size: ' num2str(sum(labels_temp==1)) ' vs. ' num2str(sum(labels_temp==2))])

    % define the counter
    subject_counter = 0;

    % get the parameters
    for s = subjects_analyze

        % increase the subject counter
        subject_counter = subject_counter + 1;

        % initialize the temporary arrays
        a_temp = NaN(size(Amatrix_allSubjects));
        b_temp = NaN(size(Bmatrix_allSubjects));
        c_temp = NaN(size(Cmatrix_allSubjects));

        % get endogenous parameters
        for int = 1:size(Amatrix_allSubjects,1)
            for int2 = 1:size(Amatrix_allSubjects,2)
                a_temp(int,int2) = Amatrix_allSubjects{int,int2}(s);
            end
        end

        % get modulatory parameters
        for int = 1:size(Bmatrix_allSubjects,1)
            for int2 = 1:size(Bmatrix_allSubjects,2)
                for int3 = 1:size(Bmatrix_allSubjects,3)
                    b_temp(int,int2,int3) = Bmatrix_allSubjects{int,int2,int3}(s);
                end
            end
        end

        % get driving parameters
        for int = 1:size(Cmatrix_allSubjects,1)
            for int2 = 1:size(Cmatrix_allSubjects,2)
                c_temp(int,int2) = Cmatrix_allSubjects{int,int2}(s);
            end
        end
        
        % get functional connectivity
        for int = 1:size(FC_allSubjects,1)
            for int2 = 1:size(FC_allSubjects,2)
                fc_temp(int,int2) = FC_allSubjects{int,int2}(s);
            end
        end
        
        % make the functional connectivity matrix upper triangular
        fc_temp = triu(fc_temp);
        
        % get all the present connections
        if ( isempty(model) )
            temp_A = temp.results.mean_A_Matrix_AllSubjects~=0;
            temp_B = temp.results.mean_B_Matrix_AllSubjects~=0;
            temp_C = temp.results.mean_C_Matrix_AllSubjects~=0;
        else
            if ( (model ~= 0) )
                temp_A = temp.results.mean_A_Matrix_AllSubjects~=0;
                temp_B = temp.results.mean_B_Matrix_AllSubjects~=0;
                temp_C = temp.results.mean_C_Matrix_AllSubjects~=0;
            else
                temp_A = temp.BMS.DCM.ffx.bma.mEps{1}.A~=0;
                temp_B = zeros(size(temp.BMS.DCM.ffx.bma.mEps{1}.B));
                matrix = temp_A-eye(size(temp_A));
                for int = 2:size(temp_B,3)
                    temp_B(:,:,int) = matrix;
                end
                temp_C = temp.BMS.DCM.ffx.bma.mEps{1}.C~=0;
            end
        end
        indicator_vector = [temp_A(:); temp_B(:); temp_C(:)];
        
        % collect all the non-zero parameters
        temp_vector             = [a_temp(:); b_temp(:); c_temp(:)]';
        data(subject_counter,:) = temp_vector(indicator_vector~=0);
        
        % collect all the non-zero FC parameters
        data_FC(subject_counter,:) = fc_temp(fc_temp~=0);
        
        
        % get the names of the connections
        counter     = 0;
        name_from   = {'lAmy','rAmy','lFFA','rFFA','lOFA','rOFA'};
        name_to     = {'lAmy','rAmy','lFFA','rFFA','lOFA','rOFA'};
        name_emp    = {'','happy','angry','fear','sad'};
        
        % get endogenous connections
        for int = 1:size(temp_A,2)
            for int2 = 1:size(temp_A,1)
                if ( temp_A(int2,int) == 1 )
                    counter = counter + 1;
                    name_connection{counter} = [name_from{int} ' -> ' name_to{int2}];
                end
            end
        end

        % get modulatory inputs
        for int3 = 1:size(temp_B,3)
            for int = 1:size(temp_B,2)
                for int2 = 1:size(temp_B,1)
                    if ( temp_B(int2,int,int3) == 1 )
                        counter = counter + 1;
                        name_connection{counter} = [name_from{int} ' -> ' name_to{int2} ' (' name_emp{int3} ')'];
                    end
                end
            end
        end

        % get dirving inputs
        for int = 1:size(temp_C,2)
            for int2 = 1:size(temp_C,1)
                if ( temp_C(int2,int) == 1 )
                    counter = counter + 1;
                    name_connection{counter} = ['to ' name_to{int2}];
                end
            end
        end
    end
    
    % get the mean contrast image values
    data_CI = CI_allSubjects(subjects_analyze,:);
    
    
    % test whether there are any NaN's in the data
    vector_NaN = any(~isfinite(data));
    if ( any(vector_NaN) )
        disp('================================')
        disp('NaN in data - Results not valid!')
        disp('================================')
    end
    
    
    % check if results directory exists
    if ( ~exist(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder),'dir') )
        mkdir(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder))
    end
    
    % regress out variables
    if ( regress_covariates )
        
        % display processing step
        disp('Regressing confounds from data')
        
        % load the covariates
        info          = load([exp_foldername 'LCGA_classes/Subjects_info.mat']);
        Subjects_info = info.Subjects_info;
        
        % empty index array
        index = [];
        
        % get all the indices
        for int = 1:length(subjects_group)
            index(int) = find(subjects_group(int) == Subjects_info(:,1)); 
        end
        
        % get the confounds
        class_opt.confounds = Subjects_info(index,3);
        
        % generate filenames
        file_data           = fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['data_orth_' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local_' num2str(group_comp) '.mat']);
        file_FC_data        = fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['data_orth_FC_' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local_' num2str(group_comp) '.mat']);
        file_CI_data        = fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['data_orth_CI_' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local_' num2str(group_comp) '.mat']);
        
        clear data_old
        
    else
        
        % empty options
        class_opt = [];
        
        % generate filenames
        if ( ~isempty(model) )
            file_data           = fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['data_' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local_' num2str(group_comp) '.mat']);
            file_FC_data        = fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['data_FC_' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local_' num2str(group_comp) '.mat']);
            file_CI_data        = fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['data_CI_' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local_' num2str(group_comp) '.mat']);
        else
            file_data        = fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['data_BMS' DLPFC_name{include_DLPFC+1} '_local_' num2str(group_comp) '.mat']);
        end
        
    end
    
    % store the data features so that they can be used by the GPC
    if ( classifier == 1 )
        save(file_data,'data','labels_group')
        save(file_FC_data,'data_FC','labels_group')
        save(file_CI_data,'data_CI','labels_group')
    end
    
    % classification (VBL)
    if ( classifier == 1 )
        [~, class]                      = evalc(['loo_gp_DCM_MainExperiment(''' file_data ''',0)']);
        results(group_comp).class       = class;
        results(group_comp).stats(1,1)  = NaN;
        results(group_comp).stats(1,2)  = NaN;
        results(group_comp).stats(1,3)  = results(group_comp).class.acc_thr;
        results(group_comp).stats(1,4)  = results(group_comp).class.p_acc_thr;
    elseif ( classifier == 2 )
        results(group_comp).class       = classify_simple_liblinear(labels_group,data,3,0);
        results(group_comp).stats(1,1)  = results(group_comp).class.sample_accuracy;
        results(group_comp).stats(1,2)  = results(group_comp).class.p_accuracy;
        results(group_comp).stats(1,3)  = results(group_comp).class.balanced_accuracy;
        results(group_comp).stats(1,4)  = results(group_comp).class.p_balanced_accuracy;
        
        % get the weight vectors
        summed_weights                  = sum(abs((results(group_comp).class.weights)));
        [~,indices_sorted]              = sort(summed_weights,'descend');
        results(group_comp).weights     = name_connection(indices_sorted(1:10));
    elseif ( classifier == 3 )
        results(group_comp).class       = classify_simple_fitcsvm(labels_group, data, class_opt, 0);
        results(group_comp).stats(1,1)  = results(group_comp).class.sample_accuracy;
        results(group_comp).stats(1,2)  = results(group_comp).class.p_accuracy;
        results(group_comp).stats(1,3)  = results(group_comp).class.balanced_accuracy;
        results(group_comp).stats(1,4)  = results(group_comp).class.p_balanced_accuracy;
    end

    
    % plot the parameter estimates
    if ( plot_figure )
        figure(1)
        subplot(1,3,group_comp)
        plot(data(labels_group==1,:)','b')
        hold on
        plot(data(labels_group==2,:)','r')
        axis square
        xlim([0 size(data,2)+1])
        ylim([-6 6])
        title([group_names{group_comp}(1:3) '-' group_names{group_comp}(5:7)])
        xlabel('# parameters')
        ylabel('parameter estimate')
    end
    
    
    % classification (FC)
    if ( classifier == 1 )
        [~, class_FC]                     = evalc(['loo_gp_DCM_MainExperiment(''' file_FC_data ''',0)']);
        results(group_comp).class_FC      = class_FC;
        results(group_comp).stats_FC(1,1) = NaN;
        results(group_comp).stats_FC(1,2) = NaN;
        results(group_comp).stats_FC(1,3) = results(group_comp).class_FC.acc_thr;
        results(group_comp).stats_FC(1,4) = results(group_comp).class_FC.p_acc_thr;
    elseif ( classifier == 2 )
        results(group_comp).class_FC      = classify_simple_liblinear(labels_group,data_FC,3,0);
        results(group_comp).stats_FC(1,1) = results(group_comp).class_FC.sample_accuracy;
        results(group_comp).stats_FC(1,2) = results(group_comp).class_FC.p_accuracy;
        results(group_comp).stats_FC(1,3) = results(group_comp).class_FC.balanced_accuracy;
        results(group_comp).stats_FC(1,4) = results(group_comp).class_FC.p_balanced_accuracy;
    elseif ( classifier == 3 )
        results(group_comp).class_FC      = classify_simple_fitcsvm(labels_group,data_FC,class_opt,0);
        results(group_comp).stats_FC(1,1) = results(group_comp).class_FC.sample_accuracy;
        results(group_comp).stats_FC(1,2) = results(group_comp).class_FC.p_accuracy;
        results(group_comp).stats_FC(1,3) = results(group_comp).class_FC.balanced_accuracy;
        results(group_comp).stats_FC(1,4) = results(group_comp).class_FC.p_balanced_accuracy;
    end
    
    
    % classification (CI)
    if ( classifier == 1 )
        [~, class_CI]                     = evalc(['loo_gp_DCM_MainExperiment(''' file_CI_data ''',0)']);
        results(group_comp).class_CI      = class_CI;
        results(group_comp).stats_CI(1,1) = NaN;
        results(group_comp).stats_CI(1,2) = NaN;
        results(group_comp).stats_CI(1,3) = results(group_comp).class_CI.acc_thr;
        results(group_comp).stats_CI(1,4) = results(group_comp).class_CI.p_acc_thr;
    elseif ( classifier == 2 )
        results(group_comp).class_CI      = classify_simple_liblinear(labels_group,data_CI,3,0);
        results(group_comp).stats_CI(1,1) = results(group_comp).class_CI.sample_accuracy;
        results(group_comp).stats_CI(1,2) = results(group_comp).class_CI.p_accuracy;
        results(group_comp).stats_CI(1,3) = results(group_comp).class_CI.balanced_accuracy;
        results(group_comp).stats_CI(1,4) = results(group_comp).class_CI.p_balanced_accuracy;
    elseif ( classifier == 3 )
        results(group_comp).class_CI      = classify_simple_fitcsvm(labels_group,data_CI,class_opt,0);
        results(group_comp).stats_CI(1,1) = results(group_comp).class_CI.sample_accuracy;
        results(group_comp).stats_CI(1,2) = results(group_comp).class_CI.p_accuracy;
        results(group_comp).stats_CI(1,3) = results(group_comp).class_CI.balanced_accuracy;
        results(group_comp).stats_CI(1,4) = results(group_comp).class_CI.p_balanced_accuracy;
    end
    
    % select which analysis to choose
    results_disp(group_comp).class    = results(group_comp).class;
    results_disp(group_comp).stats    = results(group_comp).stats;
    results_disp(group_comp).class_FC = results(group_comp).class_FC;
    results_disp(group_comp).stats_FC = results(group_comp).stats_FC;
    results_disp(group_comp).class_CI = results(group_comp).class_CI;
    results_disp(group_comp).stats_CI = results(group_comp).stats_CI;
    
    
    % compare classification rates
    if ( classifier ~= 1 )
        warning off
        p_values(group_comp)  = acc_cmp1(labels_group,results_disp(group_comp).class.predicted_labels,results_disp(group_comp).class_FC.predicted_labels);
        p_values2(group_comp) = acc_cmp1(labels_group,results_disp(group_comp).class.predicted_labels,results_disp(group_comp).class_CI.predicted_labels);
        warning on
    end
    
    % asign the labels
    labels_all{group_comp} = labels_group;
    
    % plot a figure of the SVM weights
    if ( classifier == 2 )
        figure('units','normalized','outerposition',[0 0 1 1])
        s = sum(abs((results(group_comp).class.weights)));
        hold on
        col = [192 0 0]/255;
        barh(s, 'facecolor', col, 'edgecolor', 'none');
        plot((max(s)+1)*ones(length(indices_sorted(1:10)),1),indices_sorted(1:10),'k*')
        axis ij;
        set(gca, 'box', 'off');
        set(gca, 'YTickLabel', name_connection);
        set(gca, 'YTick', 1:length(s));
        set(gca,'fontsize',8);
        title(strrep(group_names{group_comp},'_','\_'))
        p = get(gca,'position');
        p(3) = 0.2;
        set(gca,'position',p);
    end
    
end

% significance text
sig_text = {'','*'};

% display the DCM results
if ( ~isempty(model) ) 
    fprintf(['\nClassification ' name{model+1} '_local' regress_name{regress_covariates+1} ' - ' classifier_name{classifier} ' (spm12_v7487' ' / NeuroSynth' SPM_folder_text{SPM_prep+1} ' / Schmaal' ' / ' settings_folder{include_DLPFC+1} '):\n'])
else
    fprintf(['\nClassification BMS_local' regress_name{regress_covariates+1} ' (pxp = ' num2str(pxp_max) ') - ' classifier_name{classifier} ' (spm12_v7487' ' / NeuroSynth' SPM_folder_text{SPM_prep+1} ' / Schmaal' ' / ' settings_folder{include_DLPFC+1} '):\n'])
end
fprintf(['\t\t\t' group_names{1} '\t\t' group_names{2} '\t\t' group_names{3} '\n'])
fprintf(['CV accuracy: \t\t %f' sig_text{(results(1).stats(1,2)<0.05)+1} '\t %f' sig_text{(results(2).stats(1,2)<0.05)+1} '\t %f' sig_text{(results(3).stats(1,2)<0.05)+1} '\n'],results(1).stats(1,1),results(2).stats(1,1),results(3).stats(1,1));
fprintf('Balanced accuracy: \t '); fprintf((results(1).stats(1,4)<0.05)+1,['%f' sig_text{(results(1).stats(1,4)<0.05)+1} '\t '],results(1).stats(1,3)); fprintf((results(2).stats(1,4)<0.05)+1,['%f' sig_text{(results(2).stats(1,4)<0.05)+1} '\t '],results(2).stats(1,3)); fprintf((results(3).stats(1,4)<0.05)+1,['%f' sig_text{(results(3).stats(1,4)<0.05)+1} '\n'],results(3).stats(1,3));


% display the FC results
fprintf(['\nClassification FC_local' regress_name{regress_covariates+1} ' - ' classifier_name{classifier} ' (NeuroSynth' SPM_folder_text{SPM_prep+1} ' / Schmaal' ' / ' settings_folder{include_DLPFC+1} '):\n'])
fprintf(['\t\t\t' group_names{1} '\t\t' group_names{2} '\t\t' group_names{3} '\n'])
fprintf(['CV accuracy: \t\t %f' sig_text{(results(1).stats_FC(1,2)<0.05)+1} '\t %f' sig_text{(results(2).stats_FC(1,2)<0.05)+1} '\t %f' sig_text{(results(3).stats_FC(1,2)<0.05)+1} '\n'],results(1).stats_FC(1,1),results(2).stats_FC(1,1),results(3).stats_FC(1,1));
fprintf('Balanced accuracy: \t '); fprintf((results(1).stats_FC(1,4)<0.05)+1,['%f' sig_text{(results(1).stats_FC(1,4)<0.05)+1} '\t '],results(1).stats_FC(1,3)); fprintf((results(2).stats_FC(1,4)<0.05)+1,['%f' sig_text{(results(2).stats_FC(1,4)<0.05)+1} '\t '],results(2).stats_FC(1,3)); fprintf((results(3).stats_FC(1,4)<0.05)+1,['%f' sig_text{(results(3).stats_FC(1,4)<0.05)+1} '\n'],results(3).stats_FC(1,3));


% display the CI results
fprintf(['\nClassification CI_local' regress_name{regress_covariates+1} ' - ' classifier_name{classifier} ' (NeuroSynth' SPM_folder_text{SPM_prep+1} ' / Schmaal' ' / ' settings_folder{include_DLPFC+1} '):\n'])
fprintf(['\t\t\t' group_names{1} '\t\t' group_names{2} '\t\t' group_names{3} '\n'])
fprintf(['CV accuracy: \t\t %f' sig_text{(results(1).stats_CI(1,2)<0.05)+1} '\t %f' sig_text{(results(2).stats_CI(1,2)<0.05)+1} '\t %f' sig_text{(results(3).stats_CI(1,2)<0.05)+1} '\n'],results(1).stats_CI(1,1),results(2).stats_CI(1,1),results(3).stats_CI(1,1));
fprintf('Balanced accuracy: \t '); fprintf((results(1).stats_CI(1,4)<0.05)+1,['%f' sig_text{(results(1).stats_CI(1,4)<0.05)+1} '\t '],results(1).stats_CI(1,3)); fprintf((results(2).stats_CI(1,4)<0.05)+1,['%f' sig_text{(results(2).stats_CI(1,4)<0.05)+1} '\t '],results(2).stats_CI(1,3)); fprintf((results(3).stats_CI(1,4)<0.05)+1,['%f' sig_text{(results(3).stats_CI(1,4)<0.05)+1} '\n'],results(3).stats_CI(1,3));


% display the difference
fprintf('\nComparison DCM - FC (p-value): \t %f \t %f \t %f\n',p_values(1),p_values(2),p_values(3))
fprintf('Comparison DCM - CI (p-value): \t %f \t %f \t %f\n',p_values2(1),p_values2(2),p_values2(3))


% plot a figure that displays the generative embedding results
if ( plot_figure )
    figure(2);
    ha(1) = bar(1, results_disp(1).stats(1,3), 'FaceColor', [0.05 0.05 0.9], 'FaceAlpha', 0.5);
    hold on
    errorbar(1, results_disp(1).stats(1,3), results_disp(1).class.bounds_balanced_accuracy(1)-results_disp(1).stats(1,3), results_disp(1).class.bounds_balanced_accuracy(2)-results_disp(1).stats(1,3),'k','LineWidth',2)
    ha(2) = bar(2, results_disp(1).stats_FC(1,3), 'FaceColor', [0.8 0.8 0.8]);
    errorbar(2, results_disp(1).stats_FC(1,3), results_disp(1).class_FC.bounds_balanced_accuracy(1)-results_disp(1).stats_FC(1,3), results_disp(1).class_FC.bounds_balanced_accuracy(2)-results_disp(1).stats_FC(1,3),'k','LineWidth',2)
    ha(3) = bar(3, results_disp(1).stats_CI(1,3), 'FaceColor', [0.4 0.4 0.4]);
    errorbar(3, results_disp(1).stats_CI(1,3), results_disp(1).class_CI.bounds_balanced_accuracy(1)-results_disp(1).stats_CI(1,3), results_disp(1).class_CI.bounds_balanced_accuracy(2)-results_disp(1).stats_CI(1,3),'k','LineWidth',2)

    bar(5, results_disp(2).stats(1,3), 'FaceColor', [0.05 0.05 0.9], 'FaceAlpha', 0.5)
    errorbar(5, results_disp(2).stats(1,3), results_disp(2).class.bounds_balanced_accuracy(1)-results_disp(2).stats(1,3), results_disp(2).class.bounds_balanced_accuracy(2)-results_disp(2).stats(1,3),'k','LineWidth',2)
    bar(6, results_disp(2).stats_FC(1,3), 'FaceColor', [0.8 0.8 0.8])
    errorbar(6, results_disp(2).stats_FC(1,3), results_disp(2).class_FC.bounds_balanced_accuracy(1)-results_disp(2).stats_FC(1,3), results_disp(2).class_FC.bounds_balanced_accuracy(2)-results_disp(2).stats_FC(1,3),'k','LineWidth',2)
    bar(7, results_disp(2).stats_CI(1,3), 'FaceColor', [0.4 0.4 0.4])
    errorbar(7, results_disp(2).stats_CI(1,3), results_disp(2).class_CI.bounds_balanced_accuracy(1)-results_disp(2).stats_CI(1,3), results_disp(2).class_CI.bounds_balanced_accuracy(2)-results_disp(2).stats_CI(1,3),'k','LineWidth',2)

    bar(9, results_disp(3).stats(1,3), 'FaceColor', [0.05 0.05 0.9], 'FaceAlpha', 0.5)
    errorbar(9, results_disp(3).stats(1,3), results_disp(3).class.bounds_balanced_accuracy(1)-results_disp(3).stats(1,3), results_disp(3).class.bounds_balanced_accuracy(2)-results_disp(3).stats(1,3),'k','LineWidth',2)
    bar(10, results_disp(3).stats_FC(1,3), 'FaceColor', [0.8 0.8 0.8])
    errorbar(10, results_disp(3).stats_FC(1,3), results_disp(3).class_FC.bounds_balanced_accuracy(1)-results_disp(3).stats_FC(1,3), results_disp(3).class_FC.bounds_balanced_accuracy(2)-results_disp(3).stats_FC(1,3),'k','LineWidth',2)
    bar(11, results_disp(3).stats_CI(1,3), 'FaceColor', [0.4 0.4 0.4])
    errorbar(11, results_disp(3).stats_CI(1,3), results_disp(3).class_CI.bounds_balanced_accuracy(1)-results_disp(3).stats_CI(1,3), results_disp(3).class_CI.bounds_balanced_accuracy(2)-results_disp(3).stats_CI(1,3),'k','LineWidth',2)

    ylim([0 1])
    xlim([0 12])
    axis square
    set(gca,'ytick',0:0.25:1)
    set(gca,'xtick',[2 6 10])
    set(gca,'xticklabel',{[group_names{1}(1:3) '-' group_names{1}(5:7)],[group_names{2}(1:3) '-' group_names{2}(5:7)],[group_names{3}(1:3) '-' group_names{3}(5:7)]},'FontSize',12)
    legend(ha,{'DCM','fun conn','con image'},'Location','SE')
    ylabel('balanced accuracy','FontSize',14)
end


% check if results directory exists
if ( ~exist(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder),'dir') )
    mkdir(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder))
end

% store the results of the generative embedding
if ( ~isempty(model) )
    save(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding' regress_name{regress_covariates+1} name{model_number+1} DLPFC_name{include_DLPFC+1} '_local.mat']),'results')
else
    save(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding' regress_name{regress_covariates+1} 'BMS' DLPFC_name{include_DLPFC+1} '_local.mat']),'results')
end


% define the data to plot
if ( plot_figure )
    
    % open the figure
    figure(3)
    
    % iterate over comparisons
    for group_comp = 1:3
    
        % get the correct trial
        correct_trials = results_disp(group_comp).class.correct_trials;

        % define the results array
        results_array = zeros(length(labels_all{group_comp}),1);
        results_array(((correct_trials==1) + (labels_all{group_comp}==1))'==2,1) = 1;
        results_array(((correct_trials==0) + (labels_all{group_comp}==2))'==2,1) = 1;
        results_array(((correct_trials==1) + (labels_all{group_comp}==2))'==2,1) = 2;
        results_array(((correct_trials==0) + (labels_all{group_comp}==1))'==2,1) = 2;
        
        % plot the results array
        subplot(1,3,group_comp)
        imagesc(results_array')
        hold on
        for int = 1:length(labels_all{group_comp})
            if ( int ~= sum(labels_all{group_comp}==1) )
                plot([int+0.5 int+0.5],[0.5 1.5],'k-','LineWidth',1)
            else
                plot([int+0.5 int+0.5],[0.5 1.5],'k-','LineWidth',4)
            end
        end
        colormap('bipolar')
        axis square
        caxis([0.5 2.8])
        set(gca,'ytick',1)
        set(gca,'xtick',[1 sum(labels_all{group_comp}==1) length(labels_all{group_comp})])
        set(gca,'yticklabel',' ')
        xlabel('subjects')
        title([group_names{group_comp}(1:3) '-' group_names{group_comp}(5:7)])
        
    end
end

end
