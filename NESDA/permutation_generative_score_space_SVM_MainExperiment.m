function permutation_generative_score_space_SVM_MainExperiment(FilenameInfo,model,experiment,display_results)
% Evaluate the utility of the projection into the generative score space
% for the different patient subgroups - namely fast remission, gradualy 
% improving, and chronic. The script takes the DCM posterior parameter 
% estimates and the feature weights from the support vector machines (SVM).
%
% Input:
%   FilenameInfo    -- important definitions for the experiment
%   model           -- model to be analyzed
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   display_results -- show results?
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% fix the random number generator seed
load('rngSeed.mat')
rng(rngSeed);

% close all figures
close all


% load the paths if necessary
if ( isempty(FilenameInfo) )
    
    % get the path
    m_path = mfilename('fullpath');
    m_path = m_path(1:find(m_path=='/',1,'last'));

    try
        load(fullfile(m_path,'ConfigFile.mat'))
    catch err
        disp('Need to specify FilenameInfo containing paths!')
        rethrow(err)
    end
end


% default settings
SPM_prep        = 1;
include_DLPFC   = 0;
basis_set       = 2;
version         = [];

% set the experiment
exp_folder = {'FACES/','ToL/'};

% set folders
SPM_folder          = {'','SPM_preprocessing/'};
SPM_folder_text     = {'',' / SPM_preprocessing'};
DLPFC_name          = {'','_DLPFC'};
basis_folder        = {'HRF','TD','IBS',''};
settings_folder    	= {'woDLPFC','wDLPFC'};

% add path
addpath(genpath(fullfile(FilenameInfo.MATLAB_path,'PBAR')))

% set the SPM path and the path to the experiment
foldername = [FilenameInfo.DataPath exp_folder{experiment}];


% define the name of the different models
name = {'BMA','model_FF_INTER','model_FB_INTER','model_RC_INTER','model_FF_noINTER',...
    'model_FB_noINTER','model_RC_noINTER','model_noMOD'};

% specify the model folder name
if ( isempty(model) )
    model_folder  = 'Bayesian_model_selection_RandRest';
elseif ( model == 0 )
    model_number  = 0; 
    model_folder  = 'Bayesian_model_averaging_RandRest_single';
else
    model_number  = model;
    model_folder  = 'IndividualModel_RandRest';
end

% define group and emotion names
group_names = {'CHR\_REM','CHR\_IMP','IMP\_REM'};
Emo_names   = {'happy','angry','fear','sad'};


% load the actual classifcation results
if ( ~isempty(model) )
    Effect = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local.mat']));
else
    Effect = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbeddingBMS' DLPFC_name{include_DLPFC+1} '_local.mat']));
end


% number of endogenous connections (to select correct parameters)
NR_Amatrix = 20;
NR_Bmatrix = 56;

% number of parameters
NR_param = size(Effect.results(1).class.features,2);

% number of permuations
maxNrPerm   = 10000;
PermIndices	= NaN(maxNrPerm,NR_param);

% define the results arrays
Weights             = cell(size(Effect.results,1),1);
Features            = cell(size(Effect.results,1),1);
DecisionValue_Emo   = cell(size(Effect.results,1),4);
DecisionValue_All   = cell(size(Effect.results,1),maxNrPerm);
training_accuracy   = cell(size(Effect.results,1),1);
class_edge          = cell(size(Effect.results,1),1);
weights_all         = cell(size(Effect.results,1),1);


% generate the permutations
for NrPerm = 1:maxNrPerm
    PermIndices(NrPerm,:) = randperm(NR_param);
end


% iterate over permutations
for NrPerm = 1:size(PermIndices,1)+1
    
    % get permuted parameter labels and actual parameter labels
    if NrPerm <= size(PermIndices,1)
        perm_Aparams = PermIndices(NrPerm,1:NR_Amatrix);
        perm_Bparams = PermIndices(NrPerm,NR_Amatrix+1:NR_Amatrix+NR_Bmatrix);
        perm_Cparams = PermIndices(NrPerm,NR_param-1:NR_param);
    else
        perm_Aparams = 1:NR_Amatrix;
        perm_Bparams = NR_Amatrix+1:NR_Amatrix+NR_Bmatrix;
        perm_Cparams = NR_param-1:NR_param;
    end
    
    % evaluate for all group comparisons
    for group_comp = 1:size(Effect.results,2)

        % actual weights
        Weights{group_comp}  = mean(Effect.results(group_comp).class.weights);
        Features{group_comp} = Effect.results(group_comp).class.features;

        % compute dot products of weights and features
        for s = 1:size(Features{group_comp},1)

            % endogenous, modulatory (emotion), driving
            DecisionValue_All{group_comp,NrPerm}(s,1) = Weights{group_comp}(perm_Aparams) * Features{group_comp}(s,perm_Aparams)';
            DecisionValue_All{group_comp,NrPerm}(s,2) = Weights{group_comp}(perm_Bparams) * Features{group_comp}(s,perm_Bparams)';
            DecisionValue_All{group_comp,NrPerm}(s,3) = Weights{group_comp}(perm_Cparams) * Features{group_comp}(s,perm_Cparams)';

%             % emotion-specific categories
%             DecisionValue_Emo{group_comp,1}(s) = Weights{group_comp}(NR_Amatrix+1:NR_Amatrix+14) * Features{group_comp}(s,NR_Amatrix+1:NR_Amatrix+14)';
%             DecisionValue_Emo{group_comp,2}(s) = Weights{group_comp}(NR_Amatrix+15:NR_Amatrix+28) * Features{group_comp}(s,NR_Amatrix+15:NR_Amatrix+28)';
%             DecisionValue_Emo{group_comp,3}(s) = Weights{group_comp}(NR_Amatrix+29:NR_Amatrix+42) * Features{group_comp}(s,NR_Amatrix+29:NR_Amatrix+42)';
%             DecisionValue_Emo{group_comp,4}(s) = Weights{group_comp}(NR_Amatrix+33:NR_Amatrix+56) * Features{group_comp}(s,NR_Amatrix+33:NR_Amatrix+56)';

        end
        
        % asign the training data and labels
        train_data   = DecisionValue_All{group_comp,NrPerm};
        train_labels = Effect.results(group_comp).class.true_labels;
        
        % train the classifier (no hyperparameter optimization)
        Md1 = fitcsvm(train_data,train_labels,'KernelFunction','linear','OptimizeHyperparameters','no');
        
        % get the training error
        warning off
        Md1 = fitPosterior(Md1);
        assigned_labels = resubPredict(Md1);
        C = confusionmat(train_labels, assigned_labels);
        training_accuracy{group_comp}(NrPerm) = (C(1,1)/sum(C(1,:)) + C(2,2)/sum(C(2,:)))/2;
        class_edge{group_comp}(NrPerm)        = resubEdge(Md1);
        weights_all{group_comp}(NrPerm,:)  	  = Md1.Beta';
        warning on
        
    end
end

% output extra line break
fprintf('\n')

% store the results
results.training_accuracy = training_accuracy;
results.class_edge        = class_edge;
results.weights_all   	  = weights_all;


% results (p-value) arrays
p_val_weights_NonParam  = NaN(3,3);
p_val_Bacc_NonParam     = NaN(3,3);


% check if figure folder exists
if ( ~exist(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local']),'dir') )
    mkdir(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local']))
else
    
    % delete previous plots of necessary
    if ( exist(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_GenerativeScoreSpace_WeightsPermutations.ps'),'file') )
        delete(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_GenerativeScoreSpace_WeightsPermutations.ps'))
    end
    
    % delete previous plots of necessary
    if ( exist(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_GenerativeScoreSpace_AccuracyPermutations.ps'),'file') )
        delete(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_GenerativeScoreSpace_AccuracyPermutations.ps'))
    end
end

% switch off warnings
warning off


% display the results 
if ( display_results )
    
    % close all figures
    close all
    
    % illustrate the results
    for group_comp = 1:3
        
        % evaluate the p values (weights)
        p_val_weights_NonParam(group_comp,1) = sum(results.weights_all{group_comp}(1:maxNrPerm,1) <= results.weights_all{group_comp}(maxNrPerm+1,1))/maxNrPerm;
        p_val_weights_NonParam(group_comp,2) = sum(results.weights_all{group_comp}(1:maxNrPerm,2) >= results.weights_all{group_comp}(maxNrPerm+1,2))/maxNrPerm;
        p_val_weights_NonParam(group_comp,3) = sum(results.weights_all{group_comp}(1:maxNrPerm,3) <= results.weights_all{group_comp}(maxNrPerm+1,3))/maxNrPerm;
        
        % evaluate the p values (balanced accuracy)
        p_val_Bacc_NonParam(group_comp,1) = sum(results.training_accuracy{group_comp}(1:maxNrPerm) >= results.training_accuracy{group_comp}(maxNrPerm+1))/maxNrPerm;
        
        
        % open necessary figures
        figure('units','normalized','outerposition',[0 0 1 1]);
        figure('units','normalized','outerposition',[0 0 1 1]);
        
        % select figure 1
        figure(2*group_comp-1)
        
        % define the titles
        title_labels = {'Endogenous','Modulatory','Driving input'};
        
        % iterate over paramater class
        for param_class = 1:length(title_labels)
        
            subplot(1,3,param_class)
            h = histogram(results.weights_all{group_comp}(1:maxNrPerm,param_class)-eps);
            h.NumBins = 50;
            h.FaceColor = 'w';
            hold on
            ylimits = ylim();
            xlimits = xlim();
            plot([results.weights_all{group_comp}(maxNrPerm+1,param_class) results.weights_all{group_comp}(maxNrPerm+1,param_class)],ylimits,'r','LineWidth',3)
            text(xlimits(1),ylimits(2),['p = ' num2str(p_val_weights_NonParam(group_comp,param_class))])
            title([group_names{group_comp} ': ' title_labels{param_class}],'FontSize',18)
            xlabel('Weight (SVM)','FontSize',14)
            ylabel('Number','FontSize',14)
            axis square
            box off
            
        end
        
        % switch to landscape and save figure
        h = gcf;
        set(h,'PaperOrientation','landscape');
        print(gcf, '-dpsc', fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_GenerativeScoreSpace_WeightsPermutations'), '-fillpage', '-append')
        
        
        % select figure 2
        figure(2*group_comp)
        h = histogram(results.training_accuracy{group_comp}(1:maxNrPerm)-eps);
        h.BinWidth = 0.01;
        h.FaceColor = 'w';
        hold on
        ylimits = ylim();
        xlimits = xlim();
        plot([results.training_accuracy{group_comp}(maxNrPerm+1) results.training_accuracy{group_comp}(maxNrPerm+1)],ylimits,'r','LineWidth',3)
        text(xlimits(1),ylimits(2),['p = ' num2str(p_val_Bacc_NonParam(group_comp))])
        xlim([0.5 1])
        title([group_names{group_comp} ': Training Accuracy'],'FontSize',18)
        xlabel('In-sample accuracy','FontSize',14)
        ylabel('Number','FontSize',14)
        axis square
        box off
        
        % switch to landscape and save figure
        h = gcf;
        set(h,'PaperOrientation','landscape');
        print(gcf, '-dpsc', fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_GenerativeScoreSpace_AccuracyPermutations'), '-fillpage', '-append')
        
    end
end

% switch on warnings
warning on


% summarize the (within-sample) classification results
fprintf('\nWithin-sample classification balanced accuracy:\n')
fprintf('\t\tCHR_REM\t\tCHR_IMP\t\tIMP_REM\n')
fprintf('Endogenous: \t %f \t %f \t %f \n',results.training_accuracy{1}(maxNrPerm+1),results.training_accuracy{2}(maxNrPerm+1),results.training_accuracy{3}(maxNrPerm+1));


% summarize the permutation test on the feature weights
fprintf('\nSignificance of DCM parameter classes:\n')
fprintf('\t\tCHR_REM\t\tCHR_IMP\t\tIMP_REM\n')
fprintf('Endogenous: \t %f \t %f \t %f \n',p_val_weights_NonParam(1,1),p_val_weights_NonParam(2,1),p_val_weights_NonParam(3,1));
fprintf('Modulatory: \t %f \t %f \t %f \n',p_val_weights_NonParam(1,2),p_val_weights_NonParam(2,2),p_val_weights_NonParam(3,2));
fprintf('Driving: \t %f \t %f \t %f \n\n',p_val_weights_NonParam(1,3),p_val_weights_NonParam(2,3),p_val_weights_NonParam(3,3));


% store the results
if ( ~isempty(model) )
    save(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeScoreSpace' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local.mat']),'results')
else
    save(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeScoreSpaceBMS' DLPFC_name{include_DLPFC+1} '_local.mat']),'results')
end

end
