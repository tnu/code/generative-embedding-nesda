function group_analysis_DCM_MainExperiment(FilenameInfo,model,experiment,version,verbose)
% Evaluates the mean effective connectivity parameters across all the 
% patient subgroups from the NESDA study and test whether the effective
% connectivity parameters are significantly different between the different
% patient groups.
%
% Input:
%   FilenameInfo    -- important definitions for the experiment
%   model           -- model to be analyzed
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   version         -- version
%   verbose         -- output results
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% specify some default settings
if ( nargin < 5 )
    verbose = 0;
end


% if FilenameInfo not specified, try to load
if ( isempty(FilenameInfo) )
    
    % get the path
    m_path = mfilename('fullpath');
    m_path = m_path(1:find(m_path=='/',1,'last'));
    
    try
        load(fullfile(m_path,'ConfigFile.mat'))
    catch err
        disp('Need to specify FilenameInfo containing paths!')
        rethrow(err)
    end
end


% add the respective path
addpath(genpath(fullfile(FilenameInfo.MATLAB_path,'PBAR')))
addpath(genpath(fullfile(FilenameInfo.MATLAB_path,'fdr_bh')))


% default settings
SPM_prep        = 1;
include_DLPFC   = 0;
basis_set       = 2;


% set the experiment
exp_folder = {'FACES/','ToL/'};

% set folders
SPM_folder          = {'','SPM_preprocessing/'};
DLPFC_name          = {'','_DLPFC'};
settings_folder    	= {'woDLPFC','wDLPFC'};
basis_folder        = {'HRF','TD','IBS',''};


% set the path of the experiment
exp_foldername	 = FilenameInfo.Exp_path;

% set the SPM path and the path to the experiment
foldername = [FilenameInfo.DataPath exp_folder{experiment}];


% define the name of the different models
name = {'BMA','model_FF_INTER','model_FB_INTER','model_RC_INTER','model_FF_noINTER',...
    'model_FB_noINTER','model_RC_noINTER','model_noMOD'};

% define the different groups
group_names_ttest = {'CHR_REM','CHR_IMP','IMP_REM'};
group_comb_ttest  = {[3 1],[3 2],[2 1]};

% specify the model folder name
if ( isempty(model) )
    model_folder  = 'Bayesian_model_selection_RandRest';
elseif ( model == 0 )
    model_number  = 0; 
    model_folder  = 'Bayesian_model_averaging_RandRest_single';
else
    model_number  = model;
    model_folder  = 'IndividualModel_RandRest';
end

% get the classes of the subjects
temp                = load([exp_foldername 'LCGA_classes/LCGA_classes.mat']);
LCGA_classes        = temp.LCGA_classes;
AllSubjects_Schmaal = xlsread([exp_foldername 'information/AllSubjects_Faces.xlsx']);

% results folder
results_folder = 'DCM_GroupAnalysis';

% load the posterior parameter estimates
if ( ~isempty(model) && model_number ~= 0 )
    temp = load(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['PosteriorParameterEstimates_' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local.mat']));
elseif ( isempty(model) )
    file = dir(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,[settings_folder{include_DLPFC+1} '_local'],'PosteriorParameterEstimates*'));
    temp    = load(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,[settings_folder{include_DLPFC+1} '_local'],file(1).name));
    pxp_max = temp.results.pxp_max;
else
    temp = load(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,[settings_folder{include_DLPFC+1} '_local'],'BMS.mat'));
    
    % bring the results in the same format
    for s = 1:length(temp.BMS.DCM.ffx.bma.mEps)
        
        % endogenous connections
        for int = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.A,1)
            for int2 = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.A,2)
                temp.results.A_Matrix_AllSubjects{int,int2}(s) = temp.BMS.DCM.ffx.bma.mEps{s}.A(int,int2);
            end
        end
        
        % modulatory influences
        for int = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.B,1)
            for int2 = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.B,2)
                for int3 = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.B,3)
                    temp.results.B_Matrix_AllSubjects{int,int2,int3}(s) = temp.BMS.DCM.ffx.bma.mEps{s}.B(int,int2,int3);
                end
            end
        end
        
        % driving inputs
        for int = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.C,1)
            for int2 = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.C,2)
                temp.results.C_Matrix_AllSubjects{int,int2}(s) = temp.BMS.DCM.ffx.bma.mEps{s}.C(int,int2);
            end
        end
    end
    
    % asign the subjects included
    temp.results.AllSubjects = temp.BMS.DCM.ffx.AllSubjects;
    
end


% asign the data
Amatrix_allSubjects     = temp.results.A_Matrix_AllSubjects;
Bmatrix_allSubjects     = temp.results.B_Matrix_AllSubjects;
Cmatrix_allSubjects     = temp.results.C_Matrix_AllSubjects;


% define a labels array
labels = zeros(1,length(temp.results.AllSubjects));

% dummy number of subjects
nr_subject = [];
found_subjects = [];


for int = 1:length(temp.results.AllSubjects)

    % find the index of the subject in the LCGA classes
    index_LCGA = find(LCGA_classes(:,1) == str2double(temp.results.AllSubjects{int}),1);

    if ( ~isempty(index_LCGA) )
        
        % select the subjects that were included in Schmaal et al.
        index = find(AllSubjects_Schmaal == str2double(temp.results.AllSubjects{int}),1);

        if ( ~isempty(index) )
            if ( ~isempty(index_LCGA) && isfinite(Amatrix_allSubjects{1,1}(int)) )
                nr_subject(end+1)     = LCGA_classes(index_LCGA,1);
                found_subjects(end+1) = str2double(temp.results.AllSubjects{int});
            end

            % if the subject exists, assign the class label
            if ( isfinite(Amatrix_allSubjects{1,1}(int)) )
                labels(int) = LCGA_classes(index_LCGA,3);
            end
        end
    end
end


% display how many subjects were found
fprintf('\n')
disp(['Found: ' num2str(length(nr_subject)) ' out of ' num2str(length(AllSubjects_Schmaal)) ' subjects - Running: ' basis_folder{basis_set+1}])

% display subject name of missing subject
if ( length(nr_subject) ~= length(AllSubjects_Schmaal) )
    missing_subjects = setdiff(AllSubjects_Schmaal,found_subjects);
    fprintf((~isempty(missing_subjects))+1,'Subjects missing:');
    for int = 1:length(missing_subjects)
        fprintf((~isempty(missing_subjects))+1,' %d',missing_subjects(int));
    end
    fprintf('\n')
end


% initialize the group arrays
mean_Amatrix_allSubjects_allGroups = NaN(size(Amatrix_allSubjects));
pVal_Amatrix_allSubjects_allGroups = NaN(size(Amatrix_allSubjects));
mean_Bmatrix_allSubjects_allGroups = NaN(size(Bmatrix_allSubjects));
pVal_Bmatrix_allSubjects_allGroups = NaN(size(Bmatrix_allSubjects));
mean_Cmatrix_allSubjects_allGroups = NaN(size(Cmatrix_allSubjects));
pVal_Cmatrix_allSubjects_allGroups = NaN(size(Cmatrix_allSubjects));


% get the mean endogenous parameters
for int = 1:size(Amatrix_allSubjects,1)
    for int2 = 1:size(Amatrix_allSubjects,2)
        mean_Amatrix_allSubjects_allGroups(int,int2) = mean(Amatrix_allSubjects{int,int2});
        [~,p] = ttest(Amatrix_allSubjects{int,int2});
        pVal_Amatrix_allSubjects_allGroups(int,int2) = p;
    end
end

% get the mean modulatory parameters
for int = 1:size(Bmatrix_allSubjects,1)
    for int2 = 1:size(Bmatrix_allSubjects,2)
        for int3 = 1:size(Bmatrix_allSubjects,3)
            mean_Bmatrix_allSubjects_allGroups(int,int2,int3) = mean(Bmatrix_allSubjects{int,int2,int3});
            [~,p] = ttest(Bmatrix_allSubjects{int,int2,int3});
            pVal_Bmatrix_allSubjects_allGroups(int,int2,int3) = p;
        end
    end
end

% get the mean driving parameters
for int = 1:size(Cmatrix_allSubjects,1)
    for int2 = 1:size(Cmatrix_allSubjects,2)
        mean_Cmatrix_allSubjects_allGroups(int,int2) = mean(Cmatrix_allSubjects{int,int2});
        [~,p] = ttest(Cmatrix_allSubjects{int,int2});
        pVal_Cmatrix_allSubjects_allGroups(int,int2) = p;
    end
end

% get the corrected threshold
pVal_all             = [pVal_Amatrix_allSubjects_allGroups(:); pVal_Bmatrix_allSubjects_allGroups(:); pVal_Cmatrix_allSubjects_allGroups(:)];
pVal_all             = pVal_all(isfinite(pVal_all));
[~, threshold, ~, ~] = fdr_bh(pVal_all,0.05,'dep','no');

% display the output
fprintf('\nEndogenous connectivity (sig)\n')
disp(mean_Amatrix_allSubjects_allGroups.*(pVal_Amatrix_allSubjects_allGroups<=threshold))

disp('Modulatory connectivity (sig)')
disp(mean_Bmatrix_allSubjects_allGroups.*(pVal_Bmatrix_allSubjects_allGroups<=threshold))

disp('Driving input (sig)')
disp(mean_Cmatrix_allSubjects_allGroups.*(pVal_Cmatrix_allSubjects_allGroups<=threshold))


% store the results
results.mean_Amatrix_allSubjects_allGroups = mean_Amatrix_allSubjects_allGroups;
results.mean_Bmatrix_allSubjects_allGroups = mean_Bmatrix_allSubjects_allGroups;
results.mean_Cmatrix_allSubjects_allGroups = mean_Cmatrix_allSubjects_allGroups;
results.pVal_Amatrix_allSubjects_allGroups = pVal_Amatrix_allSubjects_allGroups;
results.pVal_Bmatrix_allSubjects_allGroups = pVal_Bmatrix_allSubjects_allGroups;
results.pVal_Cmatrix_allSubjects_allGroups = pVal_Cmatrix_allSubjects_allGroups;


% initialize the temporary arrays
mean_Amatrix_allSubjects = cell(1,3);
pVal_Amatrix_allSubjects = cell(1,3);
mean_Bmatrix_allSubjects = cell(1,3);
pVal_Bmatrix_allSubjects = cell(1,3);
mean_Cmatrix_allSubjects = cell(1,3);
pVal_Cmatrix_allSubjects = cell(1,3);
threshold                = NaN(1,3);


% specify the true labels for a binary comparison of groups
for group_comp = 1:3
    
    % clear the data
    clear data
    
    % define temporary label
    labels_temp  = labels;
    labels_temp2 = labels;
    labels_temp(labels_temp~=group_comb_ttest{group_comp}(1))   = 0;
    labels_temp2(labels_temp2~=group_comb_ttest{group_comp}(2)) = 0;
    
    labels_temp(labels_temp~=0)   = 1;
    labels_temp2(labels_temp2~=0) = 1;

    % define the subjects that should be included
    subjects_analyze  = find(labels_temp~=0);
    subjects_analyze2 = find(labels_temp2~=0);
    
    
    % display the setting
    disp(['Group selection: ' group_names_ttest{group_comp} ' - Group size: ' num2str(sum(labels_temp==1)) ' vs. ' num2str(sum(labels_temp2==1))])
    
    
    % get the names of the connections
    counter         = 0;
    name_connection = [];
    name_from       = {'lAmy','rAmy','lFFA','rFFA','lOFA','rOFA'};
    name_to         = {'lAmy','rAmy','lFFA','rFFA','lOFA','rOFA'};
    name_emp        = {'','happy','angry','fear','sad'};
    
    
    % get endogenous parameters
    for int = 1:size(Amatrix_allSubjects,1)
        for int2 = 1:size(Amatrix_allSubjects,2)
            mean_Amatrix_allSubjects{group_comp}(int,int2) = mean(Amatrix_allSubjects{int,int2}(subjects_analyze))-mean(Amatrix_allSubjects{int,int2}(subjects_analyze2));
            [~,p] = ttest2(Amatrix_allSubjects{int,int2}(subjects_analyze),Amatrix_allSubjects{int,int2}(subjects_analyze2));
            pVal_Amatrix_allSubjects{group_comp}(int,int2) = p;
        end
    end
    
    % find p-values smaller 0.05
    [rows_A,columns_A] = find(pVal_Amatrix_allSubjects{group_comp} < 0.05);
    
    for sig_int = 1:length(rows_A)
        counter = counter + 1;
        name_connection{counter} = [name_from{columns_A(sig_int)} ' -> ' name_to{rows_A(sig_int)}];
    end
    
    % get modulatory parameters
    for int = 1:size(Bmatrix_allSubjects,1)
        for int2 = 1:size(Bmatrix_allSubjects,2)
            for int3 = 1:size(Bmatrix_allSubjects,3)
                mean_Bmatrix_allSubjects{group_comp}(int,int2,int3) = mean(Bmatrix_allSubjects{int,int2,int3}(subjects_analyze))-mean(Bmatrix_allSubjects{int,int2,int3}(subjects_analyze2));
                [~,p] = ttest2(Bmatrix_allSubjects{int,int2,int3}(subjects_analyze),Bmatrix_allSubjects{int,int2,int3}(subjects_analyze2));
                pVal_Bmatrix_allSubjects{group_comp}(int,int2,int3) = p;
            end
        end
    end
    
    % find p-values smaller 0.05
    for int3 = 1:size(Bmatrix_allSubjects,3)
        [rows_B,columns_B] = find(pVal_Bmatrix_allSubjects{group_comp}(:,:,int3) < 0.05);
        
        for sig_int = 1:length(rows_B)
            counter = counter + 1;
            name_connection{counter} = [name_from{columns_B(sig_int)} ' -> ' name_to{rows_B(sig_int)} ' (' name_emp{int3} ')'];
        end
    end
    
    % get driving parameters
    for int = 1:size(Cmatrix_allSubjects,1)
        for int2 = 1:size(Cmatrix_allSubjects,2)
            mean_Cmatrix_allSubjects{group_comp}(int,int2) = mean(Cmatrix_allSubjects{int,int2}(subjects_analyze))-mean(Cmatrix_allSubjects{int,int2}(subjects_analyze2));
            [~,p] = ttest2(Cmatrix_allSubjects{int,int2}(subjects_analyze),Cmatrix_allSubjects{int,int2}(subjects_analyze2));
            pVal_Cmatrix_allSubjects{group_comp}(int,int2) = p;
        end
    end
    
    % find p-values smaller 0.05
    [rows_C,columns_C] = find(pVal_Cmatrix_allSubjects{group_comp} < 0.05);
    
    for sig_int = 1:length(rows_C)
        counter = counter + 1;
        name_connection{counter} = [name_from{columns_C(sig_int)} ' -> ' name_to{rows_C(sig_int)}];
    end
    
    % get the corrected threshold
    pVal_all            = [pVal_Amatrix_allSubjects{group_comp}(:); pVal_Bmatrix_allSubjects{group_comp}(:); pVal_Cmatrix_allSubjects{group_comp}(:)];
    pVal_all            = pVal_all(isfinite(pVal_all));
    [~, crit_p, ~, ~]	= fdr_bh(pVal_all,0.05,'dep','no');
    
    % use corrected or uncorrected threshold
    threshold(group_comp) = crit_p;
    
    % asign the names
    if ( group_comp == 1 )
        name_connection_CHR_REM = name_connection;
    elseif ( group_comp == 2 )
        name_connection_CHR_IMP = name_connection;
    else
        name_connection_IMP_REM = name_connection;
    end
end


% fill the cell with empty string (if no sig. connections found)
if ( isempty(name_connection_CHR_REM)), name_connection_CHR_REM{1} = ''; end
if ( isempty(name_connection_CHR_IMP)), name_connection_CHR_IMP{1} = ''; end
if ( isempty(name_connection_IMP_REM)), name_connection_IMP_REM{1} = ''; end


% output the significant connections
if ( verbose )
    fprintf('\nSignificant DCM parameters (uncorrected p<0.05):')
    fprintf(['\nCHR_REM: ' repmat('%s || ',1,length(name_connection_CHR_REM))],name_connection_CHR_REM{:})
    fprintf(['\nCHR_IMP: ' repmat('%s || ',1,length(name_connection_CHR_IMP))],name_connection_CHR_IMP{:})
    fprintf(['\nIMP_REM: ' repmat('%s || ',1,length(name_connection_IMP_REM)) '\n'],name_connection_IMP_REM{:})
end


% display the results
fprintf('\nNumber of significant differences (FDR-corrected):\n')
fprintf(['\t\t' group_names_ttest{1} '\t\t' group_names_ttest{2} '\t\t' group_names_ttest{3} '\n'])
fprintf('Endogenous: \t %d \t\t %d \t\t %d \n',sum(pVal_Amatrix_allSubjects{1}(:)<threshold(1)),sum(pVal_Amatrix_allSubjects{2}(:)<threshold(2)),sum(pVal_Amatrix_allSubjects{3}(:)<threshold(3)));
fprintf('Modulatory: \t %d \t\t %d \t\t %d \n',sum(pVal_Bmatrix_allSubjects{1}(:)<threshold(1)),sum(pVal_Bmatrix_allSubjects{2}(:)<threshold(2)),sum(pVal_Bmatrix_allSubjects{3}(:)<threshold(3)));
fprintf('Driving: \t %d \t\t %d \t\t %d \n\n',sum(pVal_Cmatrix_allSubjects{1}(:)<threshold(1)),sum(pVal_Cmatrix_allSubjects{2}(:)<threshold(2)),sum(pVal_Cmatrix_allSubjects{3}(:)<threshold(3)));


% store the results
results.mean_Amatrix_allSubjects = mean_Amatrix_allSubjects;
results.mean_Bmatrix_allSubjects = mean_Bmatrix_allSubjects;
results.mean_Cmatrix_allSubjects = mean_Cmatrix_allSubjects;
results.pVal_Amatrix_allSubjects = pVal_Amatrix_allSubjects;
results.pVal_Bmatrix_allSubjects = pVal_Bmatrix_allSubjects;
results.pVal_Cmatrix_allSubjects = pVal_Cmatrix_allSubjects;

% store the results
if ( ~isempty(results) )
    save(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,[settings_folder{include_DLPFC+1} '_local'],'GroupComparison.mat'),'results');
end

end