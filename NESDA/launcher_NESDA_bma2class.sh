#! /bin/bash

set -e

# Load the matlab module (Matlab 2016b)
module load new matlab/9.1


# Run the classification for all groups
bsub -W 12:00 -J "job_genemb" -R "rusage[mem=7000]" -o log_genemb matlab -singleCompThread -r "generative_embedding_DCM_MainExperiment([],0,1,1,3,0,2,0,[],0)"


# Run the classification for all groups (corrected for age)
bsub -W 12:00 -J "job_genemb" -R "rusage[mem=7000]" -o log_genemb_age matlab -singleCompThread -r "generative_embedding_DCM_MainExperiment([],0,1,1,3,0,2,1,[],0)"


# Generate the permutation indices
bsub -W 1:00 -J "job_genperm" -o log_genperm matlab -singleCompThread -r "generate_permutation_indices_MainExperiment([],0,1,1000,[])"


# Run the permutation tests for all groups
for group in {1..3}
do
	for NrPerm in {1..1000}
	do
		bsub -W 4:00 -J "job_perm_$group,$NrPerm" -w 'ended("job_genperm")' -o log_perm_"$NrPerm" matlab -singleCompThread -r "permutation_test_SVM_MainExperiment([],0,1,3,$NrPerm,0,[],$group)"
	done
done

# Create a summary file of all permutation results
bsub -W 2:00 -w 'ended("job_perm_3,5*") && ended("job_perm_3,6*") && ended("job_perm_3,7*") && ended("job_perm_3,8*") && ended("job_perm_3,9*") && ended("job_perm_3,1000")' -o log_getperm matlab -singleCompThread -r "get_permutation_test_SVM_MainExperiment([],0,1,0,[])"


# Run the permutation tests for all groups (corrected for age)
for group in {1..3}
do
	for NrPerm in {1..1000}
	do
		bsub -W 4:00 -J "job_perm_age_$group,$NrPerm" -w 'ended("job_genperm")' -o log_perm_age_"$NrPerm" matlab -singleCompThread -r "permutation_test_SVM_MainExperiment([],0,1,3,$NrPerm,1,[],$group)"
	done
done


# Create a summary file of all permutation results (corrected for age)
bsub -W 2:00 -w 'ended("job_perm_age_3,5*") && ended("job_perm_age_3,6*") && ended("job_perm_age_3,7*") && ended("job_perm_age_3,8*") && ended("job_perm_age_3,9*") && ended("job_perm_age_3,1000")' -o log_getperm_age matlab -singleCompThread -r "get_permutation_test_SVM_MainExperiment([],0,1,1,[])"
