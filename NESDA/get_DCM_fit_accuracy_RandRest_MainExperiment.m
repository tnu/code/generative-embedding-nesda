function get_DCM_fit_accuracy_RandRest_MainExperiment(experiment,SPM_prep,include_DLPFC,basis_set)
% Get the correlation coefficient of the Dynamic Causal Models (DCMs) for 
% the FACES dataset or the Tower of London (ToL) dataset of the NESDA study
% (Netherlands Study on Depression and Social Anxiety). 
% 
% This function reads the DCM files that have been estimated using
% Variational Bayes under the Laplace approximation in SPM. The function
% computes the correlation coefficient and stores the respective values. 
% Importantly, this function uses the DCMs from the random restart procedure 
% in order to ensure that the best solution is found.
% 
% Input:
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   SPM_prep        -- (0) NESDA pipeline (1) SPM preprocessing
%   include_DLPFC   -- (0) not include DLPFC, (1) include DLPFC
%   basis_set       -- basis set: (0) only HRF, (1) temporal derivative, (2) informed basis set
%
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2016
% Copyright 2016 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
exp_folders = {'FACES/','ToL/'};

% set the preprocessing folder
SPM_folder      = {'','SPM_preprocessing/'};
DLPFC_name      = {'','_DLPFC'};
basis_folder    = {'HRF','TD','IBS',''};
settings_folder	= {'woDLPFC','wDLPFC'};


% set the SPM path and the path to the experiment
addpath(FilenameInfo.SPM_path)
foldername      = fullfile(FilenameInfo.DataPath,exp_folders{experiment});


% define some variables
spm('Defaults','fMRI');

% get the subject list
Subject_List = [dir(fullfile(foldername,'1*')); dir(fullfile(foldername,'2*')); dir(fullfile(foldername,'3*'))];

% define the name of the different models
name = {'model_FF_INTER','model_FB_INTER','model_RC_INTER','model_FF_noINTER',...
    'model_FB_noINTER','model_RC_noINTER','model_noMOD'};

% set the models to analyze
model_analyze = 1:length(name);
model_folder  = 'IndividualModel_RandRest';

% create a cell object for the subject names
results.AllSubjects	= cell(1,length(Subject_List));
results.R           = NaN(length(Subject_List),length(model_analyze));
results.p           = NaN(length(Subject_List),length(model_analyze));

% iterate over all models and subjects
for model_number = model_analyze
    
    % display progress
    disp(['Model ' num2str(model_number) ' - get correlations (R)'])
    
    for s = 1:length(Subject_List)

        % get the subject name
        Subject = Subject_List(s).name;
        
        % define the folder where the SPM.mat (DCM regressors) is stored
        DCM_regressors_foldername = fullfile(foldername,Subject,'FirstLevel_DCM_RandRestart');
        
        % asign the subject name
        if ( model_number == 1 )
            results.AllSubjects{s} = Subject;
        end
        
        % get the number of random restarts
        files_RandRest = dir(fullfile(foldername,'RandomStartingValues','spm12_v7487',['Model_' name{model_number} DLPFC_name{include_DLPFC+1} '/VB_RSV*.mat']));
        NR_RandRest    = length(files_RandRest); 
        F_temp         = NaN(1,NR_RandRest);
        
        % check whether all files are present and get neg. free energy
        for restart = 1:NR_RandRest
            restart_name = ['00' num2str(restart)];
            restart_name = restart_name(end-2:end);

            % filename
            filename_temp = fullfile(DCM_regressors_foldername,'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth',['DCM_NESDA_' name{model_number} DLPFC_name{include_DLPFC+1} '_local_' restart_name '.mat']);

            % try loading data
            try
                temp_DCM = load(filename_temp);
                F_temp(restart) = temp_DCM.DCM.F;
            end
        end
        
        
        % find the best solution (maximal neg. free energy)
        [~, F_max_ind] = max(F_temp);

        % get the filename appendix
        restart_max_name = ['00' num2str(F_max_ind)];
        restart_max_name = restart_max_name(end-2:end);

        % get the filename
        filename = fullfile(DCM_regressors_foldername,'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth',['DCM_NESDA_' name{model_number} DLPFC_name{include_DLPFC+1} '_local_' restart_max_name '.mat']);
        
        % load the file
        try
            temp = load(filename);
            DCM  = temp.DCM;

            % compute the correlation ebtween predicted and measured signal
            if ( isfield(DCM,'y') && isfield(DCM,'R') )
                [R_temp,p_temp] = corrcoef(DCM.y(:)+DCM.R(:),DCM.y(:));
                R = R_temp(2,1);
                p = p_temp(2,1);
            else
                [R,p] = deal(NaN);
            end

        catch
            
            % not a number
            [R,p] = deal(NaN);
            
        end
        
        % asign the correlation coefficient and p-value
        results.R(s,model_number) = R;
        results.p(s,model_number) = p;
        
    end
end


% save the results of the particular model
if ( ~exist(fullfile(foldername,'DCM_GroupAnalysis','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',model_folder,[settings_folder{include_DLPFC+1} '_local']),'dir') )
    mkdir(fullfile(foldername,'DCM_GroupAnalysis','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',model_folder,[settings_folder{include_DLPFC+1} '_local']))
end
save(fullfile(foldername,'DCM_GroupAnalysis','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',model_folder,[settings_folder{include_DLPFC+1} '_local'],['AccuracyFit_AllModels' DLPFC_name{include_DLPFC+1} '.mat']),'results')
    
end