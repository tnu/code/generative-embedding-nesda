function extract_VOI_timeseries_ROI_MainExperiment(subject,experiment,SPM_prep,basis_set)
% Finds the center coordinate of the regions of interest (ROIs) for each 
% subject individually from the NESDA study. For the FACES dataset, left and 
% right occipital face area has to be located in the inferior occipital gyrus 
% and left and right fusiform face area has to be located in the fusiform gyrus.
% 
% Input:
%   subject      	-- subject to be analyzed
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   SPM_prep        -- (0) NESDA pipeline (1) SPM preprocessing
%   basis_set       -- basis set: (0) only HRF, (1) temporal derivative, (2) informed basis set
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2017
% Copyright 2017 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
exp_folders = {'FACES/','ToL/'};

% set the SPM folder
SPM_folder	 = {'','SPM_preprocessing/'};
basis_folder = {'HRF','TD','IBS'};


% set the SPM path and the path to the experiment
addpath(FilenameInfo.SPM_path)
foldername      = fullfile(FilenameInfo.DataPath,exp_folders{experiment});


% initialize spm
spm_jobman('initcfg')

% get the subject list
Subject_List = [dir([foldername '1*']); dir([foldername '2*']); dir([foldername '3*'])];


% get the center coordinates from NeuroSynth
mask_info	= {[-40 -52 -20],[40 -50 -20],[-38 -84 -12],[42 -76 -12],[-18 -10 -16],[22 -8 -16],[44 18 22]};
ROI_names	= {'FFA_left','FFA_right','OFA_left','OFA_right','Amyg_left','Amyg_right','DLPFC_right'};


% specify the coordinate array
center_allregions = cell(length(Subject_List),length(ROI_names));

% specify the effect of interest
EoI_nr = 7; 

% define the subjects to analyze
if ( isempty(subject) )
    subjects_analyze = 1:length(Subject_List);
else
    subjects_analyze = subject;
end


% display the progress
fprintf('\nExtract VoI time series...\n')


for s = subjects_analyze

    % subject name
    Subject = Subject_List(s).name;

    % specify the SPM folder
    SPM_foldername = fullfile(foldername,Subject,'FirstLevel',SPM_folder{SPM_prep+1},basis_folder{basis_set+1});
    
    % check if subject has data
    files = dir(fullfile(SPM_foldername,'SPM.mat'));
    run_subject = ~isempty(files);

    % perform BOLD signal time series extraction
    if ( run_subject )

        % time series folder
        timeseries_folder = fullfile(SPM_foldername,'timeseries_VOI_adjusted','NeuroSynth');

        % check whether folder exists
        if ( ~exist(timeseries_folder,'dir') )
            mkdir(timeseries_folder)
        else
            delete(fullfile(timeseries_folder,'VOI*right_local_1.mat'))
            delete(fullfile(timeseries_folder,'VOI*left_local_1.mat'))
        end

        % choose which ROIs to extract
        ROI_analyze = 1:length(ROI_names);

        % extract the time series for each region of interest
        for number_of_regions = ROI_analyze

            % load the batch for extracting the time series
            load(fullfile(FilenameInfo.Batch_path, 'Batch_VoiExtraction_NeuroSynth.mat'))

            % define the path where the SPM.mat can be found
            matlabbatch{1}.spm.util.voi.spmmat = cellstr(fullfile(SPM_foldername,'SPM.mat'));

            % choose an adjustment of the time series (effects of interest)
            matlabbatch{1}.spm.util.voi.adjust = EoI_nr;

            % choose session number
            matlabbatch{1}.spm.util.voi.session = 1;

            % set the name of the VOI
            matlabbatch{1}.spm.util.voi.name = [ROI_names{number_of_regions} '_local'];

            % specify which contrast to use
            matlabbatch{1}.spm.util.voi.roi{1}.spm.contrast = 1;

            % specify the threshold correction
            matlabbatch{1}.spm.util.voi.roi{1}.spm.threshdesc = 'none';

            % specify the threshold
            matlabbatch{1}.spm.util.voi.roi{1}.spm.thresh = 1;
            
            % define center coordinate based on the group activation
            matlabbatch{1}.spm.util.voi.roi{2}.sphere.centre = mask_info{number_of_regions};

            % define the radius of the search sphere
            matlabbatch{1}.spm.util.voi.roi{2}.sphere.radius = 12;

            % define the sphere from which the eigenvariate is extracted
            matlabbatch{1}.spm.util.voi.roi{3}.sphere.centre = mask_info{number_of_regions};

            % define the radius of the sphere
            matlabbatch{1}.spm.util.voi.roi{3}.sphere.radius = 8;

            % remove the field for jumping to individual maximum
            matlabbatch{1}.spm.util.voi.roi{3}.sphere = rmfield(matlabbatch{1}.spm.util.voi.roi{3}.sphere,'move');

            % local maximum
            matlabbatch{1}.spm.util.voi.roi{3}.sphere.move.local.spm  = 1;
            matlabbatch{1}.spm.util.voi.roi{3}.sphere.move.local.mask = 'i2';

            % intersection of activation and sphere
            matlabbatch{1}.spm.util.voi.expression = 'i1 & i3';

            % run the batch and extract the time series
            spm_jobman('run',matlabbatch);

            % clear the matlabbatch
            clear matlabbatch

            % pause briefly to show extracted time series
            pause(2)

            % load the VOI to get the coordinates
            load(fullfile(SPM_foldername,['VOI_' ROI_names{number_of_regions} '_local_1.mat']))

            % get the center coordinates for the extracted time series
            center_allregions{s,number_of_regions} = xY.xyz';

        end

        % display the coordinates
        disp(['Subject ' Subject ' - ' ROI_names{1} ': ' num2str(center_allregions{s,1}(1)) ' ' num2str(center_allregions{s,1}(2)) ' ' num2str(center_allregions{s,1}(3)) ' / ' ROI_names{2} ': ' num2str(center_allregions{s,2}(1)) ' ' num2str(center_allregions{s,2}(2)) ' ' num2str(center_allregions{s,2}(3)) ' / ' ROI_names{3} ': ' num2str(center_allregions{s,3}(1)) ' ' num2str(center_allregions{s,3}(2)) ' ' num2str(center_allregions{s,3}(3)) ' / ' ROI_names{4} ': ' num2str(center_allregions{s,4}(1)) ' ' num2str(center_allregions{s,4}(2)) ' ' num2str(center_allregions{s,4}(3))])
        
        % move the extracted time series to the results folder
        movefile(fullfile(SPM_foldername,'VOI*.mat'),timeseries_folder)

        % delete the remaining files
        delete(fullfile(SPM_foldername,'VOI*'))

    else

        % display subject no data
        disp(['Subject ' Subject ' - No data!'])

    end
end

% foldername for the center coordinates
VoI_foldername = fullfile(foldername,'VoI_coordinates',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth');

% create the VoI folder
if ( ~exist(VoI_foldername,'dir') )
    mkdir(VoI_foldername)
end

% save the center coordinates in a file
if ( isempty(subject) )
    save(fullfile(VoI_foldername,'VoI_CenterCoordinates_local_AllSubjects.mat'),'center_allregions')
end

end