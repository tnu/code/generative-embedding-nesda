function copy_NESDA_data(subject_analyze,copy_direction,copy_content)
% Copies NESDA "raw" data (functional images, anatomical images, logfiles
% between "/cluster/scratch" (which is deleted afer couple of weeks) and
% the long-term storage on Euler "/cluster/project/tnu".
% 
% Input:
%   subject_analyze	-- subject(s) for which data should be copied
%   copy_direction	-- direction of data transfer
%   copy_content 	-- (0) all, (1) ana, (2) funk, (3) logfiles
%
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2016
% Copyright 2016 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% set the foldername
if ( copy_direction == 1 )
    foldername    = fullfile(FilenameInfo.DataPath,'FACES');
    to_foldername = fullfile(FilenameInfo.LongTermStorage_path,'FACES');
else
    foldername    = fullfile(FilenameInfo.LongTermStorage_path,'FACES');
    to_foldername = fullfile(FilenameInfo.DataPath,'FACES');
end

% get the subject list
Subject_List = [dir(fullfile(foldername,'1*')); dir(fullfile(foldername,'2*')); dir(fullfile(foldername,'3*'))];

% specify the subjects to copy
if ( isempty(subject_analyze) )
    subject_analyze = 1:length(Subject_List);
end

% copy data for all subjects
for s = subject_analyze
    
    % get subject name
    subject = Subject_List(s).name;
    
    % display the progress
    disp(subject)
    
    % copy anatomical images
    if ( copy_content == 0 || copy_content == 1 )
        if ( exist(fullfile(foldername,subject,'ana'),'dir') )
            if ( ~exist(fullfile(to_foldername,subject,'ana'),'dir') )
                mkdir(fullfile(to_foldername,subject,'ana'))
            end
            copyfile(fullfile(foldername,subject,'ana'),fullfile(to_foldername,subject,'ana'))
        end
    end
    
    % copy functional images
    if ( copy_content == 0 || copy_content == 2 )
        if ( exist(fullfile(foldername,subject,'funk'),'dir') )
            if ( ~exist(fullfile(to_foldername,subject,'funk'),'dir') )
                mkdir(fullfile(to_foldername,subject,'funk'))
            end
            copyfile(fullfile(foldername,subject,'funk'),fullfile(to_foldername,subject,'funk'))
        end
    end
    
    % copy logfiles
    if ( copy_content == 0 || copy_content == 3 )
        if ( exist(fullfile(foldername,subject,'logfiles'),'dir') )
            if ( ~exist(fullfile(to_foldername,subject,'logfiles'),'dir') )
                mkdir(fullfile(to_foldername,subject,'logfiles'))
            end
            copyfile(fullfile(foldername,subject,'logfiles'),fullfile(to_foldername,subject,'logfiles'))
        end
    end
end