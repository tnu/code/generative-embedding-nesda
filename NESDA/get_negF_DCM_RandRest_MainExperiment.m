function get_negF_DCM_RandRest_MainExperiment(experiment,SPM_prep,include_DLPFC,basis_set,onCluster)
% Get the negative free energy for all the subjects, models and folds. This
% should speed up subsequent Bayesian model averaging (BMA).
% 
% Input:
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   SPM_prep        -- (0) NESDA pipeline (1) SPM preprocessing
%   include_DLPFC   -- (0) not include DLPFC, (1) include DLPFC
%   basis_set       -- basis set: (0) only HRF, (1) temporal derivative, (2) informed basis set
%   onCluster       -- operation system: (1) Euler, (0) local machine
%
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2016
% Copyright 2016 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
exp_folders = {'FACES/','ToL/'};

% set folders
SPM_folder           = {'','SPM_preprocessing/'};
DLPFC_name           = {'','_DLPFC'};
setting_folder       = {'woDLPFC','wDLPFC'};
basis_folder         = {'HRF','TD','IBS'};


% set the SPM path and the path to the experiment
addpath(FilenameInfo.SPM_path)
foldername      = fullfile(FilenameInfo.DataPath,exp_folders{experiment});
homefolder      = FilenameInfo.Exp_path;


% define some variables
spm('Defaults','fMRI');
spm_jobman('initcfg');


% get the subject list
Subject_List = [dir(fullfile(foldername,'1*')); dir(fullfile(foldername,'2*')); dir(fullfile(foldername,'3*'))];

% define the name of the different models
name = {'model_FF_INTER','model_FB_INTER','model_RC_INTER',...
    'model_FF_noINTER','model_FB_noINTER','model_RC_noINTER',...
    'model_noMOD'};


% asign an old subject list
Subject_List_old = Subject_List;
Subject_List     = [];

% get the classes of the subjects
if ( onCluster == 1 )
    temp = load(fullfile(homefolder,'LCGA_classes.mat'));
else
    temp = load(fullfile(homefolder,'LCGA_classes','LCGA_classes.mat'));
end
LCGA_classes = temp.LCGA_classes;

% get all the included subjects in Schmaal et al. (2015)
if ( onCluster == 1 )
    temp2 = xlsread(fullfile(homefolder,'AllSubjects_Faces.xlsx'));
else
    temp2 = xlsread(fullfile(homefolder,'information','AllSubjects_Faces.xlsx'));
end
AllSubjects_Schmaal = temp2;

% names of all subjects
Subject_List_old_name = cell(length(Subject_List_old),1);

% find the subjects that are also listed in the LCGA classes
for int = 1:length(Subject_List_old)

    % asign subject names
    Subject_List_old_name{int} = Subject_List_old(int).name;

    % find the index of the subject in the LCGA classes
    index_LCGA = find(LCGA_classes(:,1) == str2double(Subject_List_old(int).name),1);

    if ( ~isempty(index_LCGA) )
        
        % get the subject that were included in Schmaal et al.
        index = find(AllSubjects_Schmaal == str2double(Subject_List_old(int).name),1);
        
        % check whether there is DCM data
        existDCM = exist(fullfile(foldername,Subject_List_old(int).name,'FirstLevel_DCM_RandRestart','spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth',['DCM_NESDA_' name{1} DLPFC_name{include_DLPFC+1} '_local_001.mat']),'file');

        % asign the subject names and class labels
        if ( ~isempty(index) )
            if ( ~isempty(index_LCGA) && existDCM )
                if ( isempty(Subject_List) )
                    Subject_List(1).name       = Subject_List_old(int).name;
                    LCGA_classes_restricted(1) = LCGA_classes(index_LCGA,2);
                else
                    Subject_List(end+1).name       = Subject_List_old(int).name;
                    LCGA_classes_restricted(end+1) = LCGA_classes(index_LCGA,2);
                end
            end
        end
    end
end

% display the number of subjects in the restricted sample
disp(length(Subject_List))


% subject counter
s = 0;

% missing subjects
sub_missing = [];


% get the number of random restarts
NR_RandRest    = 100; 
F_temp         = NaN(length(name),NR_RandRest,length(Subject_List));

    
% load all the models of all the subjects
for subject = 1:length(Subject_List)
    
    % get the subject name
    Subject = Subject_List(subject).name;
    
    % define the folder where the SPM.mat (DCM regressors) is stored
    DCM_regressors_foldername = fullfile(foldername,Subject,'FirstLevel_DCM_RandRestart');
    
    % check whether models have been estimated for that subject
    if ( exist(fullfile(DCM_regressors_foldername,'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth',['DCM_NESDA_' name{1} DLPFC_name{include_DLPFC+1} '_local_001.mat']),'file') )

        % increase subject counter
        s = s + 1;

        % display progress
        disp(['Get negative free energies from Subject ' Subject])
        
        % asign the subjects models
        for model_number = 1:length(name)
            
            % check whether all files are present and get neg. free energy
            for restart = 1:NR_RandRest
                restart_name = ['00' num2str(restart)];
                restart_name = restart_name(end-2:end);

                % filename
                filename_temp = fullfile(DCM_regressors_foldername,'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth',['DCM_NESDA_' name{model_number} DLPFC_name{include_DLPFC+1} '_local_' restart_name '.mat']);

                try
                    load(filename_temp);
                    F_temp(model_number,restart,s) = DCM.F;
                catch
                    sub_missing     = [sub_missing, {Subject}];
                end
            end
        end
    end
end


% display whether there are subjects missing
if ( ~isempty(sub_missing) )
    disp(sub_missing)
end

% set the folder where to store negative free energies
BMS_folder = fullfile(foldername,'DCM_GroupAnalysis','spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal','Bayesian_model_averaging_RandRest_single',[setting_folder{include_DLPFC+1} '_local']);

% asign the results
F_all = F_temp;

% create the folder
if ( ~exist(BMS_folder,'dir') )
    mkdir(BMS_folder)
end

% store the results
if ( ~isempty(F_all) )
    save(fullfile(BMS_folder,'F_all.mat'),'F_all')
end

end