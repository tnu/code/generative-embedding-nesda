function plot_performance_curves_MainExperiment(FilenameInfo,model,experiment,regress_covariates,accuracy_mode)
% Plot the receiver operating characteristic (ROC) curve, the precision-
% recall curve, as well as the accuracy-reject curve for the different
% features (i.e., DCM parameter estimates, functional connectivity,
% local BOLD activation) and for the different subgroups (pair-wise
% comparisons). The function also outputs various measures, such as the
% area under the curve (AUC), sensitivity and specificity, etc. as
% indicators of classifier performance. 
% 
% Input:
%   FilenameInfo        -- important definitions for the experiment
%   model               -- model to be analyzed
%   experiment          -- (1) FACES dataset, (2) Tower of London dataset
%   regress_covariates	-- regress out covariates (default: 0)
%   accuracy_mode       -- (1) sample accuracy, (2) balanced accuracy
%
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% clear and close all
% close all


% set the variables
SPM_prep            = 1;
include_DLPFC       = 0;
basis_set           = 2;
version             = [];

% if FilenameInfo not specified, try to load
if ( isempty(FilenameInfo) )
    
    % get the path
    m_path = mfilename('fullpath');
    m_path = m_path(1:find(m_path=='/',1,'last'));
    
    try
        load(fullfile(m_path,'ConfigFile.mat'))
    catch err
        disp('Need to specify FilenameInfo containing paths!')
        rethrow(err)
    end
end


% define the name of the different models
name = {'BMA','model_FF_INTER','model_FB_INTER','model_RC_INTER','model_FF_noINTER',...
    'model_FB_noINTER','model_RC_noINTER','model_noMOD'};

% set the experiment
exp_folder = {'FACES/','ToL/'};

% set folders
SPM_folder          = {'','SPM_preprocessing/'};
DLPFC_name          = {'','_DLPFC'};
basis_folder        = {'HRF','TD','IBS',''};
settings_folder    	= {'woDLPFC','wDLPFC'};
regress_name        = {'','_orth'};

% set the SPM path and the path to the experiment
foldername = [FilenameInfo.DataPath exp_folder{experiment}];

% specify the model folder name
if ( isempty(model) )
    model_folder  = 'Bayesian_model_selection_RandRest';
elseif ( model == 0 )
    model_folder  = 'Bayesian_model_averaging_RandRest_single';
else
    model_folder  = 'IndividualModel_RandRest';
end


% load the classifcation results
temp    = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding' regress_name{regress_covariates+1} name{model+1} DLPFC_name{include_DLPFC+1} '_local.mat']));
results = temp.results;

% % get the ROC curve for the different classifiers (for the CHR vs REM group)
% [x,y,~,~]     	= perfcurve(results(1).class.true_labels,results(1).class.predicted_scores(:,1),2);
% [x_FC,y_FC,~,~] = perfcurve(results(1).class.true_labels,results(1).class_CI.predicted_scores(:,1),2);
% [x_CI,y_CI,~,~] = perfcurve(results(1).class.true_labels,results(1).class_CI.predicted_scores(:,1),2);
% 
% 
% figure
% ha(1) = plot(x,y,'LineWidth',2);
% hold on
% ha(2) = plot(x_FC,y_FC,'LineWidth',2);
% ha(3) = plot(x_CI,y_CI,'LineWidth',2);
% plot([0 1],[0 1],'k:')
% set(gca,'xtick',0:0.2:1)
% set(gca,'ytick',0:0.2:1)
% 
% ylabel('True positive rate','FontSize',14)
% xlabel('False positive rate','FontSize',14)
% title('ROC curves (CHR)','FontSize',16)
% axis square
% box off
% legend(ha,{'DCM','FC','CI'},'Location','SE','box','off')


% check if figure folder exists
if ( ~exist(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local']),'dir') )
    mkdir(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local']))
else
    
    % delete previous plots of necessary
    if ( exist(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],['Performance_curve' regress_name{regress_covariates+1} '.ps']),'file') )
        delete(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],['Performance_curve' regress_name{regress_covariates+1} '.ps']))
    end
end

% switch off warnings
warning off

% compute ROC curves for the different subgroups
[x,y,~,auc]     = perfcurve(results(1).class.true_labels,results(1).class.predicted_scores(:,1),2);
[x2,y2,~,auc2]	= perfcurve(results(2).class.true_labels,results(2).class.predicted_scores(:,1),2);
[x3,y3,~,auc3]  = perfcurve(results(3).class.true_labels,results(3).class.predicted_scores(:,1),2);

% compute precision-recall curves for the different subgroups
[x_RP,y_RP,~,~]     = perfcurve(results(1).class.true_labels,results(1).class.predicted_scores(:,1),2, 'xCrit', 'reca', 'yCrit', 'prec');
[x2_RP,y2_RP,~,~]	= perfcurve(results(2).class.true_labels,results(2).class.predicted_scores(:,1),2, 'xCrit', 'reca', 'yCrit', 'prec');
[x3_RP,y3_RP,~,~]	= perfcurve(results(3).class.true_labels,results(3).class.predicted_scores(:,1),2, 'xCrit', 'reca', 'yCrit', 'prec');

% sensitivity / recall
sen     = results(1).class.C(2,2)/sum(results(1).class.C(2,:));
sen2	= results(2).class.C(2,2)/sum(results(2).class.C(2,:));
sen3	= results(3).class.C(2,2)/sum(results(3).class.C(2,:));

% specificity
spec    = results(1).class.C(1,1)/sum(results(1).class.C(1,:));
spec2	= results(2).class.C(1,1)/sum(results(2).class.C(1,:));
spec3	= results(3).class.C(1,1)/sum(results(3).class.C(1,:));

% positive predictive value / precision
prec    = results(1).class.C(2,2)/sum(results(1).class.C(:,2));
prec2	= results(2).class.C(2,2)/sum(results(2).class.C(:,2));
prec3	= results(3).class.C(2,2)/sum(results(3).class.C(:,2));

% negative predictive value
npv     = results(1).class.C(1,1)/sum(results(1).class.C(:,1));
npv2	= results(2).class.C(1,1)/sum(results(2).class.C(:,1));
npv3	= results(3).class.C(1,1)/sum(results(3).class.C(:,1));

% for 0/0 -> set NPV to 0
if ( ~isfinite(npv2) )
    npv2 = 0;
end


% plot the different ROC curves
figure
ha(1) = plot(x,y,'.-','LineWidth',2);
hold on
ha(2) = plot(x3,y3,'.-','LineWidth',2);
plot([0 1],[0 1],'k:')
xlim([0 1])
ylim([0 1])
set(gca,'xtick',0:0.2:1)
set(gca,'ytick',0:0.2:1)

ylabel('True positive rate','FontSize',14)
xlabel('False positive rate','FontSize',14)
title('ROC curves','FontSize',16)
axis square
box off
legend(ha,{'CHR\_REM','IMP\_REM'},'Location','SE','box','off')
print(gcf, '-dpsc', fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],['Performance_curve' regress_name{regress_covariates+1}]), '-fillpage', '-append')


% plot the different precision-recall curves
figure
ha(1) = plot(x_RP,y_RP,'.-','LineWidth',2);
hold on
ha(2) = plot(x3_RP,y3_RP,'.-','LineWidth',2);
plot([0 1],[0.5 0.5],'k:')
xlim([0 1])
ylim([0 1])
set(gca,'xtick',0:0.2:1)
set(gca,'ytick',0:0.2:1)

ylabel('positive predictive value (precision)','FontSize',14)
xlabel('recall','FontSize',14)
title('precision-recall curves','FontSize',16)
axis square
box off
legend(ha,{'CHR\_REM','IMP\_REM'},'Location','SE','box','off')
print(gcf, '-dpsc', fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],['Performance_curve' regress_name{regress_covariates+1}]), '-fillpage', '-append')


% compute accuracy-reject curve for the different subgroups
for comp_groups = 1:length(results)
    
    % get the sorted scores
    [~ ,indices_sorted] = sort(abs(results(comp_groups).class.predicted_scores));
    
    % define step size
    step_size = 1/length(indices_sorted);
    
    % get true and predicted labels
    labels           = results(comp_groups).class.true_labels;
    predicted_labels = (results(comp_groups).class.predicted_scores(:,1)>0)+1;
    
    % evalaute the no rejection balanced accuracy
    if ( accuracy_mode == 1 )
        accuracy = sum(labels==predicted_labels)/length(labels);
    else
        C = confusionmat(labels, predicted_labels);
        accuracy = (C(1,1)/sum(C(1,:)) + C(2,2)/sum(C(2,:)))/2;
    end
    
    % define temporary array
    y_temp = NaN(1,length(indices_sorted)+1);
    
    % asign balanced accuracy
    y_temp(1) = accuracy;
    
    % successively remove subjects with lowest "confidence"
    for NR_remove = 1:length(indices_sorted)-1
        
        % get respective indices indices
        indices_all = 1:length(results(comp_groups).class.true_labels);
        indices_cut = setdiff(indices_all,indices_sorted(1:NR_remove));
        
        % get the respective true and predicted lables
        labels           = results(comp_groups).class.true_labels(indices_cut);
        predicted_labels = (results(comp_groups).class.predicted_scores(indices_cut,1)>0)+1;
        
        % evalaute the no rejection balanced accuracy
        if ( accuracy_mode == 1 )
            accuracy = sum(labels==predicted_labels)/length(labels);
        else
            C = confusionmat(labels, predicted_labels);
            if ( isscalar(C) || isscalar(labels) )
                accuracy = sum(labels==predicted_labels)/length(labels);
            else
                accuracy = (C(1,1)/sum(C(1,:)) + C(2,2)/sum(C(2,:)))/2;
            end
        end
        
        % asign balanced accuracy
        y_temp(NR_remove+1) = accuracy;
        
    end
    
    % final value
    y_temp(end) = 1;
    
    % asign the result
    if ( comp_groups == 1 )
        y_AR = y_temp;
        x_AR = 0:step_size:1;
    elseif ( comp_groups == 2 )
        y2_AR = y_temp;
        x2_AR = 0:step_size:1;
    elseif ( comp_groups == 3 )
        y3_AR = y_temp;
        x3_AR = 0:step_size:1;
    end
end

% plot the different accuracy-reject curves
figure
ha(1) = plot(x_AR,y_AR,'.-','LineWidth',2);
hold on
ha(2) = plot(x3_AR,y3_AR,'.-','LineWidth',2);
xlim([0 1])
ylim([0 1])
set(gca,'xtick',0:0.2:1)
set(gca,'ytick',0:0.2:1)

if ( accuracy_mode == 1 )
    ylabel('Accuracy','FontSize',14)
else
    ylabel('Balanced Accuracy','FontSize',14)
end
xlabel('Rejection rate','FontSize',14)
title('accuracy-rejection curve','FontSize',16)
axis square
box off
legend(ha,{'CHR\_REM','IMP\_REM'},'Location','SW','box','off')
print(gcf, '-dpsc', fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],['Performance_curve' regress_name{regress_covariates+1}]), '-fillpage', '-append')


% output the different areas under the curve
fprintf('\nReceiver operating characteristic (ROC):\n')
fprintf('\t\t\tCHR vs. REM\t\tCHR vs. IMP\t\tIMP vs. REM\n')
fprintf('Accuracy: \t\t %f\t\t %f\t\t %f\n',results(1).class.sample_accuracy,results(2).class.sample_accuracy,results(3).class.sample_accuracy);
fprintf('Balanced accuracy: \t %f\t\t %f\t\t %f\n',results(1).class.balanced_accuracy,results(2).class.balanced_accuracy,results(3).class.balanced_accuracy);
fprintf('Area under the curve: \t %f\t\t %f\t\t %f\n',auc,auc2,auc3);
fprintf('Sensitivity / recall: \t %f\t\t %f\t\t %f\n',sen,sen2,sen3);
fprintf('Specificity: \t\t %f\t\t %f\t\t %f\n',spec,spec2,spec3);
fprintf('PPV / precision: \t %f\t\t %f\t\t %f\n',prec,prec2,prec3);
fprintf('NPV: \t\t\t %f\t\t %f\t\t %f\n',npv,npv2,npv3);

end