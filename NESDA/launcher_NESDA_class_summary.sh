#! /bin/bash

set -e

# Load the matlab module (Matlab 2016b)
module load new matlab/9.1


# create a logs folder (for relevant outputs)
mkdir -p logs


# Evaluate the balanced accuracy and significance of classifier based on the permutation tests
bsub -W 1:00 -J "job_resultSVMperm" -oo logs/results_SVM_perm matlab -singleCompThread -r "permutation_balanced_accuracy_SVM_MainExperiment([],0,1,0)"
bsub -W 1:00 -J "job_resultSVMperm" -oo logs/results_SVM_perm_age matlab -singleCompThread -r "permutation_balanced_accuracy_SVM_MainExperiment([],0,1,1)"


# Plot the performance curves (ROC, RP, AR)
bsub -W 1:00 -J "job_resultSVMperfcurve" -oo logs/results_SVM_perfcurve matlab -singleCompThread -r "plot_performance_curves_MainExperiment([],0,1,0,1)"
bsub -W 1:00 -J "job_resultSVMperfcurve" -oo logs/results_SVM_perfcurve_age matlab -singleCompThread -r "plot_performance_curves_MainExperiment([],0,1,1,1)"


# Draw the generative score space
bsub -W 1:00 -R "rusage[mem=14000]" -J "job_SVMscorespace" -o log_SVMscorespace matlab -singleCompThread -r "generative_score_space_SVM_MainExperiment([],0,1,[])"


# Perform permutation tests on the generative score space
bsub -W 2:00 -J "job_SVMscorespaceperm" -oo logs/results_SVM_permScoreSpace matlab -singleCompThread -r "permutation_generative_score_space_SVM_MainExperiment([],0,1,1)"


# Transform feature weights into interpretable feature patterns 
bsub -W 2:00 -J "job_SVMtransform" -o log_SVMtransform matlab -singleCompThread -r "transform_SVM_weights_to_patterns_MainExperiment([],0,1,1,0,2)"


# Perform analysis on the individual feature weights
bsub -W 2:00 -J "job_SVMweights" -w 'ended("job_SVMscorespaceperm") && ended("job_SVMtransform")' -oo logs/results_SVM_permWeights matlab -singleCompThread -r "evaluate_importance_weights_SVM_MainExperiment([],0,1,[],0,0,0)"
