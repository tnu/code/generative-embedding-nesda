function check_indvidual_coordinates_MainExperiment(experiment,include_DLPFC,basis_set,onCluster)
% Get the center coordinate of the regions of interest (ROIs) for each 
% subject individually from the NESDA study. The function then plots a 3D
% image illustrating the location of the individual ROIs to visually
% inspect the coordinates for outliers.
% 
% Input:
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   include_DLPFC   -- (0) not include DLPFC, (1) include DLPFC
%   basis_set     	-- basis set: (0) only HRF, (1) temporal derivative, (2) informed basis set
%   onCluster       -- (0) local machine, (1) Euler
% 
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2017
% Copyright 2017 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------



% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
exp_folders = {'FACES/','ToL/'};

% set the preprocessing folder
DLPFC_name      = {'','_DLPFC'};
basis_folder    = {'HRF','TD','IBS',''};
settings_folder	= {'woDLPFC','wDLPFC'};


% foldername for the center coordinates
foldername      = fullfile(FilenameInfo.DataPath,exp_folders{experiment});
homefolder    	= FilenameInfo.Exp_path;
VoI_foldername  = fullfile(foldername,'VoI_coordinates','SPM_preprocessing',basis_folder{basis_set+1},'NeuroSynth','Schmaal');

% load the coordinates file
temp                  = load(fullfile(VoI_foldername,['VoI_CenterCoordinates' DLPFC_name{include_DLPFC+1} '_local_AllSubjects.mat']));
center_allregions     = temp.center_allregions;
center_allregions_new = cell(size(center_allregions,2),1);


% re-order the data
for subject = 1:size(center_allregions,1)
    
    % store the coordinates in new structure
    if ( ~isempty(center_allregions{subject,1}) )
        for region = 1:size(center_allregions,2)
            center_allregions_new{region}(subject,:) = center_allregions{subject,region};
        end
    else
        for region = 1:size(center_allregions,2)
            center_allregions_new{region}(subject,:) = [NaN NaN NaN];
        end
    end
end


% get the subject list
Subject_List = [dir([foldername '1*']); dir([foldername '2*']); dir([foldername '3*'])];

% asign an old subject list
Subject_List_old = Subject_List;
Subject_List     = [];

% get the classes of the subjects
temp = load(fullfile(homefolder,'LCGA_classes','LCGA_classes.mat'));
LCGA_classes = temp.LCGA_classes;

% get all the included subjects in Schmaal et al. (2015)
temp2 = xlsread(fullfile(homefolder,'information','AllSubjects_Faces.xlsx'));
AllSubjects_Schmaal = temp2;


% specify a indicator vector
vector = zeros(length(Subject_List_old),1);

% find the subjects that are also listed in the LCGA classes
for int = 1:length(Subject_List_old)

    % find the index of the subject in the LCGA classes
    index_LCGA = find(LCGA_classes(:,1) == str2double(Subject_List_old(int).name),1);

    if ( ~isempty(index_LCGA) )
        
        % get only the subjects that were also included in Schmaal et al.
        index = find(AllSubjects_Schmaal == str2double(Subject_List_old(int).name),1);
        
        % check whether there is DCM data
        existData = ~isempty(center_allregions{int,1});

        % asign the subject names and class labels
        if ( ~isempty(index) )
            if ( ~isempty(index_LCGA) && existData )
                
                % set a one in the indicator vector
                vector(int,1) = 1;
                
                % get labels and subjects
                if ( isempty(Subject_List) )
                    Subject_List(1).name       = Subject_List_old(int).name;
                    LCGA_classes_restricted(1) = LCGA_classes(index_LCGA,2);
                    labels(1)                  = LCGA_classes(index_LCGA,3);
                else
                    Subject_List(end+1).name       = Subject_List_old(int).name;
                    LCGA_classes_restricted(end+1) = LCGA_classes(index_LCGA,2);
                    labels(end+1)                  = LCGA_classes(index_LCGA,3);
                end
            end
        end
    end
end

% display the number of subjects in the restricted sample
fprintf(['\nSPM_preprocessing / NeuroSynth / Schmaal / ' basis_folder{basis_set+1}])
fprintf('\nFound: %d subjects\n',length(Subject_List))


% folder for figures
figure_foldername = fullfile(foldername,'Figures','spm12_v7487','SPM_preprocessing','IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local']);


% check if figure folder exists
if ( ~exist(figure_foldername,'dir') )
    mkdir(figure_foldername)
else
    
    % delete previous plots of necessary
    if ( exist(fullfile(figure_foldername,'VoI_Coordinates.ps'),'file') )
        delete(fullfile(figure_foldername,'VoI_Coordinates.ps'))
    end
end


% set the color code
color_style = {'r.','k.','b.','g.','m.','y.','c.'};

% make a logical vector
vector     = vector==1;
NR_regions = [6 7];

% plot the location of the regions in a 3D plot
figure
title([exp_folders{experiment}(1:end-1) ' - Individual Coordinates of ROIs'])
for regions = 1:NR_regions(include_DLPFC+1)
    plot3(center_allregions_new{regions}(vector,1),center_allregions_new{regions}(vector,2),center_allregions_new{regions}(vector,3),color_style{regions})
    hold on
end
view(-20,60)
xlabel('x [mm]')
ylabel('y [mm]')
zlabel('z [mm]')
xlim([-60 60])
ylim([-110 40])
zlim([-40 40])
print(gcf, '-dpsc', fullfile(figure_foldername,'VoI_Coordinates'), '-fillpage', '-append')


% plot the location of the regions in the horizontal view
figure
for regions = 1:NR_regions(include_DLPFC+1)
    plot(center_allregions_new{regions}(vector,1),center_allregions_new{regions}(vector,2),color_style{regions})
    hold on
    plot(mean(center_allregions_new{regions}(vector,1)),mean(center_allregions_new{regions}(vector,2)),[color_style{regions}(1) '+'],'MarkerSize',12,'LineWidth',3)
end
xlabel('x [mm]')


ylabel('y [mm]')
xlim([-60 60])
ylim([-110 40])
axis square
title([exp_folders{experiment}(1:end-1) ' - Individual Coordinates of ROIs [horizontal]'])
print(gcf, '-dpsc', fullfile(figure_foldername,'VoI_Coordinates'), '-fillpage', '-append')


% plot the location of the regions in the sagital view
figure
for regions = 1:NR_regions(include_DLPFC+1)
    plot(center_allregions_new{regions}(vector,2),center_allregions_new{regions}(vector,3),color_style{regions})
    hold on
    plot(mean(center_allregions_new{regions}(vector,2)),mean(center_allregions_new{regions}(vector,3)),[color_style{regions}(1) '+'],'MarkerSize',12,'LineWidth',3)
end
xlabel('y [mm]')
ylabel('z [mm]')
xlim([-110 40])
ylim([-40 40])
axis square
title([exp_folders{experiment}(1:end-1) ' - Individual Coordinates of ROIs [sagittal]'])
print(gcf, '-dpsc', fullfile(figure_foldername,'VoI_Coordinates'), '-fillpage', '-append')


% if on local machine
if ( onCluster ~= 0 )

    % convert into the BrainNet format
    fileBrainNet = fullfile(VoI_foldername,'AllCoordinates.node');
    if ( exist(fileBrainNet,'file') )
        delete(fileBrainNet)
    end

    % open an empty file
    fileID = fopen(fileBrainNet,'w');

    % get randomized color
    rng(2406);
    color_BrainNet = randperm(NR_regions(include_DLPFC+1)/2);

    % write the mean coordinates
    for region = 1:NR_regions(include_DLPFC+1)
        for subject = 1:size(center_allregions_new{region},1)

            % get the coordiantes for each region in every subject
            if ( vector(subject) == 1 )
                x_coord = center_allregions_new{region}(subject,1);
                y_coord = center_allregions_new{region}(subject,2);
                z_coord = center_allregions_new{region}(subject,3);

                % write the coordinate and set the color
                fprintf(fileID,[num2str(x_coord) '\t' num2str(y_coord) '\t' num2str(z_coord) '\t' num2str(color_BrainNet(ceil(region/2))) '\t' '1\n']);
            end
        end
    end
    
    % close file again
    fclose(fileID);
    
end

% name of all regions
name_ROI = {'lAMY','rAMY','lFFA','rFFA','lOFA','rOFA'};
name_sig = {'n.s.','sig.'};


% iterate over regions
for regions = 1:length(center_allregions_new)
    
    % dependent variable: coordinates
    X = center_allregions_new{regions}(vector,:);
    
    % group variable
    group = labels';
    
    % perform a one-way MANOVA
    [D,P] = manova1(X,group);
    
    % store all D and P values
    fprintf('Region: %s || %s ( i.e., p(1) = %f, p(2) = %f ) \n',name_ROI{regions},name_sig{D+1},P(1),P(2))
    
end

end
