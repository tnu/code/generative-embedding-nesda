function generative_score_space_SVM_MainExperiment(FilenameInfo,model,experiment,group_display)
% Plots participants in the generative score space (for different subspaces)
% for the different patient subgroups - namely fast remission, gradualy 
% improving, and chronic. The script takes the DCM posterior parameter 
% estimates and the feature weights from the support vector machines (SVM).
%
% Input:
%   FilenameInfo    -- important definitions for the experiment
%   model           -- model to be analyzed
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   group_display   -- which group comparison to display
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% close all figures
close all


% load the paths if necessary
if ( isempty(FilenameInfo) )
    
    % get the path
    m_path = mfilename('fullpath');
    m_path = m_path(1:find(m_path=='/',1,'last'));

    try
        load(fullfile(m_path,'ConfigFile.mat'))
    catch err
        disp('Need to specify FilenameInfo containing paths!')
        rethrow(err)
    end
end


% default settings
SPM_prep        = 1;
include_DLPFC   = 0;
basis_set       = 2;
version         = [];

% set the experiment
exp_folder = {'FACES/','ToL/'};

% set folders
SPM_folder          = {'','SPM_preprocessing/'};
DLPFC_name          = {'','_DLPFC'};
basis_folder        = {'HRF','TD','IBS',''};
settings_folder    	= {'woDLPFC','wDLPFC'};


% add path
addpath(genpath(fullfile(FilenameInfo.MATLAB_path,'PBAR')))
addpath(genpath(fullfile(FilenameInfo.MATLAB_path,'bipolar_colormap')))
addpath(genpath(fullfile(FilenameInfo.MATLAB_path,'append_pdfs')))


% set the SPM path and the path to the experiment
foldername = [FilenameInfo.DataPath exp_folder{experiment}];


% define the name of the different models
name = {'BMA','model_FF_INTER','model_FB_INTER','model_RC_INTER','model_FF_noINTER',...
    'model_FB_noINTER','model_RC_noINTER','model_noMOD'};

% specify the model folder name
if ( isempty(model) )
    model_folder  = 'Bayesian_model_selection_RandRest';
elseif ( model == 0 )
    model_number  = 0; 
    model_folder  = 'Bayesian_model_averaging_RandRest_single';
else
    model_number  = model;
    model_folder  = 'IndividualModel_RandRest';
end

group_names = {'CHR\_REM','CHR\_IMP','IMP\_REM'};
Emo_names   = {'happy','angry','fear','sad'};

% which figures to plot
if ( isempty(group_display) )
    group_display = 1:3;
end



% load the actual classifcation results
if ( ~isempty(model) )
    Effect = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local.mat']));
else
    Effect = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbeddingBMS' DLPFC_name{include_DLPFC+1} '_local.mat']));
end


% define the results arrays
Weights                     = cell(size(Effect.results,1),1);
Features                    = cell(size(Effect.results,1),1);
DecisionValue_Emo           = cell(size(Effect.results,1),4);
DecisionValue_All           = cell(size(Effect.results,1),3);
DecisionValue_All_allFolds  = cell(size(Effect.results,1),3);


% check if figure folder exists
if ( ~exist(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local']),'dir') )
    mkdir(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local']))
else
    
    % delete previous plots of necessary
    if ( exist(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_GenerativeScoreSpace.ps'),'file') )
        delete(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_GenerativeScoreSpace.ps'))
    end
    
    % delete previous plots of necessary
    if ( exist(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_GenerativeScoreSpace_AllFolds.ps'),'file') )
        delete(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_GenerativeScoreSpace_AllFolds.ps'))
    end
    
    % delete previous plots of necessary
    if ( exist(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_GenerativeScoreSpace_2D.ps'),'file') )
        delete(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_GenerativeScoreSpace_2D.ps'))
    end
end

% switch off warnings
warning off

% number of endogenous connections (to select correct parameters)
NR_Amatrix = 20;


% define the different marker types and colors
marker_type  = {'.','s','^'};
marker_color = {[0.2 0.2 0.2],[0.2 0.2 0.9],[0.9 0.2 0.2]};
marker_size  = [45 15 15];
marker_size2 = [30 10 10];

% define the group combinations
group_comb = {[1 3],[1 2],[2,3]}; 


% obtain actual weights and comupte p-values
for group_comp = 1:size(Effect.results,2)
    
    % actual weights
    Weights{group_comp}  = mean(Effect.results(group_comp).class.weights);
    Features{group_comp} = Effect.results(group_comp).class.features;
    
    % compute dot products of weights and features
    for s = 1:size(Features{group_comp},1)
        
        % endogenous, modulatory (emotion), driving
        DecisionValue_All{group_comp,1}(s) = Weights{group_comp}(1:NR_Amatrix) * Features{group_comp}(s,1:NR_Amatrix)';
        DecisionValue_All{group_comp,2}(s) = Weights{group_comp}(NR_Amatrix+1:NR_Amatrix+56) * Features{group_comp}(s,NR_Amatrix+1:NR_Amatrix+56)';
        DecisionValue_All{group_comp,3}(s) = Weights{group_comp}(NR_Amatrix+57:length(Weights{group_comp})) * Features{group_comp}(s,NR_Amatrix+57:length(Weights{group_comp}))';
        
        % endogenous, modulatory (emotion), driving (all folds)
        for nrFold = 1:size(Effect.results(group_comp).class.weights,1)
            DecisionValue_All_allFolds{group_comp,1}(nrFold,s) = Effect.results(group_comp).class.weights(nrFold,1:NR_Amatrix) * Features{group_comp}(s,1:NR_Amatrix)';
            DecisionValue_All_allFolds{group_comp,2}(nrFold,s) = Effect.results(group_comp).class.weights(nrFold,NR_Amatrix+1:NR_Amatrix+56) * Features{group_comp}(s,NR_Amatrix+1:NR_Amatrix+56)';
            DecisionValue_All_allFolds{group_comp,3}(nrFold,s) = Effect.results(group_comp).class.weights(nrFold,NR_Amatrix+57:length(Weights{group_comp})) * Features{group_comp}(s,NR_Amatrix+57:length(Weights{group_comp}))';
        end
        
        % emotion-specific categories
        DecisionValue_Emo{group_comp,1}(s) = Weights{group_comp}(NR_Amatrix+1:NR_Amatrix+14) * Features{group_comp}(s,NR_Amatrix+1:NR_Amatrix+14)';
        DecisionValue_Emo{group_comp,2}(s) = Weights{group_comp}(NR_Amatrix+15:NR_Amatrix+28) * Features{group_comp}(s,NR_Amatrix+15:NR_Amatrix+28)';
        DecisionValue_Emo{group_comp,3}(s) = Weights{group_comp}(NR_Amatrix+29:NR_Amatrix+42) * Features{group_comp}(s,NR_Amatrix+29:NR_Amatrix+42)';
        DecisionValue_Emo{group_comp,4}(s) = Weights{group_comp}(NR_Amatrix+33:NR_Amatrix+56) * Features{group_comp}(s,NR_Amatrix+33:NR_Amatrix+56)';
        
    end
    
    % plot figures
    if ( any(group_comp == group_display) )
    
        % clear the variables
        clear Y X

        % asign the values (1 = endogenous, 2 = modulatory, 3 = driving)
        Y(:,1) = DecisionValue_All{group_comp,1};
        Y(:,2) = DecisionValue_All{group_comp,2};
        Y(:,3) = DecisionValue_All{group_comp,3};
        X      = Effect.results(group_comp).class.true_labels;
        
        % train the classifier (no hyperparameter optimization)
        Md1 = fitcsvm(Y,X,'KernelFunction','linear','OptimizeHyperparameters','no');
        
        % set number of sample points (fine resolution)
        Nr_samples = 200;

        % generate grid for predictions at finer sample rate
        [x, y, z] = meshgrid(linspace(min(Y(:,1)),max(Y(:,1)),Nr_samples),linspace(min(Y(:,2)),max(Y(:,2)),Nr_samples),linspace(min(Y(:,3)),max(Y(:,3)),Nr_samples));
        xGrid = [x(:),y(:),z(:)];

        % get scores, f
        [ ~ , f] = predict(Md1,xGrid);

        % reshape to same grid size as the input
        f = reshape(f(:,2), size(x));
        
        % plot decision surface
        [faces,verts,~] = isosurface(x, y, z, f, 0, x);
        
        
        % plot the generative score space (3D)
        figure('units','normalized','outerposition',[0 0 1 1]);
        ha(1) = plot3(Y(X==1,1),Y(X==1,2),Y(X==1,3),marker_type{group_comb{group_comp}(1)},'Color',marker_color{group_comb{group_comp}(1)},'MarkerSize',marker_size(group_comb{group_comp}(1)));
        hold on
        ha(2) = plot3(Y(X==2,1),Y(X==2,2),Y(X==2,3),marker_type{group_comb{group_comp}(2)},'Color',marker_color{group_comb{group_comp}(2)},'MarkerSize',marker_size(group_comb{group_comp}(2)));
        axis square
        grid on
        view(64,24)
        set(gca,'FontSize',18)
        xlabel('endogenous (fixed)')
        ylabel('modulatory (emotions)')
        zlabel('driving (all faces)')
        title(['Generative score space - group: ' group_names{group_comp}])
        legend(ha,{group_names{group_comp}(1:3),group_names{group_comp}(6:8)})
        
        h = gcf;
        set(h,'PaperOrientation','landscape');
        print(gcf, '-dpsc', fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_GenerativeScoreSpace'), '-fillpage', '-append')
        
        
        % subplot setting
        sub_setting = 4;
        
        % plot the generative score space (3D) for all folds
        for nrFold = 1:size(DecisionValue_All_allFolds{group_comp,1},1)
            
            % asign the values (1 = endogenous, 2 = modulatory, 3 = driving)
            Y(:,1) = DecisionValue_All_allFolds{group_comp,1}(nrFold,:);
            Y(:,2) = DecisionValue_All_allFolds{group_comp,2}(nrFold,:);
            Y(:,3) = DecisionValue_All_allFolds{group_comp,3}(nrFold,:);
            X      = Effect.results(group_comp).class.true_labels;
            
            % new figure
            if ( mod(nrFold,sub_setting^2) == 1 )
                counter = 0;
                figure('units','normalized','outerposition',[0 0 1 1]);
            end
            
            % increase the counter
            counter = counter + 1;
            
            % plot the generative score space (3D)
            subplot(sub_setting,sub_setting,counter)
            ha(1) = plot3(Y(X==1,1),Y(X==1,2),Y(X==1,3),marker_type{group_comb{group_comp}(1)},'Color',marker_color{group_comb{group_comp}(1)},'MarkerSize',marker_size2(group_comb{group_comp}(1)));
            hold on
            ha(2) = plot3(Y(X==2,1),Y(X==2,2),Y(X==2,3),marker_type{group_comb{group_comp}(2)},'Color',marker_color{group_comb{group_comp}(2)},'MarkerSize',marker_size2(group_comb{group_comp}(2)));
            axis square
            grid on
            view(64,24)
            set(gca,'FontSize',14)
            if ( counter == 1 )
                xlabel('endogenous (fixed)')
                ylabel('modulatory (emotions)')
                zlabel('driving (all faces)')
            end

            % save the figure
            if ( mod(nrFold,sub_setting^2) == 0 || nrFold == size(DecisionValue_All_allFolds{group_comp,1},1) )
                h = gcf;
                set(h,'PaperOrientation','landscape');
                print(gcf, '-dpsc', fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_GenerativeScoreSpace_AllFolds'), '-fillpage', '-append')
            end
        end
        
        
        % get the normale vector of the plane
        P1 = verts(1,:);
        P2 = verts(2,:);
        P3 = verts(3,:);
        normal = cross(P1-P2, P1-P3);
        
        % get the coefficients of the plane equation
        a = normal(1);
        b = normal(2);
        c = normal(3);
        d = P1(1)*a + P1(2)*b + P1(3)*c;
        
        % plot the generative score space (2D)
        figure('units','normalized','outerposition',[0 0 1 1]);
        subplot(1,3,1)
        ha(1) = plot(Y(X==1,1),Y(X==1,2),marker_type{group_comb{group_comp}(1)},'Color',marker_color{group_comb{group_comp}(1)},'MarkerSize',marker_size(group_comb{group_comp}(1)));
        hold on
        ha(2) = plot(Y(X==2,1),Y(X==2,2),marker_type{group_comb{group_comp}(2)},'Color',marker_color{group_comb{group_comp}(2)},'MarkerSize',marker_size(group_comb{group_comp}(2)));
        symmetrize_axis()
        xlimits = xlim();
        ylimits = ylim();
        xlim([xlimits(1) xlimits(2)])
        ylim([ylimits(1) ylimits(2)])
        axis square
        grid on
        box off
        set(gca,'FontSize',16)
        xlabel('endogenous (fixed)')
        ylabel('modulatory (emotions)')

        subplot(1,3,2)
        ha(1) = plot(Y(X==1,1),Y(X==1,3),marker_type{group_comb{group_comp}(1)},'Color',marker_color{group_comb{group_comp}(1)},'MarkerSize',marker_size(group_comb{group_comp}(1)));
        hold on
        ha(2) = plot(Y(X==2,1),Y(X==2,3),marker_type{group_comb{group_comp}(2)},'Color',marker_color{group_comb{group_comp}(2)},'MarkerSize',marker_size(group_comb{group_comp}(2)));
        symmetrize_axis()
        xlimits = xlim();
        ylimits = ylim();
        xlim([xlimits(1) xlimits(2)])
        ylim([ylimits(1) ylimits(2)])
        axis square
        grid on
        box off
        set(gca,'FontSize',16)
        xlabel('endogenous (fixed)')
        ylabel('driving (all faces)')
        title(['Generative score space - group: ' group_names{group_comp}])

        subplot(1,3,3)
        ha(1) = plot(Y(X==1,2),Y(X==1,3),marker_type{group_comb{group_comp}(1)},'Color',marker_color{group_comb{group_comp}(1)},'MarkerSize',marker_size(group_comb{group_comp}(1)));
        hold on
        ha(2) = plot(Y(X==2,2),Y(X==2,3),marker_type{group_comb{group_comp}(2)},'Color',marker_color{group_comb{group_comp}(2)},'MarkerSize',marker_size(group_comb{group_comp}(2)));
        symmetrize_axis()
        xlimits = xlim();
        ylimits = ylim();
        xlim([xlimits(1) xlimits(2)])
        ylim([ylimits(1) ylimits(2)])
        axis square
        grid on
        box off
        set(gca,'FontSize',16)
        xlabel('modulatory (emotions)')
        ylabel('driving (all faces)')
        h = gcf;
        set(h,'PaperOrientation','landscape');
        print(gcf, '-dpsc', fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'SVM_GenerativeScoreSpace_2D'), '-fillpage', '-append')
        
        
        % combination of emotions
        Emo_combination = combnk(1:size(DecisionValue_Emo,2),3);
        Emo_pairs       = combnk(1:size(DecisionValue_Emo,2),2);
        
        % open figure
        figure('units','normalized','outerposition',[0 0 1 1]);
        
        % show all emotion combinations
        for int = 1:size(Emo_combination,1)
            
            % clear the variable 
            clear Y
        
            % asign the values
            Y(:,1) = DecisionValue_Emo{group_comp,Emo_combination(int,1)};
            Y(:,2) = DecisionValue_Emo{group_comp,Emo_combination(int,2)};
            Y(:,3) = DecisionValue_Emo{group_comp,Emo_combination(int,3)};
            X      = Effect.results(group_comp).class.true_labels;

            % plot the generative score space
            subplot(2,2,int)
            ha(1) = plot3(Y(X==1,1),Y(X==1,2),Y(X==1,3),'.','Color',[0.2 0.2 0.2],'MarkerSize',45);
            hold on
            ha(2) = plot3(Y(X==2,1),Y(X==2,2),Y(X==2,3),'^','Color',[0.9 0.2 0.2],'MarkerSize',15);
            symmetrize_axis()
            axis square
            grid on
            set(gca,'FontSize',18)
            xlabel(Emo_names{Emo_combination(int,1)})
            ylabel(Emo_names{Emo_combination(int,2)})
            zlabel(Emo_names{Emo_combination(int,3)})
            
            % display the legend
            if ( int == 4 )
                legend(ha,{group_names{group_comp}(1:3),group_names{group_comp}(6:8)})
            end
        end
        
        
        % open figure
        figure('units','normalized','outerposition',[0 0 1 1]);
        
        % show all emotion combinations
        for int = 1:size(Emo_pairs,1)
        
            % clear the variable 
            clear Y
            
            % asign the values
            Y(:,1) = DecisionValue_Emo{group_comp,Emo_pairs(int,1)};
            Y(:,2) = DecisionValue_Emo{group_comp,Emo_pairs(int,2)};
            X      = Effect.results(group_comp).class.true_labels;

            % plot the generative score space
            subplot(2,3,int)
            ha(1) = plot(Y(X==1,1),Y(X==1,2),'.','Color',[0.2 0.2 0.2],'MarkerSize',45);
            hold on
            ha(2) = plot(Y(X==2,1),Y(X==2,2),'^','Color',[0.9 0.2 0.2],'MarkerSize',15);
            symmetrize_axis()
            axis square
            grid on
            box off
            set(gca,'FontSize',18)
            xlabel(Emo_names{Emo_pairs(int,1)})
            ylabel(Emo_names{Emo_pairs(int,2)})
            
%             % display the legend
%             if ( int == 4 )
%                 legend(ha,{group_names{group_comp}(1:3),group_names{group_comp}(6:8)})
%             end
        end
    end
end
    
% switch on warnings
warning on

end