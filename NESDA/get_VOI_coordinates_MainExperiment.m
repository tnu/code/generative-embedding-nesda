function get_VOI_coordinates_MainExperiment(experiment,include_DLPFC,basis_set,onCluster)
% Collects the center coordinate of the regions of interest (ROIs) from
% each subjects of the NESDA study. For the FACES dataset, left and right 
% occipital face area has to be located in the inferior occipital gyrus and 
% left and right fusiform face area has to be located in the fusiform
% gyrus.
% 
% Input:
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   include_DLPFC   -- (0) not include DLPFC, (1) include DLPFC
%   basis_set       -- basis set: (0) only HRF, (1) temporal derivative, (2) informed basis set
%   onCluster       -- operation system: (1) Euler, (0) local machine
% 
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2017
% Copyright 2017 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
exp_folders = {'FACES/','ToL/'};

% set the preprocessing folder
DLPFC_name      = {'','_DLPFC'};
basis_folder    = {'HRF','TD','IBS',''};


% set the SPM path and the path to the experiment
addpath(FilenameInfo.SPM_path)
foldername = fullfile(FilenameInfo.DataPath,exp_folders{experiment});


% initialize spm
spm_jobman('initcfg')

% get the subject list
Subject_List = [dir([foldername '1*']); dir([foldername '2*']); dir([foldername '3*'])];

% define the results array
if ( include_DLPFC )
    ROI_names	= {'FFA_left','FFA_right','OFA_left','OFA_right','Amyg_left','Amyg_right','DLPFC_right'};
else
    ROI_names	= {'FFA_left','FFA_right','OFA_left','OFA_right','Amyg_left','Amyg_right'};
end


% specify the coordinate array
center_allregions = cell(length(Subject_List),length(ROI_names));

% define the subjects to analyze
subjects_analyze = 1:length(Subject_List);


% iterate over subjects
for s = subjects_analyze

    % subject name
    Subject = Subject_List(s).name;

    % specify the SPM folder
    if ( onCluster == 1 )
        SPM_foldername = fullfile(foldername,Subject,'FirstLevel','SPM_preprocessing',basis_folder{basis_set+1});
    else
        SPM_foldername = fullfile(foldername,Subject,'SPM_preprocessing',basis_folder{basis_set+1});
    end

    % check if subject has data
    files = dir(fullfile(SPM_foldername,'SPM.mat'));
    run_subject = ~isempty(files) & ~isequal(Subject,'110877') & ~isequal(Subject,'120652') & ~isequal(Subject,'324343');

    % perform BOLD signal time series extraction
    if ( run_subject )

        % time series folder
        timeseries_folder = fullfile(SPM_foldername,'timeseries_VOI_adjusted','NeuroSynth');
        
        % extract the time series for each region of interest
        for number_of_regions = 1:length(ROI_names)

            % load the VOI to get the coordinates
            load(fullfile(timeseries_folder,['VOI_' ROI_names{number_of_regions} '_local_1.mat']))

            % get the center coordinates for the extracted time series
            center_allregions{s,number_of_regions} = xY.xyz';

        end
    end
end

% foldername for the center coordinates
VoI_foldername = fullfile(foldername,'VoI_coordinates','SPM_preprocessing',basis_folder{basis_set+1},'NeuroSynth','Schmaal');

% create the VoI folder
if ( ~exist(VoI_foldername,'dir') )
    mkdir(VoI_foldername)
end

% save the center coordinates in a file
save(fullfile(VoI_foldername,['VoI_CenterCoordinates' DLPFC_name{include_DLPFC+1} '_local_AllSubjects.mat']),'center_allregions')

end