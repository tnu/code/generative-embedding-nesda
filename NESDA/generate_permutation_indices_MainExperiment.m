function generate_permutation_indices_MainExperiment(FilenameInfo,model,experiment,maxNrPerm,version)
% Performs a permutation test to identify features that are important for
% classifcation between the different patient subgroups - namely fast 
% remission, gradualy improving, and chronic. The script uses support 
% vector machines (SVM) in accordance with the classical approach
% used by Kay Brodersen.
%
% Input:
%   FilenameInfo    -- important definitions for the experiment
%   model           -- model to be analyzed
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   maxNrPerm       -- maximum number of permutations
%   version         -- empty
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2016
% Copyright 2016 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% set the random number seed
load('rngSeed.mat')
rng(rngSeed);


% if FilenameInfo not specified, try to load
if ( isempty(FilenameInfo) )
    
    % get the path
    m_path = mfilename('fullpath');
    m_path = m_path(1:find(m_path=='/',1,'last'));
    
    try
        load(fullfile(m_path,'ConfigFile.mat'))
    catch err
        disp('Need to specify FilenameInfo containing paths!')
        rethrow(err)
    end
end


% check version
if ( ~isempty(version) )
    version = [];
end


% default settings
SPM_prep        = 1;
classifier      = 3;
include_DLPFC   = 0;
basis_set       = 2;


% set the experiment
exp_folder = {'FACES/','ToL/'};

% set folders
SPM_folder          = {'','SPM_preprocessing/'};
DLPFC_name          = {'','_DLPFC'};
settings_folder    	= {'woDLPFC','wDLPFC'};
basis_folder        = {'HRF','TD','IBS',''};

% define the classifier name
classifier_name  = {'GPC','SVM','FITCSVM'};

% add path
addpath(genpath(fullfile(FilenameInfo.MATLAB_path,'PBAR')))

% add path for classifiers
if ( classifier == 1 )
    addpath(genpath(fullfile(FilenameInfo.MATLAB_path,'PRoNTo')))
elseif ( classifier == 2 )
    addpath(genpath(fullfile(FilenameInfo.MATLAB_path,'liblinear-2.11')))
end

% set the path of the experiment
exp_foldername	 = FilenameInfo.Exp_path;

% set the SPM path and the path to the experiment
foldername = [FilenameInfo.DataPath exp_folder{experiment}];


% define the name of the different models
name = {'BMA','model_FF_INTER','model_FB_INTER','model_RC_INTER','model_FF_noINTER',...
    'model_FB_noINTER','model_RC_noINTER','model_noMOD'};

% define the different groups
group_names = {'CHR_REM','CHR_IMP','IMP_REM'};
group_comb  = {[3 1],[3 2],[2 1]};

% specify the model folder name
if ( isempty(model) )
    model_folder  = 'Bayesian_model_selection_RandRest';
elseif ( model == 0 )
    model_number  = 0;
    model_folder  = 'Bayesian_model_averaging_RandRest_single';
else
    model_number  = model;
    model_folder  = 'IndividualModel_RandRest';
end

% get the classes of the subjects
temp                = load([exp_foldername 'LCGA_classes/LCGA_classes.mat']);
LCGA_classes        = temp.LCGA_classes;
AllSubjects_Schmaal = xlsread([exp_foldername 'information/AllSubjects_Faces.xlsx']);

% results folders
results_folder = 'DCM_GroupAnalysis';

% load the posterior parameter estimates
if ( ~isempty(model) && model_number ~= 0 )
    temp = load(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['PosteriorParameterEstimates_' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local.mat']));
elseif ( isempty(model) )
    file = dir(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,[settings_folder{include_DLPFC+1} '_local'],'PosteriorParameterEstimates*'));
    temp    = load(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,[settings_folder{include_DLPFC+1} '_local'],file(1).name));
    pxp_max = temp.results.pxp_max;
else
    temp = load(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,[settings_folder{include_DLPFC+1} '_local'],'BMS.mat'));
    
    % bring the results in the same format
    for s = 1:length(temp.BMS.DCM.ffx.bma.mEps)
        
        % endogenous connections
        for int = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.A,1)
            for int2 = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.A,2)
                temp.results.A_Matrix_AllSubjects{int,int2}(s) = temp.BMS.DCM.ffx.bma.mEps{s}.A(int,int2);
            end
        end
        
        % modulatory influences
        for int = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.B,1)
            for int2 = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.B,2)
                for int3 = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.B,3)
                    temp.results.B_Matrix_AllSubjects{int,int2,int3}(s) = temp.BMS.DCM.ffx.bma.mEps{s}.B(int,int2,int3);
                end
            end
        end
        
        % driving inputs
        for int = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.C,1)
            for int2 = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.C,2)
                temp.results.C_Matrix_AllSubjects{int,int2}(s) = temp.BMS.DCM.ffx.bma.mEps{s}.C(int,int2);
            end
        end
    end
    
    % asign the subjects included
    temp.results.AllSubjects = temp.BMS.DCM.ffx.AllSubjects;
    
end


% asign the data
Amatrix_allSubjects     = temp.results.A_Matrix_AllSubjects;


% define a labels array
labels = zeros(1,length(temp.results.AllSubjects));

% dummy number of subjects
nr_subject = [];
found_subjects = [];

for int = 1:length(temp.results.AllSubjects)

    % find the index of the subject in the LCGA classes
    index_LCGA = find(LCGA_classes(:,1) == str2double(temp.results.AllSubjects{int}),1);

    if ( ~isempty(index_LCGA) )
        
        % get subjects that were also included in Schmal et al.
        index = find(AllSubjects_Schmaal == str2double(temp.results.AllSubjects{int}),1);
        
        if ( ~isempty(index) )
            if ( ~isempty(index_LCGA) && isfinite(Amatrix_allSubjects{1,1}(int)) )
                nr_subject(end+1)     = LCGA_classes(index_LCGA,1);
                found_subjects(end+1) = str2double(temp.results.AllSubjects{int});
            end

            % if the subject exists, assign the class label
            if ( isfinite(Amatrix_allSubjects{1,1}(int)) )
                labels(int) = LCGA_classes(index_LCGA,3);
            end
        end
    end
end


% display how many subjects were found
fprintf('\n')
disp(['Found: ' num2str(length(nr_subject)) ' out of ' num2str(length(AllSubjects_Schmaal)) ' subjects - Running: ' classifier_name{classifier} ' / ' basis_folder{basis_set+1}])

% display subject name of missing subject
if ( length(nr_subject) ~= length(AllSubjects_Schmaal) )
    missing_subjects = setdiff(AllSubjects_Schmaal,found_subjects);
    fprintf((~isempty(missing_subjects))+1,'Subjects missing:');
    for int = 1:length(missing_subjects)
        fprintf((~isempty(missing_subjects))+1,' %d',missing_subjects(int));
    end
    fprintf('\n')
end


% default settings
group_comp_all = 1:3;

% results array
PermIndices = cell(1,length(group_comp_all));


% specify the true labels for a binary comparison of groups
for group_comp = group_comp_all
    
    % clear the data
    clear data
    
    % define all labels
    all_labels = 1:3;
    not_included = setdiff(all_labels,group_comb{group_comp});
    
    % define temporary label
    labels_temp = labels;
    labels_temp(labels_temp==not_included) = 0;
    vector_2 = labels_temp==group_comb{group_comp}(2);
    vector_1 = labels_temp==group_comb{group_comp}(1);
    labels_temp(vector_2) = 2;
    labels_temp(vector_1) = 1;
    
    % define the specifiy labels
    labels_group = labels_temp(labels_temp~=0);
    [labels_group, order] = sort(labels_group);
    labels_group = labels_group';

    % define the subjects that should be included
    subjects_analyze = find(labels_temp~=0);
    subjects_analyze = subjects_analyze(order);
    
    % get the subject names
    subjects_group_temp = temp.results.AllSubjects(subjects_analyze);
    subjects_group      = NaN(size(subjects_group_temp));
    for int = 1:length(subjects_group_temp)
        subjects_group(int) = str2double(subjects_group_temp{int});
    end

    % display the setting
    disp(['Group selection: ' group_names{group_comp} ' - Group size: ' num2str(sum(labels_temp==1)) ' vs. ' num2str(sum(labels_temp==2))])

    
    % iterate over permutations
    for NrPerm = 1:maxNrPerm
        
        % shuffle the labels
        PermIndices{group_comp}(NrPerm,:) = randperm(numel(labels_group));
        
    end
end


% create folder
if ( ~exist(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder),'dir') )
    mkdir(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder))
end


% store the results of the generative embedding
if ( ~isempty(model) )
    save(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding_PermutationIndices' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local.mat']),'PermIndices')
else
    save(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding_PermutationIndicesBMS' DLPFC_name{include_DLPFC+1} '_local.mat']),'PermIndices')
end

end
