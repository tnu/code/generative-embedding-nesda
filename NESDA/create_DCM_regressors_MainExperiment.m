function create_DCM_regressors_MainExperiment(subject,experiment,SPM_prep,onCluster)
% Takes the mutliple conditions file containing the onsets, durations and
% names of the different condition for the FACES dataset or the Tower of
% London (ToL) dataset of the NESDA study (Netherlands Study on Depression 
% and Social Anxiety) and creates a new SPM.mat with the respective
% regressors for the DCM analysis.
% 
% This function reads Matlab-file containing onsets and durations for the
% different stimuli and creates a new SPM.mat file that can be used during
% the DCM analyses to specify the inputs.
% 
% Input:
%   subject      	-- subject to be analyzed
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   SPM_prep        -- (0) NESDA pipeline (1) SPM preprocessing
%   onCluster       -- operation system: (1) Euler, (2) local machine
%
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2016
% Copyright 2016 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
exp_folders = {'FACES/','ToL/'};

% set the SPM folder
SPM_folder = {'','SPM_preprocessing/'};

% set the SPM version name
if ( onCluster == 1 ), SPM_version = 'spm12_v7487'; else, SPM_version = 'spm12'; end


% set the SPM path and the path to the experiment
addpath(FilenameInfo.SPM_path)
foldername      = fullfile(FilenameInfo.DataPath,exp_folders{experiment});
home_foldername = FilenameInfo.Exp_path;


% get the subject list
Subject_List = [dir(fullfile(foldername,'1*')); dir(fullfile(foldername,'2*')); dir(fullfile(foldername,'3*'))];

% define the subject to analyze
if ( isempty(subject) )
    subject_analyze = 1:length(Subject_List);
else
    subject_analyze = subject;
end


% iterate over all subjects
for s = subject_analyze

    % get the subject name
    Subject = Subject_List(s).name;

    % display the subject
    disp(['Subject: ' Subject])

    if ( SPM_prep == 0 )
        processed_filename = [Subject '_denoised_tempfilt_mni.nii'];
    else
        processed_filename = ['sw' Subject '_filtered_func_data.nii'];
    end

    if ( ~exist([foldername Subject '/funk/' SPM_folder{SPM_prep+1} processed_filename],'file') )

        % display
        disp([Subject ': No data found...'])

        % exit the function
        exit

    else

        % load the multiple condition file
        temp = load([foldername Subject '/logfiles/faces_design.mat']);

        % create a new regressor with all faces
        [sorted_onsets,indices] = sort([temp.onsets{1};temp.onsets{2};temp.onsets{3};temp.onsets{4};temp.onsets{5}]);
        durations_temp          = [temp.durations{1};temp.durations{2};temp.durations{3};temp.durations{4};temp.durations{5}];

        % create a new regressor with emotional faces
        [sorted_onsets2,indices2] = sort([temp.onsets{1};temp.onsets{2};temp.onsets{3};temp.onsets{4}]);
        durations_temp2           = [temp.durations{1};temp.durations{2};temp.durations{3};temp.durations{4}];

        % define new cell objects
        onsets    = cell(8,1);
        durations = cell(8,1);
        names     = cell(8,1);

        % define the new regressor
        onsets{1}    = sorted_onsets;
        durations{1} = durations_temp(indices);
        names{1}     = 'all_faces';

        onsets{2}    = sorted_onsets2;
        durations{2} = durations_temp2(indices2);
        names{2}     = 'emotional_faces';

        % define the other regressors
        for int = 1:6
            onsets{int+2}    = temp.onsets{int};
            durations{int+2} = temp.durations{int};
            names{int+2}     = temp.names{int};
        end

        % create the DCM folder
        if ( ~exist(fullfile(foldername,Subject,'FirstLevel_DCM',SPM_version,SPM_folder{SPM_prep+1}),'dir') )
            mkdir(fullfile(foldername,Subject,'FirstLevel_DCM',SPM_version,SPM_folder{SPM_prep+1}))
        end

        % save the multiple condition file
        save(fullfile(foldername,Subject,'FirstLevel_DCM',SPM_version,SPM_folder{SPM_prep+1},'faces_DCM_regressors.mat'),'onsets','durations','names')

        % clear the files
        clear onsets durations names

        % load the example Matlab Batch
        load([home_foldername 'Batch_FirstLevelModelSpecifications.mat'])

        % modify the working directory where the SPM.mat should be saved
        matlabbatch{1}.spm.stats.fmri_spec.dir = [];

        % get the correct folder name
        folder = fullfile(foldername,Subject,'FirstLevel_DCM',SPM_version,SPM_folder{SPM_prep+1});

        % create the FirstLevel folder if it does not exist
        if ( ~exist(folder,'dir') )
            mkdir(folder)
        end

        % assign the new filename
        matlabbatch{1}.spm.stats.fmri_spec.dir = {folder};

        % modify the unit of the multiple conditions included in the GLM
        matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';

        % get the TR for the subject
        [names, TR] = textread([home_foldername 'TRs.txt'],'%s %f','delimiter',';');
        if ( any(strcmp(Subject,names)) )
            matlabbatch{1}.spm.stats.fmri_spec.timing.RT = TR(strcmp(Subject,names));
        else
           matlabbatch{1}.spm.stats.fmri_spec.timing.RT = 2.315;
        end


        % delete the filenames of the scans and load the new filenames
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).scans = [];

        % get the correct filenames
        files = spm_select('expand', [foldername Subject '/funk/' SPM_folder{SPM_prep+1} processed_filename]);

        % asign the filenames to matlabbatch
        for int2 = 1:size(files,1)

            % assign the new filename        
            matlabbatch{1}.spm.stats.fmri_spec.sess(1).scans = [matlabbatch{1}.spm.stats.fmri_spec.sess(1).scans; {[files(int2,1:end-3) num2str(int2)]}];

        end

        % delete the filenames of the scans
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi = [];

        % create the new filename
        new_filename = fullfile(foldername,Subject,'FirstLevel_DCM',SPM_version,SPM_folder{SPM_prep+1},'faces_DCM_regressors.mat');

        % assign the new filename        
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi = {new_filename};

        % delete the filenames of the scans
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi_reg = {''};

        % disable temporal and dispersion derivative
        matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0];

        % use the value set at the beginning of the programm
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).hpf = 128;

        % specify the estimation part
        matlabbatch{2}.spm.stats.fmri_est.spmmat = cellstr(fullfile(folder,'SPM.mat'));

        % save the batch
        save(fullfile(foldername,Subject,'FirstLevel_DCM',SPM_version,SPM_folder{SPM_prep+1},'create_DCM_regressors.mat'), 'matlabbatch');

        % check whether SPM already exists
        if ( exist(fullfile(folder,'SPM.mat'),'file') )
            delete(fullfile(folder,'SPM.mat'))
        end

        % run the batch
        spm_jobman('run',matlabbatch)

    end
end

end