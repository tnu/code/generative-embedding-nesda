function check_DCM_fit_accuracy_MainExperiment(FilenameInfo,experiment,SPM_prep,include_DLPFC)
% Display the correlation coefficient for the seven DCMs to control for any 
% systematic flat-lining of the DCMs. Additionally, the function tests 
% whether there are significant differerences between the three groups in 
% model fit.
%
% Input:
%   FilenameInfo    -- impportant definitions for the experiment
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   SPM_prep        -- (0) NESDA pipeline (1) SPM preprocessing
%   include_DLPFC   -- (0) not include DLPFC, (1) include DLPFC
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% if FilenameInfo not specified, try to load
if ( isempty(FilenameInfo) )
    
    % get the path
    m_path = mfilename('fullpath');
    m_path = m_path(1:find(m_path=='/',1,'last'));
    
    try
        load(fullfile(m_path,'ConfigFile.mat'))
    catch
        disp('Need to specify FilenameInfo containing paths!')
        rethrow(err)
    end
end

% set the experiment
exp_folder = {'FACES/','ToL/'};

% set folders
SPM_folder          = {'','SPM_preprocessing/'};
SPM_folder_text     = {'',' / SPMprep'};
DLPFC_name          = {'','_DLPFC'};
settings_folder    	= {'woDLPFC','wDLPFC'};

% add path
addpath(genpath(fullfile(FilenameInfo.MATLAB_path,'PBAR')))

% set the path of the experiment
exp_foldername = FilenameInfo.Exp_path;

% set the SPM path and the path to the experiment
foldername = [FilenameInfo.DataPath exp_folder{experiment}];


% define the name of the different models
name = {'model_FF_INTER','model_FB_INTER','model_RC_INTER','model_FF_noINTER',...
    'model_FB_noINTER','model_RC_noINTER','model_noMOD'};

% specify the model folder name
model_folder  = 'IndividualModel_RandRest';

% specify the models
model_analyze = 1:length(name);


% get the classes of the subjects
temp                = load([exp_foldername 'LCGA_classes/LCGA_classes.mat']);
LCGA_classes        = temp.LCGA_classes;
AllSubjects_Schmaal = xlsread([exp_foldername 'information/AllSubjects_Faces.xlsx']);


% load the posterior parameter estimates
temp = load(fullfile(foldername,'DCM_GroupAnalysis','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',model_folder,[settings_folder{include_DLPFC+1} '_local'],['AccuracyFit_AllModels' DLPFC_name{include_DLPFC+1} '.mat']));

% asign the data
R = temp.results.R;

% define a labels array
labels = zeros(1,length(temp.results.AllSubjects));
vector = zeros(1,length(temp.results.AllSubjects));

% dummy number of subjects
nr_subject = [];

for int = 1:length(temp.results.AllSubjects)

    % find the index of the subject in the LCGA classes
    index_LCGA = find(LCGA_classes(:,1) == str2double(temp.results.AllSubjects{int}),1);

    if ( ~isempty(index_LCGA) )
        
        % restrict to subjects included in Schmaal et al.
        index = find(AllSubjects_Schmaal == str2double(temp.results.AllSubjects{int}),1);
        
        if ( ~isempty(index) )
            if ( ~isempty(index_LCGA) && isfinite(R(int,1)) )
                nr_subject(end+1) = LCGA_classes(index_LCGA,1);
            end

            % if the subject exists, assign the class label
            if ( isfinite(R(int,1)) )
                labels(int) = LCGA_classes(index_LCGA,3);
                vector(int) = 1;
            end
        end
    end
end

% display how many subjects were found
fprintf('\n')
disp(['Found: ' num2str(length(nr_subject)) ' out of ' num2str(length(AllSubjects_Schmaal)) ' subjects'])

% clear the data
clear data


% check if figure folder exists
if ( ~exist(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local']),'dir') )
    mkdir(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local']))
else
    
    % delete previous plots of necessary
    if ( exist(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'DCM_FitAccuracy.ps'),'file') )
        delete(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'DCM_FitAccuracy.ps'))
    end
end

% switch off warnings
warning off


% output the Coefficient of determination
fprintf(['\nCorrelation Coefficient R (spm12_v7487 / NeuroSynth' SPM_folder_text{SPM_prep+1} ' / Schmaal / ' settings_folder{include_DLPFC+1} '):\n'])
fprintf(['\nModel \t\t\t\t','Corr. Coef.','\t\t','Range\n'])

% switch off warnings
warning off

% plot the results simultaneously
figure;
model_counter = 0;

% small deviations for plotting purpose
delta_x = randn(1,sum(vector))/10;

% display the correlation coefficient
for model = model_analyze
    model_counter = model_counter + 1;
    if ( length(name{model}) < 16 )
        fprintf([name{model} '\t\t\t %f \t\t %f - %f \n'],nanmean(R(vector==1,model)),nanmin(R(vector==1,model)),nanmax(R(vector==1,model)))
    else
        fprintf([name{model} '\t\t %f \t\t %f - %f \n'],nanmean(R(vector==1,model)),nanmin(R(vector==1,model)),nanmax(R(vector==1,model)))
    end

    hold on
    plot(model_counter,nanmean(R(vector==1,model)),'k.','MarkerSize',30)
    alpha(1)
    scatter(model_counter.*ones(1,sum(vector))+delta_x,R(vector==1,model),20,'filled','r')
    hold off
    alpha(0.2)
end

axis square
xlim([0 model_counter+1])
ylim([0 1])
set(gca,'ytick',[0 0.5 1],'FontSize',12)
set(gca,'xtick',1:length(model_analyze),'FontSize',12)
xlabel('Model','FontSize',14)
ylabel('corr. coef. (R)','FontSize',14)
print(gcf, '-dpsc', fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],'DCM_FitAccuracy'), '-append');

% switch on warnings
warning on


% define the different groups
group_names_ttest = {'CHR_REM','CHR_IMP','IMP_REM'};
group_comb_ttest  = {[3 1],[3 2],[2 1]};

% define results array
pVal_R_allSubjects          = NaN(1,size(R,2));
pVal_R_posthoc_allSubjects  = cell(1,3);
pVal_R_ANOVA_allSubjects    = NaN(1,size(R,2));


% transform the correlation coefficient values
Z_trans = atanh(R);


% test if fit is significant
for model = 1:size(Z_trans,2)
    [~,p,~,tstat]                  = ttest(Z_trans(labels~=0,model));
    pVal_R_allSubjects(model)      = p;
    tab_R_allSubjects(model).tstat = tstat;
end


% One-way between subject ANOVA
for model = 1:size(Z_trans,2)
    [p,tab,~] = anova1(Z_trans(labels~=0,model),labels(labels~=0),'off');
    pVal_R_ANOVA_allSubjects(model)    = p;
    tab_R_ANOVA_allSubjects(model).tab = tab;
end

% add an extra empty line
fprintf('\n')


% specify the true labels for a binary comparison of groups
for group_comp = 1:3
    
    % clear the data
    clear data
    
    % define temporary label
    labels_temp  = labels;
    labels_temp2 = labels;
    labels_temp(labels_temp~=group_comb_ttest{group_comp}(1))   = 0;
    labels_temp2(labels_temp2~=group_comb_ttest{group_comp}(2)) = 0;
    
    labels_temp(labels_temp~=0)   = 1;
    labels_temp2(labels_temp2~=0) = 1;
    
    % display the setting
    disp(['Group selection: ' group_names_ttest{group_comp} ' - Group size: ' num2str(sum(labels_temp==1)) ' vs. ' num2str(sum(labels_temp2==1))])
    
    % check whether there is a significant difference
    for model = 1:size(Z_trans,2)
        [~,p] = ttest2(Z_trans(labels_temp~=0,model),Z_trans(labels_temp2~=0,model));
        pVal_R_posthoc_allSubjects{group_comp}(model) = p;
    end
end


% display the results (t-test)
fprintf('\nSignificance (p-values) of model fit (t-test)\n')
for model = 1:length(pVal_R_allSubjects)
    fprintf('Model %d: \t t(%d) = %f , p = %e\n',model,tab_R_allSubjects(model).tstat.df,tab_R_allSubjects(model).tstat.tstat,pVal_R_allSubjects(model));
end


% display the results (ANOVA)
fprintf('\nSignificance (p-values) of group differences in model fit (ANOVA)\n')
for model = 1:size(Z_trans,2)
    F_table = tab_R_ANOVA_allSubjects(model).tab;
    fprintf('Model %d: \t F(%d,%d) = %f , p = %f\n',model,F_table{2,3},F_table{3,3},F_table{2,5},pVal_R_ANOVA_allSubjects(model));
end

% display the results (post-hoc t-test)
fprintf('\nSignificance (p-values) of group differences in model fit (post-hoc t-tests):\n')
fprintf(['\t\t' group_names_ttest{1} '\t\t' group_names_ttest{2} '\t\t' group_names_ttest{3} '\n'])
for model = 1:size(Z_trans,2)
    fprintf('Model %d: \t %f \t %f \t %f \n',model,pVal_R_posthoc_allSubjects{1}(model),pVal_R_posthoc_allSubjects{2}(model),pVal_R_posthoc_allSubjects{3}(model));
end

end