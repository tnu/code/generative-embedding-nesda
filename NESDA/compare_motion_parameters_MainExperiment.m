function compare_motion_parameters_MainExperiment(subject,experiment,SPM_prep,basis_set)
% Takes the motion parameters from the realignment step during the
% preprocessing and checks whether there is a significant difference
% between the groups for the FACES dataset or the Tower of Londong (ToL) 
% dataset of the NESDA study (Netherlands Study on Depression and Social 
% Anxiety). 
% 
% Input:
%   subject      	-- subject to be analyzed
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   SPM_prep        -- preprocessing: (0) NESDA pipeline, (1) SPM routines
%   basis_set       -- basis set: (0) only HRF, (1) temporal derivative, (2) informed basis set
% 
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2017
% Copyright 2017 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
exp_folders = {'FACES/','ToL/'};

% set the SPM folder
SPM_folder = {'','SPM_preprocessing/'};


% set the SPM path and the path to the experiment
addpath(FilenameInfo.SPM_path)
foldername      = fullfile(FilenameInfo.DataPath,exp_folders{experiment});
homefolder    	= FilenameInfo.Exp_path;


% define some variables
spm('Defaults','fMRI');
spm_jobman('initcfg');


% get the subject list
Subject_List = [dir([foldername '1*']); dir([foldername '2*']); dir([foldername '3*'])];

% specify the subject
if ( isempty(subject) )
    subject = 1:length(Subject_List);
end

if ( isempty(basis_set) )
    basis_set_analyze = [0 1 2];
else
    basis_set_analyze = basis_set;
end


% asign an old subject list
Subject_List_old = Subject_List;
Subject_List     = [];


% get the classes of the subjects
temp = load(fullfile(homefolder,'LCGA_classes','LCGA_classes.mat'));
LCGA_classes = temp.LCGA_classes;

% get all the included subjects in Schmaal et al. (2015)
temp2 = xlsread(fullfile(homefolder,'information','AllSubjects_Faces.xlsx'));
AllSubjects_Schmaal = temp2;


% specify a indicator vector
vector = zeros(length(Subject_List_old),1);

% find the subjects that are also listed in the LCGA classes
for int = 1:length(Subject_List_old)

    % find the index of the subject in the LCGA classes
    index_LCGA = find(LCGA_classes(:,1) == str2double(Subject_List_old(int).name),1);

    if ( ~isempty(index_LCGA) )
        
        % get only the subjects that were also included in Schmaal et al.
        index = find(AllSubjects_Schmaal == str2double(Subject_List_old(int).name),1);
        
        % check whether there is functional data
        files = dir(fullfile(foldername, Subject_List_old(int).name, 'funk', SPM_folder{SPM_prep+1}, '*.txt'));
        existData = ~isempty(files);

        % asign the subject names and class labels
        if ( ~isempty(index) )
            if ( ~isempty(index_LCGA) && existData )
                
                % set a one in the indicator vector
                vector(int,1) = 1;
                
                % get labels and subjects
                if ( isempty(Subject_List) )
                    Subject_List(1).name       = Subject_List_old(int).name;
                    LCGA_classes_restricted(1) = LCGA_classes(index_LCGA,2);
                    labels(1)                  = LCGA_classes(index_LCGA,3);
                else
                    Subject_List(end+1).name       = Subject_List_old(int).name;
                    LCGA_classes_restricted(end+1) = LCGA_classes(index_LCGA,2);
                    labels(end+1)                  = LCGA_classes(index_LCGA,3);
                end
            end
        end
    end
end


% define empty array
meanFD_all      = zeros(length(subject),length(basis_set_analyze));
rmsMovement_all = zeros(length(subject),length(basis_set_analyze));


% iterate over all subjects
for basis_set = basis_set_analyze
    for s = subject

        % get the subject name
        Subject = Subject_List_old(s).name;
        
        % get the correct filenames
        files = dir(fullfile(foldername, Subject, 'funk', SPM_folder{SPM_prep+1}, '*.txt'));

        % create the new filename
        if ( ~isempty(files) )
            
            % create new filename
            new_filename = fullfile(foldername, Subject, 'funk', SPM_folder{SPM_prep+1}, files(1).name);
            
            % load the motion parameters
            data_rp = textread(new_filename);
            
            % head radius in mm for FD computation (Power et al., 2012)
            rHead = 50;
            
            % get derivative
            dRp = diff(data_rp);
            dRp = [zeros(1,size(data_rp,2)); dRp];
            
            % from Power et al., 2014, Fig. 2
            FD                      = sum(abs(dRp(:,1:3)),2) + rHead*sum(abs(dRp(:,4:6)),2);
            meanFD                  = mean(FD);
            detrendedR              = detrend(data_rp); % TODO: replace by simple X\y
            detrendedR(:,4:6)       = rHead*detrendedR(:,4:6);
            rmsMovement             = sqrt(mean(sum(abs(detrendedR),2).^2,1));
            
            % asign the values
            meanFD_all(s,basis_set)      = meanFD;
            rmsMovement_all(s,basis_set) = rmsMovement;
            
        else
            meanFD_all(s,basis_set)      = NaN;
            rmsMovement_all(s,basis_set) = NaN;
        end

        % delete the data
        clear data_rp

    end
    
    % make a logical vector
    vector = vector==1;
    
    % dependent variable: coordinates
    X  = meanFD_all(vector,basis_set);
    X2 = rmsMovement_all(vector,basis_set);
    
    % group variable
    group = labels';
    
    % perform a one-way ANOVA
    [P,ANOVATAB]    = anova1(X,group,'off');
    [P2,ANOVATAB2]  = anova1(X2,group,'off');
    
    % output the result
    fprintf('Group Difference (FD): \t F(%d,%d) = %f , p = %f\n',ANOVATAB{2,3},ANOVATAB{3,3},ANOVATAB{2,5},P);
    fprintf('Group Difference (RMS):\t F(%d,%d) = %f , p = %f\n',ANOVATAB2{2,3},ANOVATAB2{3,3},ANOVATAB2{2,5},P2);
    
end

end