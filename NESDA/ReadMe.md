# README

This README contains the minimal information on the analysis pipeline for the NESDA project. The repository is meant to provide all the code for code review.

# Title
Predicting clinical trajectories of major depressive disorder with generative embedding

# Contributors / Roles
|                               |                                             |
| ----------------------------- | ------------------------------------------- |
| Project lead / analysis:      | Stefan Frässle (PhD)                        |
| Code review:                  | Dario Schöbi (PhD cand)                     |
| Supervising Prof.:            | Prof. Klaas Enno Stephan (MD Dr. med., PhD) |
| Abbreviation:                 | NESDA                                       |
| Date:                         | June 18, 2019                               |

# Summary
This project uses generative embedding to classify different naturalistic clinical trajectories in major depressive disorder (MDD) patients from the NEtherlands Study of Depression and Anxiety (NESDA). Specifically, dynamic causal modeling (DCM) was used to infer effective (directed) connectivity among neuronal populations from functional magnetic resonance imaging (fMRI) data. DCM parameters were then used to span a generative score space and a linear support vector machine (SVM) was trained to distinguish the different clinical trajectories. Classification accuracy (balanced accuracy) was assessed using a nested leave-one-out cross validation where, in the inner loop, hyperparameters of the SVM were optimized. This repository contains all the necessary code to reproduce the results reported in the accompanying paper and is meant for code review by an external reviewer (DS). 

The project was conducted at the Translational Neuromodeling Unit (TNU) by the project lead (SF), in collaboration with the NESDA consortium.

# Requirements
Requirements of the project are MATLAB, Statistical Parametric Mapping (SPM12), several MATLAB toolboxes


