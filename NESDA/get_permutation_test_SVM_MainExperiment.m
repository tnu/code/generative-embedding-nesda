function get_permutation_test_SVM_MainExperiment(FilenameInfo,model,experiment,regress_covariates,version)
% Performs a permutation test to identify features that are important for
% classifcation between the different patient subgroups - namely fast 
% remission, gradualy improving, and chronic. The script uses support 
% vector machines (SVM) in accordance with the classical approach
% used by Kay Brodersen.
%
% Input:
%   FilenameInfo        -- important definitions for the experiment
%   model               -- model to be analyzed
%   experiment          -- (1) FACES dataset, (2) Tower of London dataset
%   regress_covariate	-- regress out covariates (default: 0)
%   version             -- empty
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2016
% Copyright 2016 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% set the variables
SPM_prep            = 1;
include_DLPFC       = 0;
basis_set           = 2;
classifier          = 3;


% if FilenameInfo not specified, try to load
if ( isempty(FilenameInfo) )
    
    % get the path
    m_path = mfilename('fullpath');
    m_path = m_path(1:find(m_path=='/',1,'last'));
    
    try
        load(fullfile(m_path,'ConfigFile.mat'))
    catch err
        disp('Need to specify FilenameInfo containing paths!')
        rethrow(err)
    end
end


% set the experiment
exp_folder = {'FACES/','ToL/'};

% set folders
SPM_folder          = {'','SPM_preprocessing/'};
DLPFC_name          = {'','_DLPFC'};
settings_folder    	= {'woDLPFC','wDLPFC'};
basis_folder        = {'HRF','TD','IBS',''};
regress_name        = {'','_orth'};

% define the SPM version
classifier_name  = {'GPC','SVM','FITCSVM'};

% set the path of the experiment
exp_foldername	 = FilenameInfo.Exp_path;

% set the SPM path and the path to the experiment
foldername = [FilenameInfo.DataPath exp_folder{experiment}];


% define the name of the different models
name = {'BMA','model_FF_INTER','model_FB_INTER','model_RC_INTER','model_FF_noINTER',...
    'model_FB_noINTER','model_RC_noINTER','model_noMOD'};


% specify the model folder name
if ( isempty(model) )
    model_folder  = 'Bayesian_model_selection_RandRest';
elseif ( model == 0 )
    model_number  = 0;
    model_folder  = 'Bayesian_model_averaging_RandRest_single';
else
    model_number  = model;
    model_folder  = 'IndividualModel_RandRest';
end

% get the classes of the subjects
temp                = load([exp_foldername 'LCGA_classes/LCGA_classes.mat']);
LCGA_classes        = temp.LCGA_classes;
AllSubjects_Schmaal = xlsread([exp_foldername 'information/AllSubjects_Faces.xlsx']);

% results folder
results_folder = 'DCM_GroupAnalysis';

% load the posterior parameter estimates
if ( ~isempty(model) && model_number ~= 0 )
    temp = load(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['PosteriorParameterEstimates_' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local.mat']));
elseif ( isempty(model) )
    file = dir(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,[settings_folder{include_DLPFC+1} '_local'],'PosteriorParameterEstimates*'));
    temp    = load(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,[settings_folder{include_DLPFC+1} '_local'],file(1).name));
    pxp_max = temp.results.pxp_max;
else
    temp = load(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,[settings_folder{include_DLPFC+1} '_local'],'BMS.mat'));
    
    % bring the results in the same format
    for s = 1:length(temp.BMS.DCM.ffx.bma.mEps)
        
        % endogenous connections
        for int = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.A,1)
            for int2 = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.A,2)
                temp.results.A_Matrix_AllSubjects{int,int2}(s) = temp.BMS.DCM.ffx.bma.mEps{s}.A(int,int2);
            end
        end
        
        % modulatory influences
        for int = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.B,1)
            for int2 = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.B,2)
                for int3 = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.B,3)
                    temp.results.B_Matrix_AllSubjects{int,int2,int3}(s) = temp.BMS.DCM.ffx.bma.mEps{s}.B(int,int2,int3);
                end
            end
        end
        
        % driving inputs
        for int = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.C,1)
            for int2 = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.C,2)
                temp.results.C_Matrix_AllSubjects{int,int2}(s) = temp.BMS.DCM.ffx.bma.mEps{s}.C(int,int2);
            end
        end
    end
    
    % asign the subjects included
    temp.results.AllSubjects = temp.BMS.DCM.ffx.AllSubjects;
    
end


% asign the data
Amatrix_allSubjects     = temp.results.A_Matrix_AllSubjects;


% define a labels array
labels = zeros(1,length(temp.results.AllSubjects));

% dummy number of subjects
nr_subject = [];
found_subjects = [];

for int = 1:length(temp.results.AllSubjects)

    % find the index of the subject in the LCGA classes
    index_LCGA = find(LCGA_classes(:,1) == str2double(temp.results.AllSubjects{int}),1);

    if ( ~isempty(index_LCGA) )
        
        % restrict to subjects included in Schmaal et al.
        index = find(AllSubjects_Schmaal == str2double(temp.results.AllSubjects{int}),1);
        
        if ( ~isempty(index) )
            if ( ~isempty(index_LCGA) && isfinite(Amatrix_allSubjects{1,1}(int)) )
                nr_subject(end+1)     = LCGA_classes(index_LCGA,1);
                found_subjects(end+1) = str2double(temp.results.AllSubjects{int});
            end

            % if the subject exists, assign the class label
            if ( isfinite(Amatrix_allSubjects{1,1}(int)) )
                labels(int) = LCGA_classes(index_LCGA,3);
            end
        end
    end
end


% display how many subjects were found
fprintf('\n')
disp(['Found: ' num2str(length(nr_subject)) ' out of ' num2str(length(AllSubjects_Schmaal)) ' subjects - Running: ' classifier_name{classifier} ' / ' basis_folder{basis_set+1}])

% display subject name of missing subject
if ( length(nr_subject) ~= length(AllSubjects_Schmaal) )
    missing_subjects = setdiff(AllSubjects_Schmaal,found_subjects);
    fprintf((~isempty(missing_subjects))+1,'Subjects missing:');
    for int = 1:length(missing_subjects)
        fprintf((~isempty(missing_subjects))+1,' %d',missing_subjects(int));
    end
    fprintf('\n')
end


% default settings
group_comp_all = 1:3;
NrPerm_missing = [];


% specify the true labels for a binary comparison of groups
for group_comp = group_comp_all
    
    % maximum number of permutations
    maxNrPerm = 1000;
    
    % iterate over permutations
    for NrPerm = 1:maxNrPerm
        
        try
            if ( ~isempty(model) )
                temp = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding_Permutation' regress_name{regress_covariates+1} name{model_number+1} DLPFC_name{include_DLPFC+1} '_local' num2str(group_comp) '_' num2str(NrPerm) '.mat']));
            else
                temp = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding_PermutationBMS' regress_name{regress_covariates+1} DLPFC_name{include_DLPFC+1} '_local' num2str(group_comp) '_' num2str(NrPerm) '.mat']));
            end
            
            % asign the results
            results(group_comp,NrPerm) = temp.results(group_comp,NrPerm);
            name_connection            = temp.name_connection;
            
            % clear everything
            clear temp
            
        catch
            NrPerm_missing = [NrPerm_missing, NrPerm];
            disp(['Missing file - Group Comparison: ' num2str(group_comp) ' / Permutation: ' num2str(NrPerm)])
        end
    end
end

% output all the files to resubmit
if ( ~isempty(NrPerm_missing))
    fprintf(['\n\nRerun permutation for ' repmat('%d ',1,length(NrPerm_missing)) '- Then repeat collection of permutation results!\n'],NrPerm_missing)
else
    fprintf('\n\nAll permuations estimated - results can be used!\n')
end


% store the results of the generative embedding
if ( ~isempty(results) && ~isempty(name_connection) )
    if ( ~isempty(model) )
        save(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding_Permutation' regress_name{regress_covariates+1} name{model_number+1} DLPFC_name{include_DLPFC+1} '_local.mat']),'results','name_connection')
    else
        save(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding_PermutationBMS' regress_name{regress_covariates+1} DLPFC_name{include_DLPFC+1} '_local.mat']),'results','name_connection')
    end
end

% if all permuation files exist & summary file has been stored, delete temporary files
if ( isempty(NrPerm_missing) )
    for group_comp = group_comp_all
        if ( ~isempty(model) )
            delete(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding_Permutation' regress_name{regress_covariates+1} name{model_number+1} DLPFC_name{include_DLPFC+1} '_local' num2str(group_comp) '_*.mat']));
        else
            delete(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding_PermutationBMS' regress_name{regress_covariates+1} DLPFC_name{include_DLPFC+1} '_local' num2str(group_comp) '_*.mat']));
        end
    end
end

end