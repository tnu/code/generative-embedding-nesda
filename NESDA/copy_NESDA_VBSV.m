function copy_NESDA_VBSV(copy_direction,copy_content)
% Copies NESDA rnadom starting values for the DCM model inversion
% (multi-start) between "/cluster/scratch" (which is deleted afer couple of 
% weeks) and the long-term storage on Euler "/cluster/project/tnu".
% 
% Input:
%   copy_direction	-- direction of data transfer
%   copy_content 	-- (0) all, (1) ana, (2) funk, (3) logfiles
%
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2016
% Copyright 2016 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% set the foldername
if ( copy_direction == 1 )
    foldername    = fullfile(FilenameInfo.DataPath,'FACES');
    to_foldername = fullfile(FilenameInfo.LongTermStorage_path,'FACES');
else
    foldername    = fullfile(FilenameInfo.LongTermStorage_path,'FACES');
    to_foldername = fullfile(FilenameInfo.DataPath,'FACES');
end


% Display progress
disp('Copy VB Starting Values')


% copy the starting values
if ( copy_content == 0 || copy_content == 1 )
    if ( exist(fullfile(foldername,'RandomStartingValues'),'dir') )
        if ( ~exist(fullfile(to_foldername,'RandomStartingValues'),'dir') )
            mkdir(fullfile(to_foldername,'RandomStartingValues'))
        end
        copyfile(fullfile(foldername,'RandomStartingValues'),fullfile(to_foldername,'RandomStartingValues'))
    end
end

end