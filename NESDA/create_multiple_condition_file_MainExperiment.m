function create_multiple_condition_file_MainExperiment(subject,experiment,onCluster)
% Create the mutliple conditions file containing the onsets, durations and
% names of the different condition for the FACES dataset of the NESDA study
% (Netherlands Study on Depression and Social Anxiety). 
% 
% This function reads the txt-files containing onsets and durations for the
% different stimuli and creates a single matlab file that can be plugged
% into the first level GLM specification batch from SPM.
% 
% Input:
%   subject      	-- subject to be analyzed
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   onCluster       -- operation system: (1) Euler, (0) local machine
%
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2016
% Copyright 2016 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% set the experiment
exp_folder = {'FACES/','ToL/'};

% set the SPM path and the path to the experiment
if ( onCluster == 1 )
    
else
    addpath '/Users/stefanf/Documents/SPM/spm12';
    foldername = ['/Users/stefanf/Documents/ETH Zurich/Projects/NESDA/' exp_folder{experiment}];
end

% set the emotions
emotion_names = {'happy','angry','fear','sad','neutral','baseline'};

% get the subject list
Subject_List = dir([foldername '*']);
if ( length(Subject_List) < 10 )
    Subject_List = dir('/Users/stefanf/Documents/ETH Zurich/Projects/NESDA/information/data_files/designs_scaled/*');
end
Subject_List = Subject_List(4:end);

% specify the subject
if ( isempty(subject) )
    subject = 1:length(Subject_List);
end

% iterate over all subjects
for s = subject
    
    % get the subject name
    Subject = Subject_List(s).name;
    
    % display the subject
    disp(['Subject: ' Subject])
    
    for int = 1:length(emotion_names)
        
        % make the subject folder
        if ( ~exist([foldername Subject],'dir') )
            mkdir([foldername Subject])
        end
        
        % move the files to the logfile folder
        if ( ~exist([foldername Subject '/logfiles'],'dir') )
            mkdir([foldername Subject '/logfiles'])
            copyfile(['/Users/stefanf/Documents/ETH Zurich/Projects/NESDA/information/data_files/designs_scaled/' Subject '/*.txt'],[foldername Subject '/logfiles/'])
            if ( exist([foldername Subject '/faces_design.mat'],'file') )
                delete([foldername Subject '/faces_design.mat'])
            end
        end
        
        % set the filename
        filename = [foldername Subject '/logfiles/' emotion_names{int} '.txt'];
        
        % get the FID
        fileID = fopen(filename,'r');
        
        % get the data
        data = fscanf(fileID,'%f');
        
        % bring data into correct format
        onsets{int}     = data(1:3:end);
        durations{int}  = data(2:3:end);
        names{int}      = emotion_names{int};
        
        % close the file again
        fclose(fileID);
        
    end
    
    % save the multiple condition file
    save([foldername Subject '/logfiles/faces_design.mat'],'onsets','durations','names')
    
    % clear the files
    clear onsets durations names fileID
    
end

end