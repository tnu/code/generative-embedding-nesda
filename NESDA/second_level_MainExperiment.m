 function second_level_MainExperiment(contrast_type,experiment,restrict_sample,SPM_prep,basis_set,onCluster)
% Specify the group level General Linear Model (GLM) for the FACES dataset 
% or the Tower of London (ToL) dataset of the NESDA study (Netherlands 
% Study on Depression and Social Anxiety).
% 
% This function reads a Matalb Batch and the respective first level contrast 
% images to specify the GLM for group level analysis of BOLD activation 
% correlated with the FACES task or the ToL task. The GLM is then estimated 
% and the respective contrasts are specified.
% 
% Input:
%   contrast_type   -- statistical test: (1) t-test; (2) ANOVA
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   restrict_sample -- (0) all subjects, (1) Schmaal et al. (2015)
%   SPM_prep        -- (0) NESDA pipeline (1) SPM preprocessing
%   basis_set       -- basis set: (0) only HRF, (1) temporal derivative, (2) informed basis set
%   onCluster       -- operation system: (1) Euler, (0) local machine
%
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2017
% Copyright 2017 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
exp_folders = {'FACES/','ToL/'};

% set the SPM folder
SPM_folder = {'','SPM_preprocessing/'};


% set the SPM path and the path to the experiment
addpath(FilenameInfo.SPM_path)
foldername      = fullfile(FilenameInfo.DataPath,exp_folders{experiment});
data_foldername = fullfile(FilenameInfo.DataPath,exp_folders{experiment});
home_foldername = FilenameInfo.Exp_path;


% define some variables
spm('Defaults','fMRI');
spm_jobman('initcfg');

% specify some variables
contrast_name   = {'all faces > baseline','happy > baseline','angry > baseline','fear > baseline','sad > baseline','neutral > baseline'};
basis_folder    = {'HRF','TD','IBS',''};
RunBatch        = 1;

% get all subjects 
Subject_List = [dir(fullfile(foldername,'1*')); dir(fullfile(foldername,'2*')); dir(fullfile(foldername,'3*'))];

    
% asign an old subject list
Subject_List_old = Subject_List;
Subject_List     = [];

% get the classes of the subjects
if ( onCluster == 1 )
    temp = load(fullfile(home_foldername,'LCGA_classes.mat'));
else
    temp = load(fullfile(home_foldername,'LCGA_classes','LCGA_classes.mat'));
end
LCGA_classes = temp.LCGA_classes;

% get all the included subjects in Schmaal et al. (2015)
if ( onCluster == 1 )
    temp2 = xlsread(fullfile(home_foldername,'AllSubjects_Faces.xlsx'));
else
    temp2 = xlsread(fullfile(home_foldername,'information','AllSubjects_Faces.xlsx'));
end
AllSubjects_Schmaal = temp2;
    
    
% find the subjects that are also listed in the LCGA classes
for int = 1:length(Subject_List_old)
        
    % get the index
    index_LCGA = find(LCGA_classes(:,1) == str2double(Subject_List_old(int).name),1);
    
    if ( ~isempty(index_LCGA) )
        if ( restrict_sample )
            index = find(AllSubjects_Schmaal == str2double(Subject_List_old(int).name),1);
        else
            index = 1;
        end

        % check whether SPM file exists
        if ( SPM_prep == 1 )
            files = dir([foldername Subject_List_old(int).name '/funk/' SPM_folder{SPM_prep+1} 'sw*.nii']);
        else
            files = dir([foldername Subject_List_old(int).name '/funk/' SPM_folder{SPM_prep+1} '*mni.nii']);
        end
        
        % asign the subject names and class labels
        if ( ~isempty(index) )
            if ( ~isempty(index_LCGA) && ~isempty(files) )
                if ( isempty(Subject_List) )
                    Subject_List(1).name       = Subject_List_old(int).name;
                    LCGA_classes_restricted(1) = LCGA_classes(index_LCGA,2);
                else
                    Subject_List(end+1).name       = Subject_List_old(int).name;
                    LCGA_classes_restricted(end+1) = LCGA_classes(index_LCGA,2);
                end
            end
        end
    end
end

% display the number of subjects in the restricted sample
disp(['# of subjects: ' num2str(length(Subject_List))])


% iterate over all subjects
if ( contrast_type == 1 )
   
    % define the contrasts   	
    contrast_number = 1:length(contrast_name);
    
    % load the example Matlab Batch
    load([home_foldername 'Batch_SecondLevelModelSpecifications.mat'])
    
    for contrast = contrast_number
        
        % display what is done at the moment
        disp(['Group Level - Model specification - ' contrast_name{contrast}])


        % modify the working directory where the SPM.mat should be saved
        matlabbatch{1}.spm.stats.factorial_design.dir = [];

        % get the correct folder name
        if ( restrict_sample == 0 )
            folder = fullfile(foldername, 'SecondLevel', SPM_folder{SPM_prep+1}, basis_folder{basis_set+1}, 'one_sample_ttest', 'AllSubject', contrast_name{contrast});
        else
            folder = fullfile(foldername, 'SecondLevel', SPM_folder{SPM_prep+1}, basis_folder{basis_set+1}, 'one_sample_ttest', 'Schmaal', contrast_name{contrast});
        end

        % create the folder if it does not exist
        if ( ~exist(folder,'dir') )
            mkdir(folder)
        end

        % assign the new filename
        matlabbatch{1}.spm.stats.factorial_design.dir = {folder};


        % delete the filenames of the scans and load the new filenames
        matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = [];


        % assign the new file name to the matlabbatch
        for s = 1:length(Subject_List)

            % get the subject name
            Subject = Subject_List(s).name;

            if ( exist(fullfile(data_foldername, Subject, 'FirstLevel', SPM_folder{SPM_prep+1}, basis_folder{basis_set+1}, ['spmT_000' num2str(contrast) '.nii']),'file') )

                % get the correct filenames
                files = dir(fullfile(data_foldername, Subject, 'FirstLevel', SPM_folder{SPM_prep+1}, basis_folder{basis_set+1}, ['spmT_000' num2str(contrast) '*']));

                % asign the filenames to matlabbatch
                matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = [matlabbatch{1}.spm.stats.factorial_design.des.t1.scans; {fullfile(data_foldername, Subject, 'FirstLevel', SPM_folder{SPM_prep+1}, basis_folder{basis_set+1}, [files(1).name ',1'])}];

            end
        end


        %% estimate the first level GLM

        % specify the estimation part
        matlabbatch{2}.spm.stats.fmri_est.spmmat = cellstr(fullfile(folder,'SPM.mat'));


        %% do the inference

        % specify contrasts
        matlabbatch{3}.spm.stats.con.spmmat = cellstr(fullfile(folder,'SPM.mat'));
        matlabbatch{3}.spm.stats.con.consess{1}.tcon.name = contrast_name{contrast};
        matlabbatch{3}.spm.stats.con.consess{1}.tcon.weights = 1;


        %% save and run the created matlabbatch

        % save the batch
        save([folder '/secondlevel.mat'], 'matlabbatch');

        % check whether SPM already exists
        if ( exist(fullfile(folder,'SPM.mat'),'file') )
            delete(fullfile(folder,'SPM.mat'))
        end

        % run the batch
        if ( RunBatch == 1 )
            spm_jobman('run',matlabbatch)
        end

    end
    
elseif ( contrast_type == 2 )
    
    % load the example Matlab Batch
    load([home_foldername 'Batch_SecondLevelFcontrastModelSpecifications.mat'])
    
     % modify the working directory where the SPM.mat should be saved
    matlabbatch{1}.spm.stats.factorial_design.dir = [];

    % get the correct folder name
    if ( restrict_sample == 0 )
        folder = fullfile(foldername, 'SecondLevel', SPM_folder{SPM_prep+1}, basis_folder{basis_set+1}, 'withinANOVA', 'AllSubject');
    else
        folder = fullfile(foldername, 'SecondLevel', SPM_folder{SPM_prep+1}, basis_folder{basis_set+1}, 'withinANOVA', 'Schmaal');
    end
    
    % create the folder if it does not exist
    if ( ~exist(folder,'dir') )
        mkdir(folder)
    end

    % assign the new filename
    matlabbatch{1}.spm.stats.factorial_design.dir = {folder};
    
    % assign the new file name to the matlabbatch
    for s = 1:length(Subject_List)
        
        % create a dummy entry
        matlabbatch{1}.spm.stats.factorial_design.des.anovaw.fsubject(s) = matlabbatch{1}.spm.stats.factorial_design.des.anovaw.fsubject(1);
        
        % delete the filenames of the scans and load the new filenames
        matlabbatch{1}.spm.stats.factorial_design.des.anovaw.fsubject(s).scans = [];

        % get the subject name
        Subject = Subject_List(s).name;
        
        for contrast = 2:6
        
            if ( exist(fullfile(data_foldername, Subject, 'FirstLevel', SPM_folder{SPM_prep+1}, basis_folder{basis_set+1}, ['spmT_000' num2str(contrast) '.nii']),'file') )

                % get the correct filenames
                files = dir(fullfile(data_foldername, Subject, 'FirstLevel', SPM_folder{SPM_prep+1}, basis_folder{basis_set+1}, ['spmT_000' num2str(contrast) '*']));

                % asign the filenames to matlabbatch
                if contrast == 2
                    matlabbatch{1}.spm.stats.factorial_design.des.anovaw.fsubject(s).scans{1} = fullfile(data_foldername, Subject, 'FirstLevel', SPM_folder{SPM_prep+1}, basis_folder{basis_set+1}, [files(1).name ',1']);
                else
                    matlabbatch{1}.spm.stats.factorial_design.des.anovaw.fsubject(s).scans = [matlabbatch{1}.spm.stats.factorial_design.des.anovaw.fsubject(s).scans; {fullfile(data_foldername, Subject, 'FirstLevel', SPM_folder{SPM_prep+1}, basis_folder{basis_set+1}, [files(1).name ',1'])}];
                end
            end
        end
    end


    %% estimate the first level GLM

    % specify the estimation part
    matlabbatch{2}.spm.stats.fmri_est.spmmat = cellstr(fullfile(folder,'SPM.mat'));


    %% do the inference

    % specify the SPM
    matlabbatch{3}.spm.stats.con.spmmat = cellstr(fullfile(folder,'SPM.mat'));
    counter = 0;
    
    % specify the contrasts
    for contrast_1 = 2:6
        for contrast_2 = 2:6
            if ( contrast_1 < contrast_2 )
                counter = counter + 1;
                matlabbatch{3}.spm.stats.con.consess{counter}.fcon.name = [contrast_name{contrast_1} ' vs ' contrast_name{contrast_2}];
                matlabbatch{3}.spm.stats.con.consess{counter}.fcon.weights = zeros(1,5);
                matlabbatch{3}.spm.stats.con.consess{counter}.fcon.weights(contrast_1-1) = 1;
                matlabbatch{3}.spm.stats.con.consess{counter}.fcon.weights(contrast_2-1) = -1;
            end
        end
    end


    %% save and run the created matlabbatch

    % save the batch
    save([folder '/secondlevel.mat'], 'matlabbatch');

    % check whether SPM already exists
    if ( exist(fullfile(folder,'SPM.mat'),'file') )
        delete(fullfile(folder,'SPM.mat'))
    end

    % run the batch
    if ( RunBatch == 1 )
        spm_jobman('run',matlabbatch)
    end
    
elseif ( contrast_type == 3 )
   
    % define the contrast   	
    contrast_number = 1:length(contrast_name);
    
    % load the example Matlab Batch
    load([home_foldername 'Batch_SecondLevelANOVAModelSpecifications.mat'])
    
    for contrast = contrast_number
        
        % display what is done at the moment
        disp(['Group Level - Model specification - ' contrast_name{contrast}])


        % modify the working directory where the SPM.mat should be saved
        matlabbatch{1}.spm.stats.factorial_design.dir = [];

        % get the correct folder name
        if ( restrict_sample == 0 )
            folder = fullfile(foldername, 'SecondLevel', SPM_folder{SPM_prep+1}, basis_folder{basis_set+1}, 'ANOVA', 'AllSubject', contrast_name{contrast});
        else
            folder = fullfile(foldername, 'SecondLevel', SPM_folder{SPM_prep+1}, basis_folder{basis_set+1}, 'ANOVA', 'Schmaal', contrast_name{contrast});
        end

        % create the folder if it does not exist
        if ( ~exist(folder,'dir') )
            mkdir(folder)
        end

        % assign the new filename
        matlabbatch{1}.spm.stats.factorial_design.dir = {folder};
        
        for groups = 1:length(matlabbatch{1}.spm.stats.factorial_design.des.anova.icell)
        

            % delete the filenames of the scans and load the new filenames
            matlabbatch{1}.spm.stats.factorial_design.des.anova.icell(groups).scans = [];


            % assign the new file name to the matlabbatch
            for s = 1:length(Subject_List)
                
                % get the subject name
                Subject = Subject_List(s).name;
                
                if ( exist(fullfile(data_foldername, Subject, 'FirstLevel', SPM_folder{SPM_prep+1}, basis_folder{basis_set+1}, ['spmT_000' num2str(contrast) '.nii']),'file') )

                    % get the correct filenames
                    files = dir(fullfile(data_foldername, Subject, 'FirstLevel', SPM_folder{SPM_prep+1}, basis_folder{basis_set+1}, ['spmT_000' num2str(contrast) '*']));

                    % asign the filenames to matlabbatch
                    if ( LCGA_classes_restricted(s) == groups )
                        matlabbatch{1}.spm.stats.factorial_design.des.anova.icell(groups).scans = [matlabbatch{1}.spm.stats.factorial_design.des.anova.icell(groups).scans; {fullfile(data_foldername, Subject, 'FirstLevel', SPM_folder{SPM_prep+1}, basis_folder{basis_set+1}, [files(1).name ',1'])}];
                    end

                end
            end
        end


        %% estimate the first level GLM

        % specify the estimation part
        matlabbatch{2}.spm.stats.fmri_est.spmmat = cellstr(fullfile(folder,'SPM.mat'));


        %% do the inference
        
        % specify the SPM
        matlabbatch{3}.spm.stats.con.spmmat = cellstr(fullfile(folder,'SPM.mat'));
        
        % specify contrasts
        matlabbatch{3}.spm.stats.con.consess{1}.fcon.name = 'rapid_remission vs. gradual_improvement';
        matlabbatch{3}.spm.stats.con.consess{1}.fcon.weights = [1 -1 0];
        matlabbatch{3}.spm.stats.con.consess{2}.fcon.name = 'rapid_remission vs. chronic';
        matlabbatch{3}.spm.stats.con.consess{2}.fcon.weights = [1 0 -1];
        matlabbatch{3}.spm.stats.con.consess{3}.fcon.name = 'gradual_improvement vs. chronic';
        matlabbatch{3}.spm.stats.con.consess{3}.fcon.weights = [0 1 -1];
        
        
        %% save and run the created matlabbatch

        % save the batch
        save([folder '/secondlevel.mat'], 'matlabbatch');

        % check whether SPM already exists
        if ( exist(fullfile(folder,'SPM.mat'),'file') )
            delete(fullfile(folder,'SPM.mat'))
        end

        % run the batch
        if ( RunBatch == 1 )
            spm_jobman('run',matlabbatch)
        end

    end
end

end
