function output_subject_infos_MainExperiment(FilenameInfo)
% Outputs the demographic and clinical characteristics of the participants
% included in the present study. Furthermore, the function checks whether
% patient subgroups (remitted, gradually improving, and chronic) differ in
% these characteristics using the appropriate test statistics. 
%
% Input:
%   FilenameInfo        -- important definitions for the experiment
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% if FilenameInfo not specified, try to load
if ( isempty(FilenameInfo) )
    
    % get the path
    m_path = mfilename('fullpath');
    m_path = m_path(1:find(m_path=='/',1,'last'));
    
    try
        load(fullfile(m_path,'ConfigFile.mat'))
    catch err
        disp('Need to specify FilenameInfo containing paths!')
        rethrow(err)
    end
end


% set the path of the experiment
exp_foldername	 = FilenameInfo.Exp_path;

% get the subjects included in Schmaal et al. (2015)
AllSubjects_Schmaal = xlsread([exp_foldername 'information/AllSubjects_Faces.xlsx']);


% load subject information
info          = load([exp_foldername 'LCGA_classes/Subjects_info.mat']);
Subjects_info = info.Subjects_info;

% empty index array
index = [];

% get all the indices
for int = 1:length(AllSubjects_Schmaal)
    index(int) = find(AllSubjects_Schmaal(int) == Subjects_info(:,1)); 
end

% restrict information to the Schmaal sample
Subjects_info_restricted = Subjects_info(index,:);


% format specification of output
formatSpec      = '%25s | %15s | %15s | %15s | %10s | %10s |\n';
cell_seperator = '-----------------------------------------------------------------------------------------------------------\n';

% print the title
fprintf('\n')
fprintf(formatSpec, 'Characteristic', 'REM', 'IMP', 'CHR', 'Statistic', 'p Value');
fprintf('===========================================================================================================\n')


% compute the ANOVA for age
[p,tab] = anova1(Subjects_info_restricted(:,3),Subjects_info_restricted(:,2),'off');

% specify the pars for age
clear pars
pars(1,1) = round(mean(Subjects_info_restricted(Subjects_info_restricted(:,2)==1,3)),2);
pars(1,2) = round(std(Subjects_info_restricted(Subjects_info_restricted(:,2)==1,3)),2);
pars(2,1) = round(mean(Subjects_info_restricted(Subjects_info_restricted(:,2)==2,3)),2);
pars(2,2) = round(std(Subjects_info_restricted(Subjects_info_restricted(:,2)==2,3)),2);
pars(3,1) = round(mean(Subjects_info_restricted(Subjects_info_restricted(:,2)==3,3)),2);
pars(3,2) = round(std(Subjects_info_restricted(Subjects_info_restricted(:,2)==3,3)),2);

% output age
fprintf(formatSpec, ...
    'Age, Years', [num2str(pars(1,1)) ' (' num2str(pars(1,2)) ')'],...
                  [num2str(pars(2,1)) ' (' num2str(pars(2,2)) ')'],...
                  [num2str(pars(3,1)) ' (' num2str(pars(3,2)) ')'],...
                  num2str(round(tab{2,5},2)), num2str(round(p,2)));
fprintf(cell_seperator)


% compute the chi-square test for gender
[~,chi2,p] = crosstab(Subjects_info_restricted(:,4),Subjects_info_restricted(:,2));

% output gender
fprintf(formatSpec, 'Gender, n (%)', ' ', ' ', ' ', ' ', ' ');

% gender names
gender_names = {'Male','Female'};

% specify the pars for gender
for gender = [2 1]
    clear pars
    pars(1,1) = sum(Subjects_info_restricted(Subjects_info_restricted(:,2)==1,4)==gender);
    pars(1,2) = round(pars(1,1)/sum(Subjects_info_restricted(:,2)==1),2);
    pars(2,1) = sum(Subjects_info_restricted(Subjects_info_restricted(:,2)==2,4)==gender);
    pars(2,2) = round(pars(2,1)/sum(Subjects_info_restricted(:,2)==2),2);
    pars(3,1) = sum(Subjects_info_restricted(Subjects_info_restricted(:,2)==3,4)==gender);
    pars(3,2) = round(pars(3,1)/sum(Subjects_info_restricted(:,2)==3),2);
    
    % output gender
    switch gender
        case 2
            fprintf(formatSpec, ...
                gender_names{gender}, [num2str(pars(1,1)) ' (' num2str(pars(1,2)) ')'],...
                                      [num2str(pars(2,1)) ' (' num2str(pars(2,2)) ')'],...
                                      [num2str(pars(3,1)) ' (' num2str(pars(3,2)) ')'],...
                                      num2str(round(chi2,2)), num2str(round(p,2)));
        case 1
            fprintf(formatSpec, ...
                gender_names{gender}, [num2str(pars(1,1)) ' (' num2str(pars(1,2)) ')'],...
                                      [num2str(pars(2,1)) ' (' num2str(pars(2,2)) ')'],...
                                      [num2str(pars(3,1)) ' (' num2str(pars(3,2)) ')'],...
                                      ' ', ' ');
    end
end
fprintf(cell_seperator)
                 

% compute the ANOVA for education
[p,tab] = anova1(Subjects_info_restricted(:,5),Subjects_info_restricted(:,2),'off');

% specify the pars for education
clear pars
pars(1,1) = round(mean(Subjects_info_restricted(Subjects_info_restricted(:,2)==1,5)),2);
pars(1,2) = round(std(Subjects_info_restricted(Subjects_info_restricted(:,2)==1,5)),2);
pars(2,1) = round(mean(Subjects_info_restricted(Subjects_info_restricted(:,2)==2,5)),2);
pars(2,2) = round(std(Subjects_info_restricted(Subjects_info_restricted(:,2)==2,5)),2);
pars(3,1) = round(mean(Subjects_info_restricted(Subjects_info_restricted(:,2)==3,5)),2);
pars(3,2) = round(std(Subjects_info_restricted(Subjects_info_restricted(:,2)==3,5)),2);

% output education
fprintf(formatSpec, ...
    'Education, Years', [num2str(pars(1,1)) ' (' num2str(pars(1,2)) ')'],...
                        [num2str(pars(2,1)) ' (' num2str(pars(2,2)) ')'],...
                        [num2str(pars(3,1)) ' (' num2str(pars(3,2)) ')'],...
                        num2str(round(tab{2,5},2)), num2str(round(p,2)));
fprintf(cell_seperator)


% compute the chi-square test for scanner site
[~,chi2,p] = crosstab(Subjects_info_restricted(:,6),Subjects_info_restricted(:,2));

% output scanner site
fprintf(formatSpec, 'Scan Location, n (%)', ' ', ' ', ' ', ' ', ' ');

% site locations
site_names = {'AMC Amsterdam','LUMC Leiden','UMCG Groningen'};

% specify the pars for scanner site
for scanner_site = 1:3
    clear pars
    pars(1,1) = sum(Subjects_info_restricted(Subjects_info_restricted(:,2)==1,6)==scanner_site);
    pars(1,2) = round(pars(1,1)/sum(Subjects_info_restricted(:,2)==1),2);
    pars(2,1) = sum(Subjects_info_restricted(Subjects_info_restricted(:,2)==2,6)==scanner_site);
    pars(2,2) = round(pars(2,1)/sum(Subjects_info_restricted(:,2)==2),2);
    pars(3,1) = sum(Subjects_info_restricted(Subjects_info_restricted(:,2)==3,6)==scanner_site);
    pars(3,2) = round(pars(3,1)/sum(Subjects_info_restricted(:,2)==3),2);

    % output scanner site
    if scanner_site == 1
        fprintf(formatSpec, ...
            site_names{scanner_site}, [num2str(pars(1,1)) ' (' num2str(pars(1,2)) ')'],...
                                      [num2str(pars(2,1)) ' (' num2str(pars(2,2)) ')'],...
                                      [num2str(pars(3,1)) ' (' num2str(pars(3,2)) ')'],...
                                      num2str(round(chi2,2)), num2str(round(p,2)));
    else
        fprintf(formatSpec, ...
            site_names{scanner_site}, [num2str(pars(1,1)) ' (' num2str(pars(1,2)) ')'],...
                                      [num2str(pars(2,1)) ' (' num2str(pars(2,2)) ')'],...
                                      [num2str(pars(3,1)) ' (' num2str(pars(3,2)) ')'],...
                                      ' ', ' ');
    end
end
fprintf(cell_seperator)


% load further subject information (IDS scores)
info2          = xlsread([exp_foldername 'information/ids_waves.xlsx']);
Subjects_info2 = info2;

% empty index array
index2 = [];

% get all the indices
for int = 1:length(AllSubjects_Schmaal)
    index2(int) = find(AllSubjects_Schmaal(int) == Subjects_info2(:,1)); 
end

% restrict information to the Schmaal sample
Subjects_info_restricted2 = Subjects_info2(index2,:);


% compute the ANOVA for IDS (T1)
[p,tab] = anova1(Subjects_info_restricted2(:,2),Subjects_info_restricted(:,2),'off');

% specify the pars for IDS (T1)
clear pars
pars(1,1) = round(mean(Subjects_info_restricted2(Subjects_info_restricted(:,2)==1,2)),2);
pars(1,2) = round(std(Subjects_info_restricted2(Subjects_info_restricted(:,2)==1,2)),2);
pars(2,1) = round(mean(Subjects_info_restricted2(Subjects_info_restricted(:,2)==2,2)),2);
pars(2,2) = round(std(Subjects_info_restricted2(Subjects_info_restricted(:,2)==2,2)),2);
pars(3,1) = round(mean(Subjects_info_restricted2(Subjects_info_restricted(:,2)==3,2)),2);
pars(3,2) = round(std(Subjects_info_restricted2(Subjects_info_restricted(:,2)==3,2)),2);

% output IDS (T1)
fprintf(formatSpec, ...
    'IDS T1', [num2str(pars(1,1)) ' (' num2str(pars(1,2)) ')'],...
              [num2str(pars(2,1)) ' (' num2str(pars(2,2)) ')'],...
              [num2str(pars(3,1)) ' (' num2str(pars(3,2)) ')'],...
              num2str(round(tab{2,5},2)), num2str(round(p,2)));
fprintf(cell_seperator)


% compute the ANOVA for IDS (T2)
[p,tab] = anova1(Subjects_info_restricted2(:,3),Subjects_info_restricted(:,2),'off');
p_text  = {num2str(round(p,2)),'<0.001'};

% specify the pars for IDS (T2)
clear pars
pars(1,1) = round(mean(Subjects_info_restricted2(Subjects_info_restricted(:,2)==1,3)),2);
pars(1,2) = round(std(Subjects_info_restricted2(Subjects_info_restricted(:,2)==1,3)),2);
pars(2,1) = round(mean(Subjects_info_restricted2(Subjects_info_restricted(:,2)==2,3)),2);
pars(2,2) = round(std(Subjects_info_restricted2(Subjects_info_restricted(:,2)==2,3)),2);
pars(3,1) = round(mean(Subjects_info_restricted2(Subjects_info_restricted(:,2)==3,3)),2);
pars(3,2) = round(std(Subjects_info_restricted2(Subjects_info_restricted(:,2)==3,3)),2);

% output IDS (T2)
fprintf(formatSpec, ...
    'IDS T2', [num2str(pars(1,1)) ' (' num2str(pars(1,2)) ')'],...
              [num2str(pars(2,1)) ' (' num2str(pars(2,2)) ')'],...
              [num2str(pars(3,1)) ' (' num2str(pars(3,2)) ')'],...
              num2str(round(tab{2,5},2)), p_text{(p<0.001)+1});
fprintf(cell_seperator)



% compute the ANOVA for change (T2-T1)
change_IDS = Subjects_info_restricted2(:,3)-Subjects_info_restricted2(:,2);
[p,tab]    = anova1(change_IDS,Subjects_info_restricted(:,2),'off');
p_text     = {num2str(round(p,2)),'<0.001'};

% specify the pars for IDS (T2)
clear pars
pars(1,1) = round(mean(change_IDS(Subjects_info_restricted(:,2)==1)),2);
pars(1,2) = round(std(change_IDS(Subjects_info_restricted(:,2)==1)),2);
pars(2,1) = round(mean(change_IDS(Subjects_info_restricted(:,2)==2)),2);
pars(2,2) = round(std(change_IDS(Subjects_info_restricted(:,2)==2)),2);
pars(3,1) = round(mean(change_IDS(Subjects_info_restricted(:,2)==3)),2);
pars(3,2) = round(std(change_IDS(Subjects_info_restricted(:,2)==3)),2);

% output IDS (T2)
fprintf(formatSpec, ...
    'IDS Change (T2-T1)', [num2str(pars(1,1)) ' (' num2str(pars(1,2)) ')'],...
                          [num2str(pars(2,1)) ' (' num2str(pars(2,2)) ')'],...
                          [num2str(pars(3,1)) ' (' num2str(pars(3,2)) ')'],...
                          num2str(round(tab{2,5},2)), p_text{(p<0.001)+1});
fprintf(cell_seperator)


% load further subject information (medication use, BAI)
info3          = xlsread([exp_foldername 'information/LCGA_5classes.xlsx']);
Subjects_info3 = info3;

% empty index array
index3 = [];

% get all the indices
for int = 1:length(AllSubjects_Schmaal)
    index3(int) = find(AllSubjects_Schmaal(int) == Subjects_info3(:,1)); 
end

% restrict information to the Schmaal sample
Subjects_info_restricted3 = Subjects_info3(index3,:);


% compute the ANOVA for BAI
[p,tab] = anova1(Subjects_info_restricted3(:,11),Subjects_info_restricted(:,2),'off');

% specify the pars for BAI
clear pars
pars(1,1) = round(mean(Subjects_info_restricted3(Subjects_info_restricted(:,2)==1,11)),2);
pars(1,2) = round(std(Subjects_info_restricted3(Subjects_info_restricted(:,2)==1,11)),2);
pars(2,1) = round(mean(Subjects_info_restricted3(Subjects_info_restricted(:,2)==2,11)),2);
pars(2,2) = round(std(Subjects_info_restricted3(Subjects_info_restricted(:,2)==2,11)),2);
pars(3,1) = round(mean(Subjects_info_restricted3(Subjects_info_restricted(:,2)==3,11)),2);
pars(3,2) = round(std(Subjects_info_restricted3(Subjects_info_restricted(:,2)==3,11)),2);

% output BAI
fprintf(formatSpec, ...
    'BAI T1', [num2str(pars(1,1)) ' (' num2str(pars(1,2)) ')'],...
           [num2str(pars(2,1)) ' (' num2str(pars(2,2)) ')'],...
           [num2str(pars(3,1)) ' (' num2str(pars(3,2)) ')'],...
           num2str(round(tab{2,5},2)), num2str(round(p,2)));
fprintf(cell_seperator)


% compute the chi-square test for medication use (T1)
[~,chi3,p] = crosstab(Subjects_info_restricted3(:,8),Subjects_info_restricted(:,2));

% output medication use (T1)
fprintf(formatSpec, 'Antidep. Use (T1), n (%)', ' ', ' ', ' ', ' ', ' ');

% medication use (T1)
use_names = {'No','Yes'};

% medication use: yes / no
for med_use = [0 1]
    clear pars
    pars(1,1) = sum(Subjects_info_restricted3(Subjects_info_restricted(:,2)==1,8)==med_use);
    pars(1,2) = round(pars(1,1)/sum(Subjects_info_restricted(:,2)==1),2);
    pars(2,1) = sum(Subjects_info_restricted3(Subjects_info_restricted(:,2)==2,8)==med_use);
    pars(2,2) = round(pars(2,1)/sum(Subjects_info_restricted(:,2)==2),2);
    pars(3,1) = sum(Subjects_info_restricted3(Subjects_info_restricted(:,2)==3,8)==med_use);
    pars(3,2) = round(pars(3,1)/sum(Subjects_info_restricted(:,2)==3),2);
    
    % output medication use (T1)
    switch med_use
        case 0
            fprintf(formatSpec, ...
                use_names{med_use+1}, [num2str(pars(1,1)) ' (' num2str(pars(1,2)) ')'],...
                                      [num2str(pars(2,1)) ' (' num2str(pars(2,2)) ')'],...
                                      [num2str(pars(3,1)) ' (' num2str(pars(3,2)) ')'],...
                                      num2str(round(chi3,2)), num2str(round(p,2)));
        case 1
            fprintf(formatSpec, ...
                use_names{med_use+1}, [num2str(pars(1,1)) ' (' num2str(pars(1,2)) ')'],...
                                      [num2str(pars(2,1)) ' (' num2str(pars(2,2)) ')'],...
                                      [num2str(pars(3,1)) ' (' num2str(pars(3,2)) ')'],...
                                      ' ', ' ');
    end
end
fprintf(cell_seperator)


% compute the chi-square test for medication use (T2)
[~,chi4,p] = crosstab(Subjects_info_restricted3(:,9),Subjects_info_restricted(:,2));

% output medication use (T2)
fprintf(formatSpec, 'Antidep. Use (T2), n (%)', ' ', ' ', ' ', ' ', ' ');

% medication use (T2)
use_names = {'No','Yes'};

% medication use: yes / no
for med_use = [0 1]
    clear pars
    pars(1,1) = sum(Subjects_info_restricted3(Subjects_info_restricted(:,2)==1,9)==med_use);
    pars(1,2) = round(pars(1,1)/sum(Subjects_info_restricted(:,2)==1),2);
    pars(2,1) = sum(Subjects_info_restricted3(Subjects_info_restricted(:,2)==2,9)==med_use);
    pars(2,2) = round(pars(2,1)/sum(Subjects_info_restricted(:,2)==2),2);
    pars(3,1) = sum(Subjects_info_restricted3(Subjects_info_restricted(:,2)==3,9)==med_use);
    pars(3,2) = round(pars(3,1)/sum(Subjects_info_restricted(:,2)==3),2);
    
    % output medication use (T2)
    switch med_use
        case 0
            fprintf(formatSpec, ...
                use_names{med_use+1}, [num2str(pars(1,1)) ' (' num2str(pars(1,2)) ')'],...
                                      [num2str(pars(2,1)) ' (' num2str(pars(2,2)) ')'],...
                                      [num2str(pars(3,1)) ' (' num2str(pars(3,2)) ')'],...
                                      num2str(round(chi4,2)), num2str(round(p,2)));
        case 1
            fprintf(formatSpec, ...
                use_names{med_use+1}, [num2str(pars(1,1)) ' (' num2str(pars(1,2)) ')'],...
                                      [num2str(pars(2,1)) ' (' num2str(pars(2,2)) ')'],...
                                      [num2str(pars(3,1)) ' (' num2str(pars(3,2)) ')'],...
                                      ' ', ' ');
    end
end
fprintf(cell_seperator)


end
