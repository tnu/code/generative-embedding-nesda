function get_CI_MainExperiment(experiment,SPM_prep,include_DLPFC,basis_set)
% Gets the estimated mean and standard deviation of the contrast images obtained
% from first-level GLM analyses of the acquired fMRI data, as a comparison 
% for the DCM (effective connectivity) estimates.
% 
% Input:
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   SPM_prep        -- (0) NESDA pipeline (1) SPM preprocessing
%   include_DLPFC   -- (0) only face emotion network, (1) include DLPFC
%   basis_set       -- basis set: (0) only HRF, (1) temporal derivative, (2) informed basis set
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2017
% Copyright 2018 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
exp_folders = {'FACES/','ToL/'};

% set the preprocessing folder
SPM_folder          = {'','SPM_preprocessing/'};
DLPFC_name          = {'','_DLPFC'};
basis_folder        = {'HRF','TD','IBS',''};


% set the SPM path and the path to the experiment
addpath(FilenameInfo.SPM_path)
foldername      = fullfile(FilenameInfo.DataPath,exp_folders{experiment});


% set the default setting for including the DLPFC node in the DCMs
if ( isempty(include_DLPFC) && experiment == 1 )
    include_DLPFC = 0;
end

% get the subject list
Subject_List = [dir(fullfile(foldername,'1*')); dir(fullfile(foldername,'2*')); dir(fullfile(foldername,'3*'))];

% display progress
disp('Get contrast image values -----------------')

% create a results array
data_CI = cell(1,6);


% iterate over subjects
for subject = 1:length(Subject_List)

    % subject name
    Subject = Subject_List(subject).name;

    % display progress
    disp(['-- Subject ' Subject])
    
    % define the folder where the mean contrast values are stored
    CI_foldername = fullfile(foldername,Subject,'FirstLevel',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'mean_CI_VOI','NeuroSynth');
    
    % load the data
    if ( exist(fullfile(CI_foldername,['ContrastImage_measures' DLPFC_name{include_DLPFC+1} '_local.mat']),'file') )
        temp = load(fullfile(CI_foldername,['ContrastImage_measures' DLPFC_name{include_DLPFC+1} '_local.mat']));
        data_CI_AllContrasts = temp.data_CI_AllContrasts;

        % asign the data for all subjects
        for contrast = 1:6
            if ( ~isempty(data_CI_AllContrasts{1}) )
                data_CI{contrast}(subject,:) = data_CI_AllContrasts{contrast};
            else
                data_CI{contrast}(subject,:) = deal(NaN);
            end
        end
    else
        for contrast = 1:6
            data_CI{contrast}(subject,:) = deal(NaN);
        end
    end
    
    % asign the subject name
    AllSubjects{subject} = Subject;
    
end

% save the results of the particular model
if ( ~exist(fullfile(foldername,'CI_GroupAnalysis',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal'),'dir') )
    mkdir(fullfile(foldername,'CI_GroupAnalysis',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal'))
end
save(fullfile(foldername,'CI_GroupAnalysis',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',['CI_measures' DLPFC_name{include_DLPFC+1} '_local.mat']),'data_CI','AllSubjects')

end