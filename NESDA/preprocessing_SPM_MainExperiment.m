function preprocessing_SPM_MainExperiment(subject,experiment,onCluster)
% Performs preprocessing using the standard SPM routines, rather than the
% preprocessing pipeline from NESDA. 
% 
% Preprocessing: (1) Realign, (2) Coregistration, (3) Segmentation,
%                (4) Normalization, (5) Smoothing
% 
% Input:
%   subject         -- subject to be analyzed
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   onCluster       -- operation system: (1) Euler, (0) local machine
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2017
% Copyright 2017 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% run batch
RunBatch = 1;


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
exp_folders = {'FACES/','ToL/'};

% set the SPM path and the path to the experiment
addpath(FilenameInfo.SPM_path)
foldername      = fullfile(FilenameInfo.DataPath,exp_folders{experiment});
home_foldername = FilenameInfo.Exp_path;


% define some variables
spm('Defaults','fMRI');
spm_jobman('initcfg');


% load the batch of the preprocessing
load(fullfile(home_foldername, 'Batch_PreprocessingSPM.mat'))

% get the subject list
Subject_List = [dir(fullfile(foldername,'1*')); dir(fullfile(foldername,'2*')); dir(fullfile(foldername,'3*'))];

% subject name
Subject = Subject_List(subject).name;

% display what is done at the moment
disp([Subject ' - PreProcessing'])


% delete the saved filenames to load the new realignment parameters
matlabbatch{1}.spm.spatial.realign.estwrite.data{1} = [];


% get the correct filenames
files = spm_select('expand', fullfile(foldername, Subject, 'funk', 'SPM_preprocessing', [Subject '_filtered_func_data.nii']));

% output the number of scans
disp(['Number of EPI volumes: ' num2str(size(files,1))])


% delete the files that have to generated again
if ( ~isempty(files) )
    delete(fullfile(foldername, Subject, 'funk', 'SPM_preprocessing','*.txt'));
    delete(fullfile(foldername, Subject, 'funk', 'SPM_preprocessing','mean*'));
    delete(fullfile(foldername, Subject, 'funk', 'SPM_preprocessing','w*'));
    delete(fullfile(foldername, Subject, 'funk', 'SPM_preprocessing','sw*'));
end


% asign the filenames to matlabbatch
for int2 = 1:size(files,1)

    % assign the new filename        
    matlabbatch{1}.spm.spatial.realign.estwrite.data{1} = [matlabbatch{1}.spm.spatial.realign.estwrite.data{1}; {[files(int2,1:end-3) num2str(int2)]}];

end
    
    
% delete the saved filenames the new corregistration parameters
matlabbatch{2}.spm.spatial.coreg.estimate.source = [];

% get the correct filenames
files_coreg_source = dir(fullfile(foldername, Subject, 'ana','*T1.nii'));

% create the new filename
new_filename_coreg_source = fullfile(foldername, Subject, 'ana',[files_coreg_source(1).name ',1']);

% assign the new filename
matlabbatch{2}.spm.spatial.coreg.estimate.source = {new_filename_coreg_source};

% change the paths to the TPM files (if on Euler)
if ( onCluster )
    for int = 1:length(matlabbatch{3}.spm.spatial.preproc.tissue)
        matlabbatch{3}.spm.spatial.preproc.tissue(int).tpm = {[FilenameInfo.SPM_path 'tpm/' matlabbatch{3}.spm.spatial.preproc.tissue(int).tpm{1}(end-8:end)]};
    end
end


% save the matlabbatch
save(fullfile(foldername, Subject, 'funk', 'SPM_preprocessing', 'preprocessing.mat'), 'matlabbatch');


% run the preprocessing
if ( RunBatch == 1 )
    spm_jobman('run',matlabbatch)
end

% delete the normalized images to free space
delete(fullfile(foldername, Subject, 'funk', 'SPM_preprocessing','w*'));

end