function estimate_CI_MainExperiment(subject,experiment,SPM_prep,include_DLPFC,basis_set)
% Computes the mean and standard deviation of the contrast images obtained
% from first-level GLM analyses of the acquired fMRI data, as a comparison 
% for the DCM (effective connectivity) estimates.
% 
% Input:
%   subject      	-- subject to be analyzed
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   SPM_prep        -- (0) NESDA pipeline (1) SPM preprocessing
%   include_DLPFC   -- (0) only face emotion network, (1) include DLPFC
%   basis_set       -- basis set: (0) only HRF, (1) temporal derivative, (2) informed basis set
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2017
% Copyright 2017 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
exp_folders = {'FACES/','ToL/'};

% set the preprocessing folder
SPM_folder          = {'','SPM_preprocessing/'};
DLPFC_name          = {'','_DLPFC'};
basis_folder        = {'HRF','TD','IBS',''};


% set the SPM path and the path to the experiment
addpath(FilenameInfo.SPM_path)
addpath(genpath(fullfile(FilenameInfo.SPM_path,'toolbox','marsbar-0.44')))
foldername      = fullfile(FilenameInfo.DataPath,exp_folders{experiment});
batch_folder    = FilenameInfo.Batch_path;


% set the default setting for including the DLPFC node in the DCMs
if ( isempty(include_DLPFC) && experiment == 1 )
    include_DLPFC = 0;
end

% get the subject list
Subject_List = [dir(fullfile(foldername,'1*')); dir(fullfile(foldername,'2*')); dir(fullfile(foldername,'3*'))];


% specify the subjects
if ( isempty(subject) )
    subject_analyze = 1:length(Subject_List);
else
    subject_analyze = subject;
end

% initialize SPM version
spm('Defaults','fMRI');
spm_jobman('initcfg');


% specify the radius of the sphere
sphere_radius = 8;

marsbar

% display progress
disp('Compute mean contrast image values -----------------')

% iterate over subjects
for subject = subject_analyze

    % subject name
    Subject = Subject_List(subject).name;

    % display progress
    disp(['-- Subject ' Subject])
    
    % define the folder where the mean contrast values are stored
    CI_foldername = fullfile(foldername,Subject,'FirstLevel',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'mean_CI_VOI','NeuroSynth');
    
    % create the folder
    if ( ~exist(CI_foldername,'dir') )
        mkdir(CI_foldername)
    end
    
    % define the folder where the VOIs are stored
    VOI_foldername = fullfile(foldername,Subject,'FirstLevel',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'timeseries_VOI_adjusted','NeuroSynth');
    
    % get the filenames of VOIs
    if ( include_DLPFC )
        VOI_files = [dir(fullfile(VOI_foldername,'VOI_DLPFC*t_local_1.mat')); dir(fullfile(VOI_foldername,'VOI_Amyg*t_local_1.mat')); dir(fullfile(VOI_foldername,'VOI_FFA*t_local_1.mat')); dir(fullfile(VOI_foldername,'VOI_OFA*t_local_1.mat'))];
    else
        VOI_files = [dir(fullfile(VOI_foldername,'VOI_Amyg*t_local_1.mat')); dir(fullfile(VOI_foldername,'VOI_FFA*t_local_1.mat')); dir(fullfile(VOI_foldername,'VOI_OFA*t_local_1.mat'))];
    end
    
    % intialize the cell
    data_CI_AllContrasts = cell(1,6);
    
    % compute the functional connectivity measures
    if ( ~isempty(VOI_files) )
        
        % change the current directory
        cd(CI_foldername)
        
        % load the VOI time series for all regions of interest
        for number_of_regions = 1:length(VOI_files)

            % load the files
            temp = load(fullfile(VOI_foldername,VOI_files(number_of_regions).name));
            
            % get the center coordinate
            xyz = temp.xY.xyz';
            
            % specify sphere center
            sphere_center = xyz;
            
            % create the sphere
            sphere_roi = maroi_sphere(struct('centre', sphere_center,'radius', sphere_radius));
            
            % define sphere name using coordinates
            coordsx     = num2str(sphere_center(1));
            coordsy     = num2str(sphere_center(2));
            coordsz     = num2str(sphere_center(3));
            spherelabel = sprintf('%s_%s_%s', coordsx, coordsy, coordsz);
            sphere_roi  = label(sphere_roi, spherelabel);
            
            % save ROI as MarsBaR ROI file
            saveroi(sphere_roi, fullfile(pwd, ['temp_' sprintf('%dmm_%s_roi.mat',sphere_radius, spherelabel)]));
            
            % get ROI
            files = dir('temp_*roi.mat');
            
            % save to image
            mars_rois2img(files(1).name, [VOI_files(number_of_regions).name(5:end-3) 'img'])
            
            % delete the mat file
            delete('*roi.mat')
            
            % compute the 
            for contrast = 1:6
                
                % load the ImCalc Batch
                load([batch_folder 'Batch_CreateMaskImCalc.mat'])
                
                % modify the matlabbatch
                matlabbatch{1}.spm.util.imcalc.input{1}     = fullfile(foldername,Subject,'FirstLevel',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},['con_000' num2str(contrast) '.nii']);
                matlabbatch{1}.spm.util.imcalc.input{2}     = fullfile(CI_foldername,[VOI_files(number_of_regions).name(5:end-3) 'img']);
                matlabbatch{1}.spm.util.imcalc.output       = [VOI_files(number_of_regions).name(5:end-4) '_con_000' num2str(contrast) '.img'];
                matlabbatch{1}.spm.util.imcalc.outdir{1}    = CI_foldername;
                matlabbatch{1}.spm.util.imcalc.expression   = 'i1.*i2';
                
                % disable warnings due to unequal volume dimensions
                warning off
                
                % run the matlabbatch
                spm_jobman('run',matlabbatch)
                
                % enable warnings
                warning on
                
                % clear the matlabbatch
                clear matlabbatch
                
                % load the data
                V    = spm_vol([VOI_files(number_of_regions).name(5:end-4) '_con_000' num2str(contrast) '.img']);
                data = spm_read_vols(V);
                
                % get the mean and std
                data_CI_AllContrasts{contrast}((number_of_regions*2)-1) = mean(data(data~=0));
                data_CI_AllContrasts{contrast}(number_of_regions*2)     = std(data(data~=0));
                
            end
        end
    end
    
    % delete the files created
    delete([CI_foldername '/*img'])
    delete([CI_foldername '/*hdr'])
    delete([CI_foldername '/*mat'])
    
    % save the data
    save(fullfile(CI_foldername,['ContrastImage_measures' DLPFC_name{include_DLPFC+1} '_local.mat']),'data_CI_AllContrasts')
    
end

end