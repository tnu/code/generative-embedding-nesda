function BMA_DCM_RandRest_singleSubject_MainExperiment(experiment,SPM_prep,include_DLPFC,basis_set,compute)
% Averages across the different Dynamic Causal Models (DCMs) using fixed effects
% Bayesian model averaging (BMA) within each subject to identify the model 
% parameters independent of the exact model architecture. Note that BMA is 
% based on the negative free energy which is an lower-bound approximation 
% to the log model evidence and thus takes into account model accuracy and 
% complexity. Importantly, this function generates BMA parameter estimates 
% for each subject individually to prevent cross-talk that could bias classification 
% accuracy. Also, this function uses the DCMs from the random restart procedure 
% in order to ensure that the best solution is found.
% 
% Input:
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   SPM_prep        -- (0) NESDA pipeline (1) SPM preprocessing
%   include_DLPFC   -- (0) not include DLPFC, (1) include DLPFC
%   basis_set     	-- basis set: (0) only HRF, (1) temporal derivative, (2) informed basis set
%   compute         -- (0) only build and get BMS file, (1) run BMA
%
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2016
% Copyright 2016 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% set the random number seed
load('rngSeed.mat')
rng(rngSeed);

% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
exp_folders = {'FACES/','ToL/'};

% set folders
SPM_folder           = {'','SPM_preprocessing/'};
DLPFC_name           = {'','_DLPFC'};
setting_folder       = {'woDLPFC','wDLPFC'};
basis_folder         = {'HRF','TD','IBS'};


% set the SPM path and the path to the experiment
addpath(FilenameInfo.SPM_path)
foldername      = fullfile(FilenameInfo.DataPath,exp_folders{experiment});
homefolder      = FilenameInfo.Exp_path;


% define some variables
spm('Defaults','fMRI');
spm_jobman('initcfg');


% get the subject list
Subject_List = [dir(fullfile(foldername,'1*')); dir(fullfile(foldername,'2*')); dir(fullfile(foldername,'3*'))];

% define the name of the different models
name = {'model_FF_INTER','model_FB_INTER','model_RC_INTER',...
    'model_FF_noINTER','model_FB_noINTER','model_RC_noINTER',...
    'model_noMOD'};


% asign an old subject list
Subject_List_old = Subject_List;
Subject_List     = [];

% get the classes of the subjects
temp = load(fullfile(homefolder,'LCGA_classes','LCGA_classes.mat'));
LCGA_classes = temp.LCGA_classes;

% get all the included subjects in Schmaal et al. (2015)
temp2 = xlsread(fullfile(homefolder,'information','AllSubjects_Faces.xlsx'));
AllSubjects_Schmaal = temp2;

% names of all subjects
Subject_List_old_name = cell(length(Subject_List_old),1);

% find the subjects that are also listed in the LCGA classes
for int = 1:length(Subject_List_old)

    % asign subject names
    Subject_List_old_name{int} = Subject_List_old(int).name;

    % find the index of the subject in the LCGA classes
    index_LCGA = find(LCGA_classes(:,1) == str2double(Subject_List_old(int).name),1);

    if ( ~isempty(index_LCGA) )
        
        % get only the subjects that were also included in Schmaal et al.
        index = find(AllSubjects_Schmaal == str2double(Subject_List_old(int).name),1);
        
        % check whether there is DCM data
        existDCM = exist(fullfile(foldername,Subject_List_old(int).name,'FirstLevel_DCM_RandRestart','spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth',['DCM_NESDA_' name{1} DLPFC_name{include_DLPFC+1} '_local_001.mat']),'file');

        % asign the subject names and class labels
        if ( ~isempty(index) )
            if ( ~isempty(index_LCGA) && existDCM )
                if ( isempty(Subject_List) )
                    Subject_List(1).name       = Subject_List_old(int).name;
                    LCGA_classes_restricted(1) = LCGA_classes(index_LCGA,2);
                else
                    Subject_List(end+1).name       = Subject_List_old(int).name;
                    LCGA_classes_restricted(end+1) = LCGA_classes(index_LCGA,2);
                end
            end
        end
    end
end

% display the number of subjects in the restricted sample
disp(length(Subject_List))


% specify the subject_name array and random_restart array
AllSubjects         = cell(1,length(Subject_List));
IndMaxF_AllSubjects = NaN(length(Subject_List),length(name));


% subject counter
s = 0;

% missing subjects
sub_missing = [];

    
% load all the models of all the subjects
for subject = 1:length(Subject_List)
    
    % load the matlabbatch
    load(fullfile(homefolder,'Batch_BayesianModelAveragingSingleSubject.mat'))
    
    
    % set the foldername 
    matlabbatch{1}.spm.dcm.bms.inference.dir{1} = [];

    % define the BMS folder
    BMS_folder = fullfile(foldername,'DCM_GroupAnalysis','spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal','Bayesian_model_averaging_RandRest_single',[setting_folder{include_DLPFC+1} '_local']);

    % create the Bayesian model selection folder
    if ( ~exist(BMS_folder,'dir') )
        mkdir(BMS_folder)
    end
    
    % check whether negative free energy file is found
    F_found_file = 0;
    
    % load the negative free energies
    if ( subject == 1 )
        if ( exist(fullfile(BMS_folder,'F_all.mat'),'file') )
            load(fullfile(BMS_folder,'F_all.mat'))
            fprintf('\nFound negative free energies...\n\n')
            F_found_file = 1;
        end
    end
    
    % get the subject name
    Subject = Subject_List(subject).name;
    
    % create the folder
    if ( ~exist(fullfile(BMS_folder,Subject),'dir') )
        mkdir(fullfile(BMS_folder,Subject))
    end
    
    % asign the BMS folder
    matlabbatch{1}.spm.dcm.bms.inference.dir{1} = fullfile(BMS_folder,Subject);


    % define the folder where the SPM.mat (DCM regressors) is stored
    DCM_regressors_foldername = fullfile(foldername,Subject,'FirstLevel_DCM_RandRestart');

    % check whether models have been estimated for that subject
    if ( exist(fullfile(DCM_regressors_foldername,'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth',['DCM_NESDA_' name{1} DLPFC_name{include_DLPFC+1} '_local_001.mat']),'file') )

        % increase subject counter
        s = s + 1;

        % create entry for this subject
        matlabbatch{1}.spm.dcm.bms.inference.sess_dcm{1} = matlabbatch{1}.spm.dcm.bms.inference.sess_dcm{1};
        matlabbatch{1}.spm.dcm.bms.inference.sess_dcm{1}.dcmmat = [];

        % display progress
        disp(['Find max RandomRestart from Subject ' Subject])
        
        % asign the subjects models
        for model_number = 1:length(name)
            
            % folder for random starting values
            RSV_folder = 'RandomStartingValues';
            
            % get the number of random restarts
            files_RandRest = dir(fullfile(foldername,RSV_folder,'spm12_v7487',['Model_' name{model_number} DLPFC_name{include_DLPFC+1} '/VB_RSV*.mat']));
            NR_RandRest    = length(files_RandRest); 
            F_temp         = NaN(1,NR_RandRest);

            % check whether all files are present and get neg. free energy
            if ( exist('F_all','var') )
                F_temp = F_all(model_number,1:NR_RandRest,s);
            else
                
                % display that not the neg free energy file was used
                if ( F_found_file == 1 )
                    fprintf('\nNegative free energy file not used...\n\n')
                end
                
                for restart = 1:NR_RandRest
                    restart_name = ['00' num2str(restart)];
                    restart_name = restart_name(end-2:end);

                    % filename
                    filename_temp = fullfile(DCM_regressors_foldername,'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth',['DCM_NESDA_' name{model_number} DLPFC_name{include_DLPFC+1} '_local_' restart_name '.mat']);

                    try
                        load(filename_temp);
                        F_temp(restart) = DCM.F;
                    catch
                        sub_missing     = [sub_missing, {Subject}];
                    end
                end
            end

            % find the best solution (maximal neg. free energy)
            [~, F_max_ind] = max(F_temp);

            % get the filename appendix
            restart_max_name = ['00' num2str(F_max_ind)];
            restart_max_name = restart_max_name(end-2:end);

            % get the filename
            filename = fullfile(DCM_regressors_foldername,'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth',['DCM_NESDA_' name{model_number} DLPFC_name{include_DLPFC+1} '_local_' restart_max_name '.mat']);

            % asign the filename of each model
            matlabbatch{1}.spm.dcm.bms.inference.sess_dcm{1}.dcmmat = [matlabbatch{1}.spm.dcm.bms.inference.sess_dcm{1}.dcmmat; {filename}];

            % asign the subject name
            AllSubjects{s}                      = Subject;
            IndMaxF_AllSubjects(s,model_number)	= F_max_ind;

        end

        fprintf('found from Subject %d %d %d %d %d %d %d \n',IndMaxF_AllSubjects(s,:))
        
    end

    
    % save the matlabbatch
    save(fullfile(BMS_folder,Subject,'BMS_comparison.mat'), 'matlabbatch');
    
    
    % if computing
    if ( compute )
    
        % check if BMS file already exists
        if ( exist(fullfile(BMS_folder,Subject,'BMS.mat'),'file') )
            delete(fullfile(BMS_folder,Subject,'BMS.mat'))
        end

        spm_jobman('run',matlabbatch)
    end
    
    % combine all the BMS results and the names of the included subjects & index of maxF
    temp = load(fullfile(BMS_folder,Subject,'BMS.mat'));
    temp_all.BMS.DCM.ffx.bma.mEps{s}         = temp.BMS.DCM.ffx.bma.mEp;
    temp_all.BMS.DCM.ffx.AllSubjects         = AllSubjects;
    temp_all.BMS.DCM.ffx.IndMaxF_AllSubjects = IndMaxF_AllSubjects;
    
    % delete the subject folder
    rmdir(fullfile(BMS_folder,Subject),'s')
    
end

% asign the results
BMS = temp_all.BMS;

% store the results
if ( ~isempty(BMS) )
    save(fullfile(BMS_folder,'BMS.mat'),'BMS')
end

end
