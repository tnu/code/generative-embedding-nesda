#! /bin/bash

set -e

# Load the matlab module
module load matlab


# Copy the raw data (anatomical and functional MRI, logfiles)
for ID in {1..263}
do
	bsub -W 1:00 -J "job_copy_$ID" -o log_copy_"$ID" matlab -singleCompThread -r "copy_NESDA_data($ID,0,0)"
done


# Copy the starting values of the mutli-start model inversion
bsub -W 1:00 -J "job_copyVBSV" -o log_copyVBSV matlab -singleCompThread -r "copy_NESDA_VBSV(0,0)"


# IDs for the preprocessing
IDS="1 5 6 7 8 9 10 11 18 30 47 48 50 51 52 61 64 68 76 78 85 88 91 94 104 107 112 114 117 132 133 140 143 144 146 147 149 154 155 156 159 160 163 164 168 170 173 183 184 185 191 211 213 215 218 219 220 222 223 224 225 226 227 228 229 230 231 232 236 237 241 244 245 246 247 248 249 250 251 252 253 255 256 257 261"


# Run the preprocessing for every subject
for ID in $IDS
do 
	bsub -W 2:00 -J "job_preproc_$ID" -w 'ended("job_copy*")' -o log_preprocessing_"$ID" matlab -singleCompThread -r "preprocessing_SPM_MainExperiment($ID,1,1)"
done


# Run the first level once the preprocessing has finished
for ID in $IDS
do
	bsub -W 2:00 -J "job_firstlevel_$ID" -w 'ended("job_preproc*")' -o log_firstlevel_"$ID" matlab -singleCompThread -r "first_level_MainExperiment($ID,1,1,2)"
done


# Extract VOI signal time series from the pre-defined regions of interest
for ID in $IDS
do
	bsub -W 2:00 -J "job_timeseries_$ID" -w 'ended("job_firstlevel*")' -o log_timeseries_"$ID" matlab -singleCompThread -r "extract_VOI_timeseries_ROI_MainExperiment($ID,1,1,2)"
done


# Run the second level once the first level has finished
bsub -W 2:00 -w 'ended("job_firstlevel*")' -o log_secondlevel matlab -singleCompThread -r "second_level_MainExperiment(1,1,1,1,2,1)"


# Compute functional connectivity measures among time-series
bsub -W 4:00 -w 'ended("job_timeseries*")' -o log_FC matlab -singleCompThread -r "estimate_FC_MainExperiment([],1,1,0,2)"


# evaluate the mean and standard deviation of BOLD signal in regions of interest
for ID in $IDS
do
	bsub -W 2:00 -J "job_conimag_$ID" -w 'ended("job_timeseries*")' -o log_CI_"$ID" matlab -singleCompThread -r "estimate_CI_MainExperiment($ID,1,1,0,2)"
done


# collect the local BOLD activity measures
bsub -W 2:00 -w 'ended("job_conimag*")' -o log_getCI matlab -singleCompThread -r "get_CI_MainExperiment(1,1,0,2)"


# Create the input regressors for DCM
for ID in $IDS
do
	bsub -W 2:00 -J "job_createSPM_$ID" -w 'ended("job_firstlevel*")' -o log_createSPM_"$ID" matlab -singleCompThread -r "create_DCM_regressors_MainExperiment($ID,1,1,1)"
done


# Run the model inversion for all subjects and starting values
for ID in $IDS
do
	for VBSV in {1..100}
	do
		bsub -W 24:00 -R "rusage[mem=7000]" -J "job_inversion_$ID,$VBSV" -w 'ended("job_createSPM_*")' -o log_inversion_"$ID" matlab -singleCompThread -r "create_and_estimate_DCM_RandRest_MainExperiment($ID,[],1,1,0,2,$VBSV)"
	done
done


# check which DCMs crashed on Euler (as this happens from time to time) and re-run model estimation for those
for ID in $IDS
do
	for model in {1..7}
	do
		bsub -W 4:00 -R "rusage[mem=7000]" -J "job_rerun_inversion_$ID,$model" -w 'ended("job_inversion_261*") && ended("job_inversion_25*")' -o log_rerun_inversion_"$ID" matlab -singleCompThread -r "create_and_estimate_DCM_RandRest_rerun_MainExperiment($ID,$model,1,1,0,2)"
	done
done


# Get the indices of the best VB starting values (maximal negative free energies per subject and model)
bsub -W 2:00 -R "rusage[mem=7000]" -J "job_getF" -w 'ended("job_rerun_inversion*")' -o log_getF matlab -singleCompThread -r "get_negF_DCM_RandRest_MainExperiment(1,1,0,2,1)"


# Compute the Bayesian model averages (BMA)
bsub -W 2:00 -J "job_BMA" -w 'ended("job_getF")' -o log_BMA matlab -singleCompThread -r "BMA_DCM_RandRest_singleSubject_MainExperiment(1,1,0,2,1)"
