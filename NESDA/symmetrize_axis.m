function symmetrize_axis()
% Make axis symmetric around zero
% 
% Input:
% 
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the upper and lower limit
x_limits = xlim();
y_limits = ylim();
z_limits = zlim();

% get maximum limit
x_limit_max = max(abs(x_limits));
y_limit_max = max(abs(y_limits));
z_limit_max = max(abs(z_limits));

% make avis symmetric
xlim([-1*x_limit_max x_limit_max])
ylim([-1*y_limit_max y_limit_max])
zlim([-1*z_limit_max z_limit_max])

end