function permutation_balanced_accuracy_SVM_MainExperiment(FilenameInfo,model,experiment,regress_covariates)
% Performs a permutation test to assess the significance of a classifier 
% to distinguish between the different patient subgroups - namely fast 
% remission, gradualy improving, and chronic - as well as whether one 
% classifier is significantly better than the other (i.e., DCM vs. FC vs. CI). 
% The script uses support vector machines (SVM) in accordance with the 
% classical approach used by Kay Brodersen.
%
% Input:
%   FilenameInfo    -- important definitions for the experiment
%   model           -- model to be analyzed
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   regress_covariates	-- regress out covariates (default: 0)
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% close all figures
close all

% load the paths if necessary
if ( isempty(FilenameInfo) )
    
    % get the path
    m_path = mfilename('fullpath');
    m_path = m_path(1:find(m_path=='/',1,'last'));

    try
        load(fullfile(m_path,'ConfigFile.mat'))
    catch err
        disp('Need to specify FilenameInfo containing paths!')
        rethrow(err)
    end
end


% default settings
SPM_prep        = 1;
classifier      = 3;
include_DLPFC   = 0;
basis_set       = 2;
version         = [];

% set the experiment
exp_folder = {'FACES/','ToL/'};

% set folders
SPM_folder          = {'','SPM_preprocessing/'};
SPM_folder_text     = {'',' / SPMprep'};
DLPFC_name          = {'','_DLPFC'};
basis_folder        = {'HRF','TD','IBS',''};
settings_folder    	= {'woDLPFC','wDLPFC'};
regress_name        = {'','_orth'};

% define the SPM version
classifier_name  = {'GPC','SVM','FITCSVM'};

% add path
addpath(genpath(fullfile(FilenameInfo.MATLAB_path,'PBAR')))
addpath(genpath(fullfile(FilenameInfo.MATLAB_path,'bipolar_colormap')))


% set the SPM path and the path to the experiment
foldername = [FilenameInfo.DataPath exp_folder{experiment}];


% define the name of the different models
name = {'BMA','model_FF_INTER','model_FB_INTER','model_RC_INTER','model_FF_noINTER',...
    'model_FB_noINTER','model_RC_noINTER','model_noMOD'};

% specify the model folder name
if ( isempty(model) )
    model_folder  = 'Bayesian_model_selection_RandRest';
elseif ( model == 0 )
    model_number  = 0; 
    model_folder  = 'Bayesian_model_averaging_RandRest_single';
else
    model_number  = model;
    model_folder  = 'IndividualModel_RandRest';
end

% group comparison names
group_names = {'CHR_REM','CHR_IMP','IMP_REM'};


% load the results of the generative embedding
if ( ~isempty(model) )
    Perm = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding_Permutation' regress_name{regress_covariates+1} name{model_number+1} DLPFC_name{include_DLPFC+1} '_local.mat']));
else
    Perm = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding_PermutationBMS' regress_name{regress_covariates+1} DLPFC_name{include_DLPFC+1} '_local.mat']));
end

% load the actual classifcation results
if ( ~isempty(model) )
    Effect = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding' regress_name{regress_covariates+1} name{model_number+1} DLPFC_name{include_DLPFC+1} '_local.mat']));
else
    Effect = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbeddingBMS' regress_name{regress_covariates+1} DLPFC_name{include_DLPFC+1} '_local.mat']));
end


% define the results arrays
BaccPerm               = cell(size(Perm.results,1),1);
BaccPerm_FC            = cell(size(Perm.results,1),1);
BaccPerm_CI            = cell(size(Perm.results,1),1);

Bacc                   = NaN(size(Perm.results,1),1);
Bacc_FC                = NaN(size(Perm.results,1),1);
Bacc_CI                = NaN(size(Perm.results,1),1);

p_val_bacc_NonParam           = NaN(size(Perm.results,1),1);
p_val_bacc_FC_NonParam        = NaN(size(Perm.results,1),1);
p_val_bacc_CI_NonParam        = NaN(size(Perm.results,1),1);
p_val_bacc_DCM_vs_FC_NonParam = NaN(size(Perm.results,1),1);
p_val_bacc_DCM_vs_CI_NonParam = NaN(size(Perm.results,1),1);

ul_bacc_NonParam    = NaN(2,size(Perm.results,1),1);
ul_bacc_FC_NonParam = NaN(2,size(Perm.results,1),1);
ul_bacc_CI_NonParam	= NaN(2,size(Perm.results,1),1);


% get the weights from the permutations
for group_comp = 1:size(Perm.results,1)
    for Nr_perm = 1:size(Perm.results,2)
        
        % balanced accuracies for the random permutations
        try
            BaccPerm{group_comp}(Nr_perm) = Perm.results(group_comp,Nr_perm).class.balanced_accuracy;
        catch
            BaccPerm{group_comp}(Nr_perm) = NaN;
        end
        
        % balanced accuracies (FC) for the random permutations
        try
            BaccPerm_FC{group_comp}(Nr_perm) = Perm.results(group_comp,Nr_perm).class_FC.balanced_accuracy;
        catch
            BaccPerm_FC{group_comp}(Nr_perm) = NaN;
        end
        
        % balanced accuracies (CI) for the random permutations
        try
            BaccPerm_CI{group_comp}(Nr_perm) = Perm.results(group_comp,Nr_perm).class_CI.balanced_accuracy;
        catch
            BaccPerm_CI{group_comp}(Nr_perm) = NaN;
        end
    end
end


% obtain actual weights and comupte p-values
for group_comp = 1:size(Effect.results,2)
    
    % balanced accuracy
    Bacc(group_comp) = Effect.results(group_comp).class.balanced_accuracy;
    
    % balanced accuracy (FC)
    Bacc_FC(group_comp) = Effect.results(group_comp).class_FC.balanced_accuracy;
    
    % balanced accuracy (CI)
    Bacc_CI(group_comp) = Effect.results(group_comp).class_CI.balanced_accuracy;
    
    
    % evaluate the non-parameteric p-value for the balanced accuracy
    p_val_bacc_NonParam(group_comp) = sum(BaccPerm{group_comp} >= Bacc(group_comp))/length(BaccPerm{group_comp});
    
    % evaluate the non-parameteric p-value for the balanced accuracy
    p_val_bacc_FC_NonParam(group_comp) = sum(BaccPerm_FC{group_comp} >= Bacc_FC(group_comp))/length(BaccPerm_FC{group_comp});
    
    % evaluate the non-parameteric p-value for the balanced accuracy
    p_val_bacc_CI_NonParam(group_comp) = sum(BaccPerm_CI{group_comp} >= Bacc_CI(group_comp))/length(BaccPerm_CI{group_comp});
    
    
    % evaluate the confidence interval of the null distribution
    Bacc_Perm_sort = sort(BaccPerm{group_comp},'descend');
    ul_bacc_NonParam(1,group_comp) = Bacc_Perm_sort(0.05*length(BaccPerm{group_comp}));
    ul_bacc_NonParam(2,group_comp) = Bacc_Perm_sort(length(BaccPerm{group_comp})-0.05*length(BaccPerm{group_comp}));
    
    % evaluate the confidence interval of the null distribution
    BaccPerm_FC_sort = sort(BaccPerm_FC{group_comp},'descend');
    ul_bacc_FC_NonParam(1,group_comp) = BaccPerm_FC_sort(0.05*length(BaccPerm{group_comp}));
    ul_bacc_FC_NonParam(2,group_comp) = BaccPerm_FC_sort(length(BaccPerm{group_comp})-0.05*length(BaccPerm{group_comp}));
    
    % evaluate the confidence interval of the null distribution
    BaccPerm_CI_sort = sort(BaccPerm_CI{group_comp},'descend');
    ul_bacc_CI_NonParam(1,group_comp) = BaccPerm_CI_sort(0.05*length(BaccPerm{group_comp}));
    ul_bacc_CI_NonParam(2,group_comp) = BaccPerm_CI_sort(length(BaccPerm{group_comp})-0.05*length(BaccPerm{group_comp}));
    
    
    % evaluate the non-parameteric p-value for the difference in balanced accuracy
    [~,p_FC] = testcholdout(Effect.results(group_comp).class.predicted_labels,Effect.results(group_comp).class_FC.predicted_labels,Effect.results(group_comp).class.true_labels);
    p_val_bacc_DCM_vs_FC_NonParam(group_comp) = p_FC;
    
    % evaluate the non-parameteric p-value for the difference in balanced accuracy
    [~,p_CI] = testcholdout(Effect.results(group_comp).class.predicted_labels,Effect.results(group_comp).class_CI.predicted_labels,Effect.results(group_comp).class.true_labels);
    p_val_bacc_DCM_vs_CI_NonParam(group_comp) = p_CI;
    
end




% significance text
sig_text = {'','*'};

% summarize the classification results
fprintf(['\nClassification_local - ' classifier_name{classifier} ' (NeuroSynth' SPM_folder_text{SPM_prep+1} ' / Schmaal / woDLPFC):\n'])
fprintf(['\t\t\t' group_names{1} '\t\t' group_names{2} '\t\t' group_names{3} '\n'])
fprintf('Balanced accuracy: \t '); fprintf((p_val_bacc_NonParam(1)<0.05)+1,['%f' sig_text{(p_val_bacc_NonParam(1)<0.05)+1} '\t '],Effect.results(1).stats(1,3)); fprintf((p_val_bacc_NonParam(2)<0.05)+1,['%f' sig_text{(p_val_bacc_NonParam(2)<0.05)+1} '\t '],Effect.results(2).stats(1,3)); fprintf((p_val_bacc_NonParam(3)<0.05)+1,['%f' sig_text{(p_val_bacc_NonParam(3)<0.05)+1} '\n'],Effect.results(3).stats(1,3));
fprintf('p-value (non-param): \t %f \t %f \t %f \n',p_val_bacc_NonParam(1),p_val_bacc_NonParam(2),p_val_bacc_NonParam(3));

% summarize the classification results (FC)
fprintf(['\nClassification FC_local - ' classifier_name{classifier} ' (NeuroSynth' SPM_folder_text{SPM_prep+1} ' / Schmaal / woDLPFC):\n'])
fprintf(['\t\t\t' group_names{1} '\t\t' group_names{2} '\t\t' group_names{3} '\n'])
fprintf('Balanced accuracy: \t '); fprintf((p_val_bacc_FC_NonParam(1)<0.05)+1,['%f' sig_text{(p_val_bacc_FC_NonParam(1)<0.05)+1} '\t '],Effect.results(1).stats_FC(1,3)); fprintf((p_val_bacc_FC_NonParam(2)<0.05)+1,['%f' sig_text{(p_val_bacc_FC_NonParam(2)<0.05)+1} '\t '],Effect.results(2).stats_FC(1,3)); fprintf((p_val_bacc_FC_NonParam(3)<0.05)+1,['%f' sig_text{(p_val_bacc_FC_NonParam(3)<0.05)+1} '\n'],Effect.results(3).stats_FC(1,3));
fprintf('p-value (non-param): \t %f \t %f \t %f \n',p_val_bacc_FC_NonParam(1),p_val_bacc_FC_NonParam(2),p_val_bacc_FC_NonParam(3));

% summarize the classification results (CI)
fprintf(['\nClassification CI_local - ' classifier_name{classifier} ' (NeuroSynth' SPM_folder_text{SPM_prep+1} ' / Schmaal / woDLPFC):\n'])
fprintf(['\t\t\t' group_names{1} '\t\t' group_names{2} '\t\t' group_names{3} '\n'])
fprintf('Balanced accuracy: \t '); fprintf((p_val_bacc_CI_NonParam(1)<0.05)+1,['%f' sig_text{(p_val_bacc_CI_NonParam(1)<0.05)+1} '\t '],Effect.results(1).stats_CI(1,3)); fprintf((p_val_bacc_CI_NonParam(2)<0.05)+1,['%f' sig_text{(p_val_bacc_CI_NonParam(2)<0.05)+1} '\t '],Effect.results(2).stats_CI(1,3)); fprintf((p_val_bacc_CI_NonParam(3)<0.05)+1,['%f' sig_text{(p_val_bacc_CI_NonParam(3)<0.05)+1} '\n'],Effect.results(3).stats_CI(1,3));
fprintf('p-value (non-param): \t %f \t %f \t %f \n',p_val_bacc_CI_NonParam(1),p_val_bacc_CI_NonParam(2),p_val_bacc_CI_NonParam(3));

% display the difference
fprintf('\nComparison DCM - FC (p-value): \t %f \t %f \t %f\n',p_val_bacc_DCM_vs_FC_NonParam(1),p_val_bacc_DCM_vs_FC_NonParam(2),p_val_bacc_DCM_vs_FC_NonParam(3))
fprintf('Comparison DCM - CI (p-value): \t %f \t %f \t %f\n',p_val_bacc_DCM_vs_CI_NonParam(1),p_val_bacc_DCM_vs_CI_NonParam(2),p_val_bacc_DCM_vs_CI_NonParam(3))


% check if figure folder exists
if ( ~exist(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local']),'dir') )
    mkdir(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local']))
else
    
    % delete previous plots of necessary
    if ( exist(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],['SVM_PredictedLabels' regress_name{regress_covariates+1} '.ps']),'file') )
        delete(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],['SVM_PredictedLabels' regress_name{regress_covariates+1} '.ps']))
    end
    
    % delete previous plots of necessary
    if ( exist(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],['SVM_BalancedAccuracy' regress_name{regress_covariates+1} '.pdf']),'file') )
        delete(fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],['SVM_BalancedAccuracy' regress_name{regress_covariates+1} '.pdf']))
    end
end

% switch off warnings
warning off


% iterate over comparisons
for group_comp = 1:3
    
    % open the figure
    figure;
    
    % number of patients in groups
    NrGroups   = [15 15 31];
    NameGroups = {'CHR vs. REM','CHR vs. IMP','IMP vs. REM'};

    % plot the results array
    imagesc(Effect.results(group_comp).class.predicted_labels')
    colormap('bipolar')
    caxis([0 3])
    hold on
    for int = 1:length(Effect.results(group_comp).class.predicted_labels)
        if ( int ~= NrGroups(group_comp) )
            plot([int+0.5 int+0.5],[0.5 1.5],'k-','LineWidth',1)
        else
            plot([int+0.5 int+0.5],[0.5 1.5],'k-','LineWidth',4)
        end
    end
    colormap('bipolar')
    axis square
    caxis([0.5 2.8])
    set(gca,'ytick',1)
    set(gca,'yticklabel',' ')
    xlabel('subjects')
    title(NameGroups{group_comp})
    
    % save the figure
    h = gcf;
    set(h,'PaperOrientation','landscape');
    print(gcf, '-dpsc', fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],['SVM_PredictedLabels' regress_name{regress_covariates+1}]), '-fillpage', '-append');

end




% display the classification results
figure(4);
ha(1) = bar(1, Effect.results(1).stats(1,3), 'FaceColor', [0.05 0.05 0.9], 'FaceAlpha', 0.5);
hold on
text(0.85, 0.825, sig_text{(p_val_bacc_NonParam(1)<0.05)+1},'FontSize',28)
ha(2) = bar(2, Effect.results(1).stats_FC(1,3), 'FaceColor', [0.8 0.8 0.8]);
text(1.85, 0.825, sig_text{(p_val_bacc_FC_NonParam(1)<0.05)+1},'FontSize',28)
ha(3) = bar(3, Effect.results(1).stats_CI(1,3), 'FaceColor', [0.4 0.4 0.4]);
text(2.85, 0.825, sig_text{(p_val_bacc_CI_NonParam(1)<0.05)+1},'FontSize',28)

bar(5, Effect.results(2).stats(1,3), 'FaceColor', [0.05 0.05 0.9], 'FaceAlpha', 0.5)
text(4.85, 0.825, sig_text{(p_val_bacc_NonParam(2)<0.05)+1},'FontSize',28)
bar(6, Effect.results(2).stats_FC(1,3), 'FaceColor', [0.8 0.8 0.8])
text(5.85, 0.825, sig_text{(p_val_bacc_FC_NonParam(2)<0.05)+1},'FontSize',28)
bar(7, Effect.results(2).stats_CI(1,3), 'FaceColor', [0.4 0.4 0.4])
text(6.85, 0.825, sig_text{(p_val_bacc_CI_NonParam(2)<0.05)+1},'FontSize',28)

bar(9, Effect.results(3).stats(1,3), 'FaceColor', [0.05 0.05 0.9], 'FaceAlpha', 0.5)
text(8.85, 0.825, sig_text{(p_val_bacc_NonParam(3)<0.05)+1},'FontSize',28)
bar(10, Effect.results(3).stats_FC(1,3), 'FaceColor', [0.8 0.8 0.8])
text(9.85, 0.825, sig_text{(p_val_bacc_FC_NonParam(3)<0.05)+1},'FontSize',28)
bar(11, Effect.results(3).stats_CI(1,3), 'FaceColor', [0.4 0.4 0.4])
text(10.85, 0.825, sig_text{(p_val_bacc_CI_NonParam(3)<0.05)+1},'FontSize',28)

plot([1 2],[0.9 0.9],'k-')
text(1.3, 0.9, sig_text{(p_val_bacc_DCM_vs_FC_NonParam(1)<0.05)+1},'FontSize',28)
plot([1 3],[0.95 0.95],'k-')
text(1.8, 0.95, sig_text{(p_val_bacc_DCM_vs_CI_NonParam(1)<0.05)+1},'FontSize',28)


ylim([0 1])
xlim([0 12])
axis square
box off
set(gca,'ytick',0:0.25:1)
set(gca,'xtick',[2 6 10])
set(gca,'xticklabel',{[group_names{1}(1:3) '-' group_names{1}(5:7)],[group_names{2}(1:3) '-' group_names{2}(5:7)],[group_names{3}(1:3) '-' group_names{3}(5:7)]},'FontSize',12)
legend(ha,{'DCM','fun conn','con image'},'Location','SE')
ylabel('balanced accuracy','FontSize',14)

% save the figure
print(gcf, '-dpdf', fullfile(foldername,'Figures','spm12_v7487',SPM_folder{SPM_prep+1},'IBS','NeuroSynth','Schmaal',[settings_folder{include_DLPFC+1} '_local'],['SVM_BalancedAccuracy' regress_name{regress_covariates+1}]));

% switch on warnings
warning on

end