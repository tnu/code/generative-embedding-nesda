function stats = classify_simple_fitcsvm(labels, data, options, verbose)
% Runs leave-one-out cross-validation for aphasia dataset using a support
% vector machine (SVM) as a supervised machine learning algorithm. This
% script used the implementation in MATLAB (fitcsvm).
% 
% Input:
%   labels          -- true labels
%   data            -- data features
%   options         -- classification options (e.g., confounds)
%   verbose         -- output to the command window
%
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the seed of the RNG
load('rngSeed.mat')
rng(rngSeed);


% number of trials for leave-one-out cross-validation
nTrials = size(labels,1);

% predicted labels
predicted_labels = zeros(size(labels,1),1);
predicted_scores = zeros(size(labels,1),1);
decision_values	 = zeros(size(labels,1),1);

% object for trained SVMs
Md = cell(nTrials,1);

% leave-one-out cross-validation
for t = 1 : nTrials
    
    % Split up data into train and test data
    test_trials  = t;
    test_labels  = labels(test_trials);
    train_trials = setdiff(1:nTrials,t);
    
    % Training data and labels
    train_data = data(train_trials,:);
    train_labels = labels(train_trials);
    
    % test data
    test_data = data(test_trials,:);
    
    % regress out confounds
    if ( isfield(options,'confounds') && ~isempty(options.confounds) )
        
        % split confounds in training and test set
        train_confounds = options.confounds(train_trials,:);
        test_confounds  = options.confounds(test_trials,:);
        
        % fit the regression model to the training data and regress out confounds
        for Nr_param = 1:size(train_data,2)
            
            % fit the regression model to the training data
            train_confounds_beta = regress(train_data(:,Nr_param),train_confounds);
            
            % regress out confounds
            train_data(:,Nr_param) = train_data(:,Nr_param) - train_confounds * train_confounds_beta;
            test_data(:,Nr_param)  = test_data(:,Nr_param) - test_confounds * train_confounds_beta;
        
        end
    end
    
    % select all the features
    fs = 1:size(train_data,2);
    
    % get the training data of only best features
    train_data_fs = train_data(:,fs);
    
    % optimize the hyperparameters (Bayes optimization)
    Md1 = fitcsvm(train_data_fs,train_labels,'KernelFunction','linear','OptimizeHyperparameters','auto',...
            'HyperparameterOptimizationOptions',struct('AcquisitionFunctionName',...
            'expected-improvement-plus','ShowPlots',false));
      
    % get the test data of only best features
    test_data_best_feature = test_data(:,fs);
    
    % test classifier on the left-out trial
    [predicted_label_temp, score_temp] = predict(Md1,test_data_best_feature);
    
    % asign the predicted labels and posterior probabilities (scores)
    predicted_labels(t) = predicted_label_temp;
    predicted_scores(t)	= score_temp(2);
    
    % store the trained SVM
    Md{t} = Md1;
    
end


% Compute sample accuracy
stats.predicted_labels  = predicted_labels;
stats.predicted_scores  = predicted_scores;
stats.true_labels       = labels;
stats.decision_values	= decision_values;
stats.correct_trials    = (predicted_labels == labels);
stats.sample_accuracy   = sum(stats.correct_trials,1) / nTrials;
stats.p_accuracy        = 1 - binocdf(sum(stats.correct_trials,1), nTrials, 0.5);


% Compute balanced accuracy
stats.C = confusionmat(labels, predicted_labels);
stats.balanced_accuracy = (stats.C(1,1)/sum(stats.C(1,:)) + stats.C(2,2)/sum(stats.C(2,:)))/2;
stats.p_balanced_accuracy = bacc_p(stats.C);
[lower, upper] = bacc_ppi(stats.C,0.05);
stats.bounds_balanced_accuracy = [lower, upper];


% return the weight vectors
weights = NaN(length(Md),length(Md{1}.Beta));
for int = 1:length(Md)
    weights(int,:) = Md{int}.Beta';
end
stats.weights = weights;


% store the features
stats.features = data;



% Output balanced accuracy
if verbose
    disp(' ');
    disp(['Cross-validated balanced accuracy: ', ...
        num2str(stats.balanced_accuracy * 100), '%']);
    disp(['p = ', num2str(stats.p_balanced_accuracy)]);
    
    % Plot balanced accuracy (with error bars)
    figure;
    subplot(1, 2, 1);
    [lower, upper] = bacc_ppi(stats.C, 0.05);
    errorbar(0, stats.balanced_accuracy, stats.balanced_accuracy - lower, ...
        upper - stats.balanced_accuracy, 'linewidth', 2)
    hold on;
    plot([-1 1], [0.5 0.5], '-')
    axis([-1 1 0 1])
    title('balanced accuracy')
    
    % Plot balanced accuracy (full distribution)
    subplot(1, 2, 2);
    x = 0:0.01:1;
    y = betaavgpdf(x, stats.C(1, 1) + 1, stats.C(1, 2) + 1, ...
        stats.C(2, 2) + 1, stats.C(2, 1) + 1);
    plot(x, y, 'linewidth', 2);
    hold on;
    v = axis;
    plot([0.5 0.5], [0 v(4)], '-')
    title('full distribution')
    
end

end
