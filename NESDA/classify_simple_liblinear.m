function stats = classify_simple_liblinear(labels, data, norm, verbose)
% Runs leave-one-out cross-validation for aphasia dataset using a support
% vector machine (SVM) as a supervised machine learning algorithm. This
% script used the implementation in LIBLINEAR (rather than libsvm). Based on 
% previous code by Kay H. Brodersen.
% 
% Input:
%   labels          -- true labels
%   data            -- data features
%   norm            -- (1&2) l2-regularized, (3) in-built optimize C, (4) Crammer and Singer, (5) l1-regularized
%   verbose         -- output to the command window
%
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2016
% Copyright 2016 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% number of trials for leave-one-out cross-validation
nTrials = size(labels,1);

% set the hyperparameter of the SVM
if ( norm == 1 || norm == 2 )
    C = power(2,-1:1:10);
else
    C = 3;
end

% predicted labels
predicted_labels = zeros(size(labels,1),1);
decision_values	 = zeros(size(labels,1),1);

% leave-one-out cross-validation
for t = 1 : nTrials
    
    % Split up data into train and test data
    test_trials  = t;
    test_labels  = labels(test_trials);
    train_trials = setdiff(1:nTrials,t);
    
    % Training data and labels
    train_data = data(train_trials,:);
    train_labels = labels(train_trials);
    
    % Manual inner cross-validation or built-in routine
    if ( norm ~= 3 )
        
        % Internal cross-validation loop
        acc = zeros(length(C),1);
        
        % iterate over C (cost) parameters
        for j = 1:length(C)
            
            % predicted labels (inner loop)
            predicted2_labels = zeros(size(train_labels));
            
            % inner leave-one-out cross-validation
            for l = 1:length(train_labels)
                
                train2_trials = setdiff(1:length(train_labels),l);
                
                train2_data   = train_data(train2_trials,:);
                train2_labels = train_labels(train2_trials);
                test2_data    = train_data(l,:);
                test2_labels  = train_labels(l);

                % choose between l2-regularization and sparse l1-regularization and train SVM
                model = train(train2_labels, sparse(train2_data), ['-s ' num2str(norm) ' -c ' num2str(C(j)) ' -q']);
                
                % predict the left-out label after training the data 
                [predicted2_labels(l), ~, ~] = predict(test2_labels, sparse(test2_data), model, ' -q');

            end

            % evaluate confusion matrix and accuracy
            Cm = confusionmat(train_labels, predicted2_labels);
%             acc(j) = mean(diag(Cm)./sum(Cm,2));
            acc(j) = (Cm(1,1)/sum(Cm(1,:)) + Cm(2,2)/sum(Cm(2,:)))/2;
            
        end
        
        % get the best C value
        [~, j_best] = max(acc);
        
        % set the best C value
        C_best = C(j_best);
        
    else
        
        % train the data to find the best C
        [~, temp] = evalc('train(train_labels, sparse(train_data), ''-s 2 -C -c 1 -v 10 -q'')');
%         [~, temp] = evalc('train(train_labels, sparse(train_data), ''-s 2 -C -c 3 -v 10 -q'')'); % rDCM
        C_best = temp(1);
        
    end
    
    % choose between l2-regularization and sparse l1-regularization and train SVM
    model_allFolds(t) = train(train_labels, sparse(train_data), ['-s ' num2str(norm) ' -c ' num2str(C_best) ' -q']);
    
    % Test data
    test_data = data(test_trials,:);
    
    % Test classifier on the left-out trial
    [predicted_labels(t), ~, ~] = predict(test_labels, sparse(test_data), model_allFolds(t), '-q');
    
end


% Compute sample accuracy
stats.predicted_labels  = predicted_labels;
stats.decision_values	= decision_values;
stats.correct_trials    = (predicted_labels == labels);
stats.sample_accuracy   = sum(stats.correct_trials,1) / nTrials;
stats.p_accuracy        = 1 - binocdf(sum(stats.correct_trials,1), nTrials, 0.5);

% Output results of sample accuracy
if verbose
    disp(' ');
    disp('================================');
    disp(' ');
    disp(['Cross-validated sample accuracy: ', ...
        num2str(stats.sample_accuracy * 100), '%']);
    disp(['p = ', num2str(stats.p_accuracy)]);
end

% Compute balanced accuracy
stats.C = confusionmat(labels, predicted_labels);
stats.balanced_accuracy = (stats.C(1,1)/sum(stats.C(1,:)) + stats.C(2,2)/sum(stats.C(2,:)))/2;
stats.p_balanced_accuracy = bacc_p(stats.C);
[lower, upper] = bacc_ppi(stats.C,0.05);
stats.bounds_balanced_accuracy = [lower, upper];


% return the weight vectors
weights = NaN(length(model_allFolds),length(model_allFolds(1).w));
for int = 1:length(model_allFolds)
    weights(int,:) = model_allFolds(int).w(1,:);
end
stats.weights = weights;


% Output balanced accuracy
if verbose
    disp(' ');
    disp(['Cross-validated balanced accuracy: ', ...
        num2str(stats.balanced_accuracy * 100), '%']);
    disp(['p = ', num2str(stats.p_balanced_accuracy)]);
    
    % Plot balanced accuracy (with error bars)
    figure;
    subplot(1, 2, 1);
    [lower, upper] = bacc_ppi(stats.C, 0.05);
    errorbar(0, stats.balanced_accuracy, stats.balanced_accuracy - lower, ...
        upper - stats.balanced_accuracy, 'linewidth', 2)
    hold on;
    plot([-1 1], [0.5 0.5], '-')
    axis([-1 1 0 1])
    title('balanced accuracy')
    
    % Plot balanced accuracy (full distribution)
    subplot(1, 2, 2);
    x = 0:0.01:1;
    y = betaavgpdf(x, stats.C(1, 1) + 1, stats.C(1, 2) + 1, ...
        stats.C(2, 2) + 1, stats.C(2, 1) + 1);
    plot(x, y, 'linewidth', 2);
    hold on;
    v = axis;
    plot([0.5 0.5], [0 v(4)], '-')
    title('full distribution')
    
end

% % store the SVM model
% [~, stats.model] = evalc('svmtrain(labels, data, ''-t 0 -c 4'');');
% stats.model_allFolds = model_allFolds;

end
