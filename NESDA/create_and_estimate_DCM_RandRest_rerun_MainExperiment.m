function create_and_estimate_DCM_RandRest_rerun_MainExperiment(subject,model,experiment,SPM_prep,include_DLPFC,basis_set)
% Checks which of the DCMs crashed on Euler and re-estimates the files. The
% function does exactly what the initial inversion function does and should
% just ensure that all files are present for subsequent analyses (BMA,
% SVM).
% 
% Input:
%   subject             -- subject to be analyzed
%   model               -- model to be analyzed
%   experiment          -- (1) FACES dataset, (2) Tower of London dataset
%   SPM_prep            -- (0) NESDA pipeline (1) SPM preprocessing
%   include_DLPFC       -- (0) only face emotion network, (1) include DLPFC
%   basis_set           -- basis set: (0) only HRF, (1) temporal derivative, (2) informed basis set
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2017
% Copyright 2017 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
exp_folders = {'FACES/','ToL/'};

% set the preprocessing folder
SPM_folder      = {'','SPM_preprocessing/'};
DLPFC_name      = {'','_DLPFC'};
basis_folder    = {'HRF','TD','IBS',''};


% set the SPM path and the path to the experiment
addpath(FilenameInfo.SPM_path)
foldername      = fullfile(FilenameInfo.DataPath,exp_folders{experiment});
home_foldername = FilenameInfo.Exp_path;


% set the default setting for including the DLPFC node in the DCMs
if ( isempty(include_DLPFC) && experiment == 1 )
    include_DLPFC = 0;
end

% initialize SPM version
spm('Defaults','fMRI');

% define the name of the different models
name = {'model_FF_INTER','model_FB_INTER','model_RC_INTER','model_FF_noINTER',...
    'model_FB_noINTER','model_RC_noINTER','model_noMOD'};


% get the subject list
Subject_List = [dir(fullfile(foldername,'1*')); dir(fullfile(foldername,'2*')); dir(fullfile(foldername,'3*'))];

% subject name
Subject = Subject_List(subject).name;

% define the model to analyze
if ( isempty(model) )
    model_analyze = 1:length(name);
else
    model_analyze = model;
end

% display progress
disp(['-- Subject ' Subject ': Creating DCM Model Space -----------------'])

    
% define the folder where the VOIs are stored
VOI_foldername = fullfile(foldername,Subject,'FirstLevel',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'timeseries_VOI_adjusted','NeuroSynth');

% get the filenames of VOIs
if ( include_DLPFC )
    VOI_files = [dir(fullfile(VOI_foldername,'VOI_DLPFC*t_local_1.mat')); dir(fullfile(VOI_foldername,'VOI_Amyg*t_local_1.mat')); dir(fullfile(VOI_foldername,'VOI_FFA*t_local_1.mat')); dir(fullfile(VOI_foldername,'VOI_OFA*t_local_1.mat'))];
else
    VOI_files = [dir(fullfile(VOI_foldername,'VOI_Amyg*t_local_1.mat')); dir(fullfile(VOI_foldername,'VOI_FFA*t_local_1.mat')); dir(fullfile(VOI_foldername,'VOI_OFA*t_local_1.mat'))];
end


% load the SPM.mat of the subject containing the DCM regressors etc.
load(fullfile(foldername,Subject,'FirstLevel_DCM','spm12_v7487',SPM_folder{SPM_prep+1},'SPM.mat'));

% set the number of VB starting values
restarts = 100;

% get time
tic;

% specify the different models
for model_number = model_analyze
    for restart_counter = 1:restarts
    
        % get the restart name
        restart_name = ['00' num2str(restart_counter)];
        restart_name = restart_name(end-2:end);

        % get the filename of that radom restart
        filename_temp = fullfile(foldername,Subject,'FirstLevel_DCM_RandRestart','spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth',['DCM_NESDA_' name{model_number} DLPFC_name{include_DLPFC+1} '_local_' restart_name '.mat']);

        % check whether the file can be loaded / exists
        try
            load(filename_temp)
            rerun = 0;
            disp(['-- Subject ' Subject ' (' basis_folder{basis_set+1} '): Model ' name{model_number}(7:end) DLPFC_name{include_DLPFC+1} '_local_' restart_name ' estimated -----------------'])
        catch
            rerun = 1;
        end

        % rerun files that could not be loaded
        if ( rerun == 1 )
            
            % define the vector that specifies which regressors enter model
            if model_number < 22
                vector = [1 0 1 1 1 1 0 0];
                vector = (vector == 1);
            else
                vector = [1 1 0 0 0 0 0 0];
                vector = (vector == 1);
            end

            % clear the DCM structure
            clear DCM

            % load the VOI time series for all regions of interest
            for number_of_regions = 1:length(VOI_files)

                % load the files
                load(fullfile(VOI_foldername,VOI_files(number_of_regions).name),'xY');

                % asign the time series
                DCM.xY(number_of_regions).name  = xY.name;
                DCM.xY(number_of_regions).Ic    = xY.Ic;
                DCM.xY(number_of_regions).Sess  = xY.Sess;
                DCM.xY(number_of_regions).xyz   = xY.xyz;
                DCM.xY(number_of_regions).def   = xY.def;
                DCM.xY(number_of_regions).spec  = xY.spec;
                DCM.xY(number_of_regions).str   = xY.str;
                DCM.xY(number_of_regions).XYZmm = xY.XYZmm;
                DCM.xY(number_of_regions).X0    = xY.X0;
                DCM.xY(number_of_regions).y     = xY.y;
                DCM.xY(number_of_regions).u     = xY.u;
                DCM.xY(number_of_regions).v     = xY.v;
                DCM.xY(number_of_regions).s     = xY.s;

            end

            % number of regions
            DCM.n = length(DCM.xY);

            % number of time points
            DCM.v = length(DCM.xY(1).u);

            % confound matrix (constant and DCT)
            DCM.Y.X0 = DCM.xY(1).X0;

            % get the TR for the subject
            [names, TR] = textread([home_foldername 'TRs.txt'],'%s %f','delimiter',';');
            if ( any(strcmp(Subject,names)) )
                DCM.Y.dt = TR(strcmp(Subject,names));
            else
               DCM.Y.dt = 2.315;
            end

            % asign the time series
            for i = 1:DCM.n
                DCM.Y.y(:,i)  = DCM.xY(i).u;
                DCM.Y.name{i} = DCM.xY(i).name;
            end

            % specify error covariance matrices
            DCM.Y.Q = spm_Ce(ones(1,DCM.n)*DCM.v);

            % sampling rate and names of driving inputs
            DCM.U.dt   = SPM.Sess.U(1).dt;
            DCM.U.name = [SPM.Sess.U.name];
            DCM.U.name = DCM.U.name(vector);

            % define a temp array
            temp = [];

            % get the driving input regressors
            for number_of_regressors = 1:length(SPM.Sess.U)
                if ( vector(number_of_regressors) == 1 )
                    temp = [temp SPM.Sess.U(number_of_regressors).u(33:end,1)];
                end
            end

            % asign the driving inputs
            DCM.U.u    = temp;

            % DCM parameters
            DCM.delays = repmat(SPM.xY.RT/2,DCM.n,1);
            DCM.TE     = 0.03;

            % DCM options
            DCM.options.nonlinear  = 0;
            DCM.options.two_state  = 0;
            DCM.options.stochastic = 0;
            DCM.options.centre     = 0;
            DCM.options.endogenous = 0;
            DCM.options.nograph    = 1;


            % specify the A matrix
            if ( include_DLPFC )
                DCM.a = [1 0 1 0 1 0 0; 0 1 1 1 0 0 0; 1 1 1 0 1 0 0; 0 1 0 1 1 1 0; 1 0 1 1 1 0 1; 0 0 0 1 0 1 1; 0 0 0 0 1 1 1];
            else
                DCM.a = [1 1 1 0 0 0; 1 1 0 1 0 0; 1 0 1 1 1 0; 0 1 1 1 0 1; 0 0 1 0 1 1; 0 0 0 1 1 1];
            end

            % code up B matrix
            DCM.b = zeros(size(DCM.a,1),size(DCM.a,2),size(DCM.U.u,2));
            if ( model_number == 1 )
                for int = 2:size(DCM.b,3)
                    if ( include_DLPFC )
                        DCM.b(:,:,int) = [0 0 1 0 1 0 0; 0 0 1 1 0 0 0; 0 1 0 0 1 0 0; 0 0 0 0 1 1 0; 0 0 0 1 0 0 1; 0 0 0 0 0 0 1; 0 0 0 0 0 1 0];
                    else
                        DCM.b(:,:,int) = [0 1 1 0 0 0; 1 0 0 1 0 0; 0 0 0 1 1 0; 0 0 1 0 0 1; 0 0 0 0 0 1; 0 0 0 0 1 0];
                    end
                end
            elseif ( model_number == 2 )
                for int = 2:size(DCM.b,3)
                    if ( include_DLPFC )
                        DCM.b(:,:,int) = [0 0 1 0 1 0 0; 0 0 1 1 0 0 0; 0 1 0 0 1 0 0; 0 0 0 0 1 1 0; 0 0 0 1 0 0 1; 0 0 0 0 0 0 1; 0 0 0 0 0 1 0]';
                    else
                        DCM.b(:,:,int) = [0 1 1 0 0 0; 1 0 0 1 0 0; 0 0 0 1 1 0; 0 0 1 0 0 1; 0 0 0 0 0 1; 0 0 0 0 1 0]';
                    end
                end
            elseif ( model_number == 3 )
                for int = 2:size(DCM.b,3)
                    DCM.b(:,:,int) = DCM.a-eye(size(DCM.a,1));
                end
            elseif ( model_number == 4 )
                for int = 2:size(DCM.b,3)
                    if ( include_DLPFC )
                        DCM.b(:,:,int) = [0 0 1 0 1 0 0; 0 0 0 1 0 0 0; 0 0 0 0 1 0 0; 0 0 0 0 0 1 0; 0 0 0 0 0 0 1; 0 0 0 0 0 0 0; 0 0 0 0 0 0 0];
                    else
                        DCM.b(:,:,int) = [0 0 1 0 0 0; 0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1; 0 0 0 0 0 0; 0 0 0 0 0 0];
                    end
                end
            elseif ( model_number == 5 )
                for int = 2:size(DCM.b,3)
                    if ( include_DLPFC )
                        DCM.b(:,:,int) = [0 0 1 0 1 0 0; 0 0 0 1 0 0 0; 0 0 0 0 1 0 0; 0 0 0 0 0 1 0; 0 0 0 0 0 0 1; 0 0 0 0 0 0 0; 0 0 0 0 0 0 0]';
                    else
                        DCM.b(:,:,int) = [0 0 1 0 0 0; 0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1; 0 0 0 0 0 0; 0 0 0 0 0 0]';
                    end
                end
            elseif ( model_number == 6 )
                for int = 2:size(DCM.b,3)
                    if ( include_DLPFC )
                        DCM.b(:,:,int) = [0 0 1 0 1 0 0; 0 0 0 1 0 0 0; 1 0 0 0 1 0 0; 0 1 0 0 0 1 0; 1 0 1 0 0 0 1; 0 0 0 1 0 0 0; 0 0 0 0 1 0 0];
                    else
                        DCM.b(:,:,int) = [0 0 1 0 0 0; 0 0 0 1 0 0; 1 0 0 0 1 0; 0 1 0 0 0 1; 0 0 1 0 0 0; 0 0 0 1 0 0];
                    end
                end
            end

            % specify the C matrix
            DCM.c = zeros(size(DCM.a,1),size(DCM.b,3));
            if ( include_DLPFC )
                DCM.c(:,1) = [0 0 0 0 0 1 1]';
            else
                DCM.c(:,1) = [0 0 0 0 1 1]';
            end

            % iterate over random restart values
            for restart = restart_counter

                % folder for random starting values
                RSV_folder = 'RandomStartingValues/';
                
                % get the random starting points
                try

                    % get the files
                    temp_RSV = dir([foldername RSV_folder 'spm12_v7487/Model_' name{model_number} DLPFC_name{include_DLPFC+1} '/VB*']);

                    % load prespecified random starting values (same for all subjects)
                    temp_file = load([foldername RSV_folder 'spm12_v7487/Model_' name{model_number} DLPFC_name{include_DLPFC+1} '/' temp_RSV(restart).name]);
                    disp(' ')
                    disp('Loaded pre-specified random starting points...')
                    disp(['Taken from file: Model_' name{model_number} DLPFC_name{include_DLPFC+1} '/' temp_RSV(restart).name])
                    disp(' ')
                catch

                    % display error
                    disp(' ')
                    disp('ERROR')
                    disp(' ')
                end

                % convert the random starting values to correct format
                init.P = temp_file.P;

                % set the random starting values
                DCM.options.P = init.P;
                
                % save the initialization values
                DCM.init = init;

                % get the results folder
                DCM_resuts_foldername = fullfile(foldername,Subject,'FirstLevel_DCM_RandRestart','spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth');

                % create the folder
                if ( ~exist(DCM_resuts_foldername,'dir') )
                    mkdir(DCM_resuts_foldername)
                end

                % set the file name
                restart_name = ['00' num2str(restart)];
                restart_name = restart_name(end-2:end);
                DCM_ToEstimate_RandRest = fullfile(DCM_resuts_foldername,['DCM_NESDA_' name{model_number} DLPFC_name{include_DLPFC+1} '_local_' restart_name '.mat']);


                % display
                disp('')
                disp(['-- Subject ' Subject ' (' basis_folder{basis_set+1} '): Estimation of Model ' name{model_number}(7:end) DLPFC_name{include_DLPFC+1} '_local_' restart_name ' -----------------'])
                disp('')

                % run model inversion of DCMs
                DCM = spm_dcm_estimate(DCM);
                
                % fields that are not important
                field_names = {'xY','n','v','Vp','Pp','H1','H2','K1','K2','T','Ce','AIC','BIC'};

                % delete those fields
                for int = 1:length(field_names)
                    if ( isfield(DCM,field_names{int}) )
                        DCM = rmfield(DCM,field_names{int});
                    end
                end
                
                % save the estimated DCM
                save(DCM_ToEstimate_RandRest,'DCM');

            end
        end
    end
end

% delay the time (so that job does not finish instantly)
delta_time = toc;
if ( delta_time < 120 )
    pause(240)
end

end