function transform_SVM_weights_to_patterns_MainExperiment(FilenameInfo,model,experiment,SPM_prep,include_DLPFC,basis_set)
% Transforms the feature weights from a linear SVM into interpretable 
% pattern according to Haufe et al (2014), Neuroimage. This is done by 
% using the weights in source space (primal problem) from  fitcsvm and 
% implementing the procedure outlined in the paper mentioned above.
% Importantly, the present function is a re-implementation of the function 
% "transres_SVM_pattern()" from "The Decoding Toolbox"
% 
% Martin N Hebart*, Kai Goergen* and John-Dylan Haynes (2015). The Decoding 
% Toolbox (TDT): A versatile software package for multivariate analyses of 
% functional imaging data. Front. Neuroinform. 8:88.
% doi: 10.3389/fninf.2014.00088.
% 
% Input:
%   FilenameInfo        -- important definitions for the experiment
%   model               -- model to be analyzed
%   experiment          -- (1) FACES dataset, (2) Tower of London dataset
%   SPM_prep            -- (0) NESDA pipeline (1) SPM preprocessing
%   include_DLPFC       -- (0) not include DLPFC, (1) include DLPFC
%   basis_set           -- basis set: (0) only HRF, (1) temporal derivative, (2) informed basis set
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% load the paths if necessary
if ( isempty(FilenameInfo) )
    
    % get the path
    m_path = mfilename('fullpath');
    m_path = m_path(1:find(m_path=='/',1,'last'));

    try
        load(fullfile(m_path,'ConfigFile.mat'))
    catch err
        disp('Need to specify FilenameInfo containing paths!')
        rethrow(err)
    end
end


% default settings
version = [];

% set the experiment
exp_folder = {'FACES/','ToL/'};

% set folders
SPM_folder          = {'','SPM_preprocessing/'};
DLPFC_name          = {'','_DLPFC'};
basis_folder        = {'HRF','TD','IBS',''};


% set the SPM path and the path to the experiment
foldername = [FilenameInfo.DataPath exp_folder{experiment}];


% define the name of the different models
name = {'BMA','model_FF_INTER','model_FB_INTER','model_RC_INTER','model_FF_noINTER',...
    'model_FB_noINTER','model_RC_noINTER','model_noMOD'};

% specify the model folder name
if ( isempty(model) )
    model_folder  = 'Bayesian_model_selection_RandRest';
elseif ( model == 0 )
    model_number  = 0; 
    model_folder  = 'Bayesian_model_averaging_RandRest_single';
else
    model_number  = model;
    model_folder  = 'IndividualModel_RandRest';
end



% print progress
fprintf('Transforming feature weights to patterns (True labels)\n')

% load the actual classification results
if ( ~isempty(model) )
    temp = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local.mat']));
else
    temp = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbeddingBMS' DLPFC_name{include_DLPFC+1} '_local.mat']));
end


% get the results
results         = temp.results;


% iterate over groups
for group = 1:length(results)
    
    % get the data
    data = results(group).class.features;
    
    % create the binary training matrix
    train = ones(size(results(group).class.weights,1),size(results(group).class.weights,1));
    train = train - diag(diag(train));
    
    for nr_folds = 1:size(results(group).class.weights,1)
        
        % get the current weights and true labels
        weights   = results(group).class.weights(nr_folds,:)';
        currlabel = results(group).class.true_labels(nr_folds);

        % initialize empty pattern
        pattern = zeros(size(weights));
        
        % get the predicted label
        ulabel  = sort(results(group).class.predicted_labels(nr_folds));
        n_label = length(ulabel);

        % iterate over subjects
        ct = 1;
%         for i_label = 1:n_label
%             for j_label = i_label+1:n_label
%                 ct = ct+1;
%                 label_ind = currlabel == ulabel(i_label) | currlabel == ulabel(j_label);
%                 data_train = data(train(:, nr_folds) > 0 & label_ind, :);
                
                data_train = data(train(:, nr_folds) > 0, :);
                
                [n_samples, n_dim] = size(data_train);
                curr_weights = weights(:,ct);

                % different computation modes (depending on dimensionality of weights)
                if ( n_dim^2 < 10^7 )
                    pattern(:,ct) = cov(data_train)*curr_weights / cov(curr_weights'*data_train'); % like cov(X)*W * inv(W'*X')
                else
                    warningv('TRANSRES_SVM_PATTERN:pattern_calculation_slow','Pattern is very large, so its estimation will be very slow (up to minutes)!')
                    scale_param = cov(curr_weights'*data_train');
                    pattern_unscaled = zeros(n_dim,1);
                    fprintf(repmat(' ',1,20))
                    backstr = repmat('\b',1,20);
                    for i = 1:n_dim % now calculate columnwise
                        if i == 1 || ~mod(i,round(n_dim/50)) || i == n_dim
                            fprintf([backstr '%03.0f percent finished'],100*i/n_dim)
                        end
                        data_train(:,i) = data_train(:,i) - mean(data_train(:,i)); % remove mean columnwise
                        data_cov = (data_train(:,i)'*data_train)/(n_samples-1);
                        pattern_unscaled(i,1) = data_cov * curr_weights;
                    end
                    fprintf('\ndone.\n')
                    pattern(:,ct) = pattern_unscaled / scale_param; % like cov(X)*W * inv(W'*X')
                end
%             end
%         end
        
        % asign the estimated patterns
        results(group).class.patterns(nr_folds,:) = pattern;
        
    end
end

% save Generative embedding results with the patterns
if ( ~isempty(model) )
    save(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local.mat']),'results');
else
    save(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbeddingBMS' DLPFC_name{include_DLPFC+1} '_local.mat']),'results');
end


% print progress
fprintf('Transforming feature weights to patterns (Permuted labels)\n')

% load the permutation results
if ( ~isempty(model) )
    temp = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding_Permutation' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local.mat']));
else
    temp = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding_PermutationBMS' DLPFC_name{include_DLPFC+1} '_local.mat']));
end


% get the results
results         = temp.results;
name_connection = temp.name_connection;


% iterate over groups
for group = 1:size(results,1)
    for Nr_perm = 1:size(results,2)
    
        % get the data
        data = results(group,Nr_perm).class.features;

        % create the binary training matrix
        train = ones(size(results(group,Nr_perm).class.weights,1),size(results(group,Nr_perm).class.weights,1));
        train = train - diag(diag(train));

        for nr_folds = 1:size(results(group,Nr_perm).class.weights,1)

            % get the current weights and true labels
            weights   = results(group,Nr_perm).class.weights(nr_folds,:)';
            currlabel = results(group,Nr_perm).class.true_labels(nr_folds);

            % initialize empty pattern
            pattern = zeros(size(weights));

            % get the predicted label
            ulabel  = sort(results(group,Nr_perm).class.predicted_labels(nr_folds));
            n_label = length(ulabel);

            % iterate over subjects
            ct = 1;
    %         for i_label = 1:n_label
    %             for j_label = i_label+1:n_label
    %                 ct = ct+1;
    %                 label_ind = currlabel == ulabel(i_label) | currlabel == ulabel(j_label);
    %                 data_train = data(train(:, nr_folds) > 0 & label_ind, :);

                    data_train = data(train(:, nr_folds) > 0, :);

                    [n_samples, n_dim] = size(data_train);
                    curr_weights = weights(:,ct);

                    % different computation modes (depending on dimensionality of weights)
                    if ( n_dim^2 < 10^7 )
                        pattern(:,ct) = cov(data_train)*curr_weights / cov(curr_weights'*data_train'); % like cov(X)*W * inv(W'*X')
                    else
                        warningv('TRANSRES_SVM_PATTERN:pattern_calculation_slow','Pattern is very large, so its estimation will be very slow (up to minutes)!')
                        scale_param = cov(curr_weights'*data_train');
                        pattern_unscaled = zeros(n_dim,1);
                        fprintf(repmat(' ',1,20))
                        backstr = repmat('\b',1,20);
                        for i = 1:n_dim % now calculate columnwise
                            if i == 1 || ~mod(i,round(n_dim/50)) || i == n_dim
                                fprintf([backstr '%03.0f percent finished'],100*i/n_dim)
                            end
                            data_train(:,i) = data_train(:,i) - mean(data_train(:,i)); % remove mean columnwise
                            data_cov = (data_train(:,i)'*data_train)/(n_samples-1);
                            pattern_unscaled(i,1) = data_cov * curr_weights;
                        end
                        fprintf('\ndone.\n')
                        pattern(:,ct) = pattern_unscaled / scale_param; % like cov(X)*W * inv(W'*X')
                    end
    %             end
    %         end

            % asign the estimated patterns
            results(group,Nr_perm).class.patterns(nr_folds,:) = pattern;
            
        end
    end
end

% save generative embedding results with the patterns
if ( ~isempty(model) )
    save(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding_Permutation' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local.mat']),'results','name_connection');
else
    save(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding_PermutationBMS' DLPFC_name{include_DLPFC+1} '_local.mat']),'results','name_connection');
end

end