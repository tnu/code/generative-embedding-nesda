function permutation_test_SVM_MainExperiment(FilenameInfo,model,experiment,classifier,NrPerm_analyze,regress_covariates,version,group_comp_all)
% Performs a permutation test to identify features that are important for
% classifcation between the different patient subgroups - namely fast 
% remission, gradualy improving, and chronic. The script uses support 
% vector machines (SVM) in accordance with the classical approach
% used by Kay Brodersen.
%
% Input:
%   FilenameInfo        -- important definitions for the experiment
%   model               -- model to be analyzed
%   experiment          -- (1) FACES dataset, (2) Tower of London dataset
%   classifier          -- classifier: (1) GPC, (2) SVM, and (3) SVMCFIT
%   NrPerm_analyze      -- # VB starting value
%   regress_covariate	-- regress out covariates (default: 0)
%   version             -- empty
%   group_comp_all      -- which binary classification should be looked at
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2016
% Copyright 2016 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% set the variables
SPM_prep            = 1;
classifier_mode     = 3;
include_DLPFC       = 0;
basis_set           = 2;


% if FilenameInfo not specified, try to load
if ( isempty(FilenameInfo) )
    
    % get the path
    m_path = mfilename('fullpath');
    m_path = m_path(1:find(m_path=='/',1,'last'));
    
    try
        load(fullfile(m_path,'ConfigFile.mat'))
    catch err
        disp('Need to specify FilenameInfo containing paths!')
        rethrow(err)
    end
end


% check version
if ( ~isempty(version) )
    version = [];
end

% set the experiment
exp_folder = {'FACES/','ToL/'};

% set folders
SPM_folder          = {'','SPM_preprocessing/'};
DLPFC_name          = {'','_DLPFC'};
settings_folder    	= {'woDLPFC','wDLPFC'};
basis_folder        = {'HRF','TD','IBS',''};
regress_name        = {'','_orth'};

% define the SPM version
classifier_name  = {'GPC','SVM','FITCSVM'};

% add path
addpath(genpath(fullfile(FilenameInfo.MATLAB_path,'PBAR')))

% add path for classifiers
if ( classifier == 1 )
    addpath(genpath(fullfile(FilenameInfo.MATLAB_path,'PRoNTo')))
elseif ( classifier == 2 )
    addpath(genpath(fullfile(FilenameInfo.MATLAB_path,'liblinear-2.11')))
end

% set the path of the experiment
exp_foldername	 = FilenameInfo.Exp_path;

% set the SPM path and the path to the experiment
foldername = [FilenameInfo.DataPath exp_folder{experiment}];


% define the name of the different models
name = {'BMA','model_FF_INTER','model_FB_INTER','model_RC_INTER','model_FF_noINTER',...
    'model_FB_noINTER','model_RC_noINTER','model_noMOD'};

% define the different groups
group_names = {'CHR_REM','CHR_IMP','IMP_REM'};
group_comb  = {[3 1],[3 2],[2 1]};

% specify the model folder name
if ( isempty(model) )
    model_folder  = 'Bayesian_model_selection_RandRest';
elseif ( model == 0 )
    model_number  = 0; 
    model_folder  = 'Bayesian_model_averaging_RandRest_single';
else
    model_number  = model;
    model_folder  = 'IndividualModel_RandRest';
end

% get the classes of the subjects
temp                = load([exp_foldername 'LCGA_classes/LCGA_classes.mat']);
LCGA_classes        = temp.LCGA_classes;
AllSubjects_Schmaal = xlsread([exp_foldername 'information/AllSubjects_Faces.xlsx']);

% results folder
results_folder = 'DCM_GroupAnalysis';

% load the posterior parameter estimates
if ( ~isempty(model) && model_number ~= 0 )
    temp = load(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['PosteriorParameterEstimates_' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local.mat']));
elseif ( isempty(model) )
    file = dir(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,[settings_folder{include_DLPFC+1} '_local'],'PosteriorParameterEstimates*'));
    temp    = load(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,[settings_folder{include_DLPFC+1} '_local'],file(1).name));
    pxp_max = temp.results.pxp_max;
else
    temp = load(fullfile(foldername,[results_folder num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,[settings_folder{include_DLPFC+1} '_local'],'BMS.mat'));
    
    % bring the results in the same format
    for s = 1:length(temp.BMS.DCM.ffx.bma.mEps)
        
        % endogenous connections
        for int = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.A,1)
            for int2 = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.A,2)
                temp.results.A_Matrix_AllSubjects{int,int2}(s) = temp.BMS.DCM.ffx.bma.mEps{s}.A(int,int2);
            end
        end
        
        % modulatory influences
        for int = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.B,1)
            for int2 = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.B,2)
                for int3 = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.B,3)
                    temp.results.B_Matrix_AllSubjects{int,int2,int3}(s) = temp.BMS.DCM.ffx.bma.mEps{s}.B(int,int2,int3);
                end
            end
        end
        
        % driving inputs
        for int = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.C,1)
            for int2 = 1:size(temp.BMS.DCM.ffx.bma.mEps{1}.C,2)
                temp.results.C_Matrix_AllSubjects{int,int2}(s) = temp.BMS.DCM.ffx.bma.mEps{s}.C(int,int2);
            end
        end
    end
    
    % asign the subjects included
    temp.results.AllSubjects = temp.BMS.DCM.ffx.AllSubjects;
    
end

% load the functional connectivity measures (if available)
if ( exist(fullfile(foldername,'FC_GroupAnalysis',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',['FC_measures' DLPFC_name{include_DLPFC+1} '_local.mat']),'file') )
    temp2 = load(fullfile(foldername,'FC_GroupAnalysis',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',['FC_measures' DLPFC_name{include_DLPFC+1} '_local.mat']));
end

% load the contrast image measures (if available)
if ( exist(fullfile(foldername, 'CI_GroupAnalysis/',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',['CI_measures' DLPFC_name{include_DLPFC+1} '_local.mat']),'file') )
    temp3  = load(fullfile(foldername, 'CI_GroupAnalysis/',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',['CI_measures' DLPFC_name{include_DLPFC+1} '_local.mat']));
end


% asign the data
Amatrix_allSubjects     = temp.results.A_Matrix_AllSubjects;
Bmatrix_allSubjects     = temp.results.B_Matrix_AllSubjects;
Cmatrix_allSubjects     = temp.results.C_Matrix_AllSubjects;
FC_allSubjects          = temp2.FC_measures.cross_corr_AllSubjects;
CI_allSubjects          = [temp3.data_CI{1}, temp3.data_CI{2}, temp3.data_CI{3}, temp3.data_CI{4}, temp3.data_CI{5}, temp3.data_CI{6}];


% define a labels array
labels = zeros(1,length(temp.results.AllSubjects));

% dummy number of subjects
nr_subject = [];
found_subjects = [];

for int = 1:length(temp.results.AllSubjects)

    % find the index of the subject in the LCGA classes
    index_LCGA = find(LCGA_classes(:,1) == str2double(temp.results.AllSubjects{int}),1);

    if ( ~isempty(index_LCGA) )
        
        % get subjects that were also included in Schmal et al.
        index = find(AllSubjects_Schmaal == str2double(temp.results.AllSubjects{int}),1);
        
        if ( ~isempty(index) )
            if ( ~isempty(index_LCGA) && isfinite(Amatrix_allSubjects{1,1}(int)) )
                nr_subject(end+1)     = LCGA_classes(index_LCGA,1);
                found_subjects(end+1) = str2double(temp.results.AllSubjects{int});
            end

            % if the subject exists, assign the class label
            if ( isfinite(Amatrix_allSubjects{1,1}(int)) )
                labels(int) = LCGA_classes(index_LCGA,3);
            end
        end
    end
end

% find the index of the included subjects
initial_subjects = str2double(temp2.FC_measures.AllSubjects);
index_sub = NaN(length(found_subjects),1);
for int = 1:length(found_subjects)
    index_sub(int) = find(initial_subjects==found_subjects(int));
end

% mark included subjects
vector(index_sub,1) = 1;

% convert to logical
vector = vector == 1;


% restrict the FC and CI cell arrays to included subjects (only for BMA)
if ( model == 0 )
    
    % adjust A parameters
    for int = 1:size(FC_allSubjects,1)
        for int2 = 1:size(FC_allSubjects,2)
            FC_allSubjects{int,int2} = FC_allSubjects{int,int2}(vector);
        end
    end
    
    % adjust contrast images
    CI_allSubjects = CI_allSubjects(vector,:);
    
end


% display how many subjects were found
fprintf('\n')
disp(['Found: ' num2str(length(nr_subject)) ' out of ' num2str(length(AllSubjects_Schmaal)) ' subjects - Running: ' classifier_name{classifier} ' / ' basis_folder{basis_set+1}])

% display subject name of missing subject
if ( length(nr_subject) ~= length(AllSubjects_Schmaal) )
    missing_subjects = setdiff(AllSubjects_Schmaal,found_subjects);
    fprintf((~isempty(missing_subjects))+1,'Subjects missing:');
    for int = 1:length(missing_subjects)
        fprintf((~isempty(missing_subjects))+1,' %d',missing_subjects(int));
    end
    fprintf('\n')
end


% default settings
if ( isempty(group_comp_all) )
    group_comp_all = 1:3;
end


% load the permutation indices
if ( ~isempty(model) )
    tempPermIndices = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding_PermutationIndices' name{model_number+1} DLPFC_name{include_DLPFC+1} '_local.mat']));
    PermIndices = tempPermIndices.PermIndices;    
else
    tempPermIndices = load(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding_PermutationIndicesBMS' DLPFC_name{include_DLPFC+1} '_local.mat']));
    PermIndices = tempPermIndices.PermIndices; 
end


% specify the true labels for a binary comparison of groups
for group_comp = group_comp_all
    
    % clear the data
    clear data
    
    % define all labels
    all_labels = 1:3;
    not_included = setdiff(all_labels,group_comb{group_comp});
    
    % define temporary label
    labels_temp = labels;
    labels_temp(labels_temp==not_included) = 0;
    vector_2 = labels_temp==group_comb{group_comp}(2);
    vector_1 = labels_temp==group_comb{group_comp}(1);
    labels_temp(vector_2) = 2;
    labels_temp(vector_1) = 1;
    
    % define the specifiy labels
    labels_group = labels_temp(labels_temp~=0);
    [labels_group, order] = sort(labels_group);
    labels_group = labels_group';

    % define the subjects that should be included
    subjects_analyze = find(labels_temp~=0);
    subjects_analyze = subjects_analyze(order);
    
    % get the subject names
    subjects_group_temp = temp.results.AllSubjects(subjects_analyze);
    subjects_group      = NaN(size(subjects_group_temp));
    for int = 1:length(subjects_group_temp)
        subjects_group(int) = str2double(subjects_group_temp{int});
    end

    % display the setting
    disp(['Group selection: ' group_names{group_comp} ' - Group size: ' num2str(sum(labels_temp==1)) ' vs. ' num2str(sum(labels_temp==2))])

    % define the counter
    subject_counter = 0;

    % get the parameters
    for s = subjects_analyze

        % increase the subject counter
        subject_counter = subject_counter + 1;

        % initialize the temporary arrays
        a_temp = NaN(size(Amatrix_allSubjects));
        b_temp = NaN(size(Bmatrix_allSubjects));
        c_temp = NaN(size(Cmatrix_allSubjects));

        % get endogenous parameters
        for int = 1:size(Amatrix_allSubjects,1)
            for int2 = 1:size(Amatrix_allSubjects,2)
                a_temp(int,int2) = Amatrix_allSubjects{int,int2}(s);
            end
        end

        % get modulatory parameters
        for int = 1:size(Bmatrix_allSubjects,1)
            for int2 = 1:size(Bmatrix_allSubjects,2)
                for int3 = 1:size(Bmatrix_allSubjects,3)
                    b_temp(int,int2,int3) = Bmatrix_allSubjects{int,int2,int3}(s);
                end
            end
        end

        % get driving parameters
        for int = 1:size(Cmatrix_allSubjects,1)
            for int2 = 1:size(Cmatrix_allSubjects,2)
                c_temp(int,int2) = Cmatrix_allSubjects{int,int2}(s);
            end
        end
        
        % get functional connectivity
        for int = 1:size(FC_allSubjects,1)
            for int2 = 1:size(FC_allSubjects,2)
                fc_temp(int,int2) = FC_allSubjects{int,int2}(s);
            end
        end
        
        % make the functional connectivity matrix upper triangular
        fc_temp = triu(fc_temp);
        
        % get all the present connections
        if ( isempty(model) )
            temp_A = temp.results.mean_A_Matrix_AllSubjects~=0;
            temp_B = temp.results.mean_B_Matrix_AllSubjects~=0;
            temp_C = temp.results.mean_C_Matrix_AllSubjects~=0;
        else
            if ( (model ~= 0) )
                temp_A = temp.results.mean_A_Matrix_AllSubjects~=0;
                temp_B = temp.results.mean_B_Matrix_AllSubjects~=0;
                temp_C = temp.results.mean_C_Matrix_AllSubjects~=0;
            else
                temp_A = temp.BMS.DCM.ffx.bma.mEps{1}.A~=0;
                temp_B = zeros(size(temp.BMS.DCM.ffx.bma.mEps{1}.B));
                matrix = temp_A-eye(size(temp_A));
                for int = 2:size(temp_B,3)
                    temp_B(:,:,int) = matrix;
                end
                temp_C = temp.BMS.DCM.ffx.bma.mEps{1}.C~=0;
            end
        end
        indicator_vector = [temp_A(:); temp_B(:); temp_C(:)];
        
        % collect all the non-zero parameters
        temp_vector             = [a_temp(:); b_temp(:); c_temp(:)]';
        data(subject_counter,:) = temp_vector(indicator_vector~=0);
        
        % collect all the non-zero FC parameters
        data_FC(subject_counter,:) = fc_temp(fc_temp~=0);
        
        
        % get the names of the connections
        counter     = 0;
        name_from   = {'lAmy','rAmy','lFFA','rFFA','lOFA','rOFA'};
        name_to     = {'lAmy','rAmy','lFFA','rFFA','lOFA','rOFA'};
        name_emp    = {'','happy','angry','fear','sad'};
        
        % get endogenous connections
        for int = 1:size(temp_A,2)
            for int2 = 1:size(temp_A,1)
                if ( temp_A(int2,int) == 1 )
                    counter = counter + 1;
                    name_connection{counter} = [name_from{int} ' -> ' name_to{int2}];
                end
            end
        end

        % get modulatory inputs
        for int3 = 1:size(temp_B,3)
            for int = 1:size(temp_B,2)
                for int2 = 1:size(temp_B,1)
                    if ( temp_B(int2,int,int3) == 1 )
                        counter = counter + 1;
                        name_connection{counter} = [name_from{int} ' -> ' name_to{int2} ' (' name_emp{int3} ')'];
                    end
                end
            end
        end

        % get dirving inputs
        for int = 1:size(temp_C,2)
            for int2 = 1:size(temp_C,1)
                if ( temp_C(int2,int) == 1 )
                    counter = counter + 1;
                    name_connection{counter} = ['to ' name_to{int2}];
                end
            end
        end
    end
    
    % get the mean contrast image values
    data_CI = CI_allSubjects(subjects_analyze,:);
    
    
    % test whether there are any NaN's in the data
    vector_NaN = any(~isfinite(data));
    if ( any(vector_NaN) )
        disp('================================')
        disp('NaN in data - Results not valid!')
        disp('================================')
    end
    
    
    % check if results directory exists
    if ( ~exist(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder),'dir') )
        mkdir(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder))
    end
    
    % regress out variables
    if ( regress_covariates )
        
        % display processing step
        disp('Regressing confounds from data')
        
        % load the covariates
        info          = load([exp_foldername 'LCGA_classes/Subjects_info.mat']);
        Subjects_info = info.Subjects_info;
        
        % empty index array
        index = [];
        
        % get all the indices
        for int = 1:length(subjects_group)
            index(int) = find(subjects_group(int) == Subjects_info(:,1)); 
        end
        
        % get the confounds
        class_opt.confounds = Subjects_info(index,3);
        
        % clear old data
        clear data_old
        
    else
        
        % empty options
        class_opt = [];
        
    end
    
    % for command output
    reverseStr = '';
    
    % number of permutations
    maxNrPerm = 1000;
    if ( isempty(NrPerm_analyze) )
        NrPerm_analyze_temp = 1:maxNrPerm;
    else
        NrPerm_analyze_temp = NrPerm_analyze;
    end
    
    
    % iterate over permutations
    for NrPerm = NrPerm_analyze_temp
        
        % output progress
        msg = sprintf('Processing permutation: %d/%d', NrPerm, maxNrPerm);
        fprintf([reverseStr, msg]);
        reverseStr = repmat(sprintf('\b'), 1, length(msg));
        
        % shuffle the labels
        labels_group = labels_group(PermIndices{group_comp}(NrPerm,:));
        
    
        % classification (VBL)
        if ( classifier == 2 )
            results(group_comp,NrPerm).class       = classify_simple_liblinear(labels_group,data,classifier_mode,0);
        elseif ( classifier == 3 )
            results(group_comp,NrPerm).class       = classify_simple_fitcsvm(labels_group, data, class_opt, 0);
        end


        % classification (FC)
        if ( classifier == 2 )
            results(group_comp,NrPerm).class_FC      = classify_simple_liblinear(labels_group,data_FC,classifier_mode,0);
        elseif ( classifier == 3 )
            results(group_comp,NrPerm).class_FC      = classify_simple_fitcsvm(labels_group,data_FC,class_opt,0);
        end


        % classification (CI)
        if ( classifier == 2 )
            results(group_comp,NrPerm).class_CI      = classify_simple_liblinear(labels_group,data_CI,classifier_mode,0);
        elseif ( classifier == 3 )
            results(group_comp,NrPerm).class_CI      = classify_simple_fitcsvm(labels_group,data_CI,class_opt,0);
        end

        % check if results directory exists
        if ( ~exist(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder),'dir') )
            mkdir(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder))
        end

        % store the individual results of the generative embedding
        if ( ~isempty(NrPerm_analyze) )
            if ( ~isempty(group_comp_all) )
                if ( ~isempty(model) )
                    save(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding_Permutation' regress_name{regress_covariates+1} name{model_number+1} DLPFC_name{include_DLPFC+1} '_local' num2str(group_comp) '_' num2str(NrPerm) '.mat']),'results','name_connection')
                else
                    save(fullfile(foldername,['DCM_GenerativeEmbedding' num2str(version)],'spm12_v7487',SPM_folder{SPM_prep+1},basis_folder{basis_set+1},'NeuroSynth','Schmaal',model_folder,['GenerativeEmbedding_PermutationBMS' regress_name{regress_covariates+1} DLPFC_name{include_DLPFC+1} '_local' num2str(group_comp) '_' num2str(NrPerm) '.mat']),'results','name_connection')
                end
            end
        end
    end
    
    % new line
    fprintf('\n')
    
end

end
