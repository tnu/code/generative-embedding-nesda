function first_level_MainExperiment(subject,experiment,SPM_prep,basis_set)
% Specify the first level General Linear Model (GLM) for the FACES dataset 
% or the Tower of Londong (ToL) dataset of the NESDA study (Netherlands 
% Study on Depression and Social Anxiety). 
% 
% This function reads a Matlab Batch, logfile and TR setting to specify the 
% GLM for first level analysis of BOLD activation correlated with the FACES
% task or the ToL task. The GLM is then estimated and the respective
% contrasts are specified.
% 
% Input:
%   subject      	-- subject to be analyzed
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   SPM_prep        -- preprocessing: (0) NESDA pipeline, (1) SPM routines
%   basis_set       -- basis set: (0) only HRF, (1) temporal derivative, (2) informed basis set
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2016
% Copyright 2016 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
exp_folders = {'FACES/','ToL/'};

% set the SPM folder
SPM_folder = {'','SPM_preprocessing/'};


% set the SPM path and the path to the experiment
addpath(FilenameInfo.SPM_path)
foldername      = fullfile(FilenameInfo.DataPath,exp_folders{experiment});
home_foldername = FilenameInfo.Exp_path;


% define some variables
spm('Defaults','fMRI');
spm_jobman('initcfg');

% specify some variables
HighPassFilter	= 128;
RunBatch        = 1;

% get the subject list
Subject_List = [dir([foldername '1*']); dir([foldername '2*']); dir([foldername '3*'])];

% specify the subject
if ( isempty(subject) )
    subject = 1:length(Subject_List);
end

if ( isempty(basis_set) )
    basis_set_analyze = [0 1 2];
else
    basis_set_analyze = basis_set;
end


% iterate over all subjects
for basis_set = basis_set_analyze
    
    % load the example Matlab Batch
    load([home_foldername 'Batch_FirstLevelModelSpecifications.mat'])
    
    for s = subject

        % get the subject name
        Subject = Subject_List(s).name;

        % display what is done at the moment
        disp([Subject ' - Model specification'])


        % modify the working directory where the SPM.mat should be saved
        matlabbatch{1}.spm.stats.fmri_spec.dir = [];

        % get the correct folder name
        if ( basis_set == 0 )
            folder = [foldername Subject '/FirstLevel/' SPM_folder{SPM_prep+1} 'HRF/'];
        elseif ( basis_set == 1 )
            folder = [foldername Subject '/FirstLevel/' SPM_folder{SPM_prep+1} 'TD/'];
        elseif ( basis_set == 2 ) 
            folder = [foldername Subject '/FirstLevel/' SPM_folder{SPM_prep+1} 'IBS/'];
        end

        % display results folders
        disp(folder)

        % create the FirstLevel folder if it does not exist
        if ( ~exist(folder,'dir') )
            mkdir(folder)
        end

        % assign the new filename
        matlabbatch{1}.spm.stats.fmri_spec.dir = {folder};

        % modify the unit of the multiple conditions included in the GLM
        matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';

        % get the TR for the subject
        [names, TR] = textread([home_foldername 'TRs.txt'],'%s %f','delimiter',';');
        if ( any(strcmp(Subject,names)) )
            matlabbatch{1}.spm.stats.fmri_spec.timing.RT = TR(strcmp(Subject,names));
        else
           matlabbatch{1}.spm.stats.fmri_spec.timing.RT = 2.315;
        end


        % delete the filenames of the scans and load the new filenames
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).scans = [];

        % define the filename of the EPI data
        if ( SPM_prep == 0 )
            processed_filename = [Subject '_denoised_tempfilt_mni.nii'];
        else
            processed_filename = ['sw' Subject '_filtered_func_data.nii'];
        end

        % assign the new file name to the matlabbatch
        if ( ~exist([foldername Subject '/funk/' SPM_folder{SPM_prep+1} processed_filename],'file') )

            % display
            disp([Subject ': No data found...'])

            % exit the function
            exit

        else

            % get the correct filenames
            files = spm_select('expand', [foldername Subject '/funk/' SPM_folder{SPM_prep+1} processed_filename]);

            % number of EPI volumes
            EPI_volumes = size(files,1);

            % output the number of scans
            disp(['Number of EPI volumes: ' num2str(EPI_volumes)])

            % asign the filenames to matlabbatch
            for int2 = 1:size(files,1)

                % assign the new filename        
                matlabbatch{1}.spm.stats.fmri_spec.sess(1).scans = [matlabbatch{1}.spm.stats.fmri_spec.sess(1).scans; {[files(int2,1:end-3) num2str(int2)]}];

            end
        end


        %% modify the filename where the onsets of the conditions

        % get the correct filenames
        files = dir([foldername Subject '/logfiles/*.mat']);

        % delete the filenames of the scans
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi = [];

        % create the new filename
        new_filename = [foldername Subject '/logfiles/' files(1).name];

        % assign the new filename        
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi = {new_filename};


        %% modify the filename for the motion parameters

        % delete the filenames of the scans
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi_reg = [];

        % get the correct filenames
        files = dir([foldername Subject '/funk/' SPM_folder{SPM_prep+1} '*.txt']);

        % create the new filename
        new_filename = [foldername Subject '/funk/' SPM_folder{SPM_prep+1} files(1).name];

        % load the motion parameters
        data_rp = textread(new_filename);

        % check if dimensions agree
        if ( EPI_volumes ~= size(data_rp,1))

            % display error message
            disp([Subject ': Dimensions do not agree...'])

            % trim the realignment parameters and save again
            if ( size(data_rp,1) > EPI_volumes )

                % trim the parameters
                data_rp = data_rp(1:EPI_volumes,:);

                % delete the new file if it already exists
                if ( exist([new_filename(1:end-4) '_trimmed.txt'],'file') )
                    delete([new_filename(1:end-4) '_trimmed.txt'])
                end

                % open the text file
                fileID = fopen([new_filename(1:end-4) '_trimmed.txt'],'w');

                % write the motion parameters into the txt-file
                for int = 1:size(data_rp,1)
                    fprintf(fileID,[num2str(data_rp(int,1)) ' ' num2str(data_rp(int,2)) ' ' num2str(data_rp(int,3)) ' ' num2str(data_rp(int,4)) ' ' num2str(data_rp(int,5)) ' ' num2str(data_rp(int,6)) '\n']);
                end

                % close the file
                fclose(fileID);

                % adapt the filename
                new_filename = [new_filename(1:end-4) '_trimmed.txt'];

            else

                % trim the number of scans
                matlabbatch{1}.spm.stats.fmri_spec.sess(1).scans = matlabbatch{1}.spm.stats.fmri_spec.sess(1).scans(1:size(data_rp,1));

            end
        end


        % assign the new filename        
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi_reg = {new_filename};


        % delete the data
        clear data_rp


        %% set the derivatives to be included in the GLM

        % enable temporal and dispersion derivative
        if ( basis_set == 0 )
            matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0];
        elseif ( basis_set == 1 )
            matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [1 0];
        elseif ( basis_set == 2 )
            matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [1 1];
        end


        %% set the threshold for the high-pass filter

        % use the value set at the beginning of the programm
        matlabbatch{1}.spm.stats.fmri_spec.sess(1).hpf = HighPassFilter;


        %% estimate the first level GLM

        % specify the estimation part
        matlabbatch{2}.spm.stats.fmri_est.spmmat = cellstr(fullfile(folder,'SPM.mat'));


        %% do the inference

        % specify contrasts
        matlabbatch{3}.spm.stats.con.spmmat = cellstr(fullfile(folder,'SPM.mat'));
        matlabbatch{3}.spm.stats.con.consess{1}.tcon.name = 'all faces > baseline';
        matlabbatch{3}.spm.stats.con.consess{1}.tcon.weights = [1 1 1 1 1 -5];
        matlabbatch{3}.spm.stats.con.consess{2}.tcon.name = 'happy > baseline';
        matlabbatch{3}.spm.stats.con.consess{2}.tcon.weights = [1 0 0 0 0 -1];
        matlabbatch{3}.spm.stats.con.consess{3}.tcon.name = 'angry > baseline';
        matlabbatch{3}.spm.stats.con.consess{3}.tcon.weights = [0 1 0 0 0 -1];
        matlabbatch{3}.spm.stats.con.consess{4}.tcon.name = 'fear > baseline';
        matlabbatch{3}.spm.stats.con.consess{4}.tcon.weights = [0 0 1 0 0 -1];
        matlabbatch{3}.spm.stats.con.consess{5}.tcon.name = 'sad > baseline';
        matlabbatch{3}.spm.stats.con.consess{5}.tcon.weights = [0 0 0 1 0 -1];
        matlabbatch{3}.spm.stats.con.consess{6}.tcon.name = 'neutral > baseline';
        matlabbatch{3}.spm.stats.con.consess{6}.tcon.weights = [0 0 0 0 1 -1];


        % add zeros for basis sets
        if ( sum(matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs) == 1 )
            for Nr_contrast = 1:length(matlabbatch{3}.spm.stats.con.consess)
                vector = zeros(1,length(matlabbatch{3}.spm.stats.con.consess{Nr_contrast}.tcon.weights)*2);
                vector(1:2:end) = matlabbatch{3}.spm.stats.con.consess{Nr_contrast}.tcon.weights;
                matlabbatch{3}.spm.stats.con.consess{Nr_contrast}.tcon.weights = vector;
            end
        elseif ( sum(matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs) == 2 )
            for Nr_contrast = 1:length(matlabbatch{3}.spm.stats.con.consess)
                vector = zeros(1,length(matlabbatch{3}.spm.stats.con.consess{Nr_contrast}.tcon.weights)*3);
                vector(1:3:end) = matlabbatch{3}.spm.stats.con.consess{Nr_contrast}.tcon.weights;
                matlabbatch{3}.spm.stats.con.consess{Nr_contrast}.tcon.weights = vector;
            end
        end

        % add the effect of interest contrast
        matlabbatch{3}.spm.stats.con.consess{7}.fcon.name    = 'EoI';
        matlabbatch{3}.spm.stats.con.consess{7}.fcon.weights = eye(length(matlabbatch{3}.spm.stats.con.consess{1}.tcon.weights));


        %% save and run the created matlabbatch

        % save the batch
        save(fullfile(folder,'firstlevel.mat'), 'matlabbatch');

        % check whether SPM already exists
        if ( exist(fullfile(folder,'SPM.mat'),'file') )
            delete(fullfile(folder,'SPM.mat'))
        end

        % run the batch
        if ( RunBatch == 1 )
            spm_jobman('run',matlabbatch)
        end

    end
end

end