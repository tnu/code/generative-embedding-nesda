function compress_DCM_results_MainExperiment(experiment,mask,SPM_prep,SPM_version,basis_set,randRestart,onCluster)
% Compresses the estimated Dynamic Causal Models (DCMs) for the FACES dataset 
% or the Tower of London (ToL) dataset of the NESDA study (Netherlands Study 
% on Depression and Social Anxiety). 
% 
% This function finds the DCM files that have been estimated using
% Variational Bayes under the Laplace approximation in SPM. The function
% compresses these files and moves them to a transfer folder.
% 
% Input:
%   model           -- model to be analyzed
%   experiment      -- (1) FACES dataset, (2) Tower of London dataset
%   mask            -- VoI extraction: (0) WFU atlas, (1) group coordinates, (2) NeuroSynth
%   SPM_prep        -- (0) NESDA pipeline (1) SPM preprocessing
%   restrict_sample -- (0) all subjects, (1) Schmaal et al. (2015)
%   SPM_version     -- SPM/DCM version to be used
%   include_DLPFC   -- (0) not include DLPFC, (1) include DLPFC
%   basis_set       -- basis set: (0) only HRF, (1) temporal derivative, (2) informed basis set
%   onCluster       -- operation system: (1) Euler, (0) local machine
%
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2016
% Copyright 2016 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% set the experiment
exp_folder = {'FACES/','ToL/'};

% set folders
SPM_folder          = {'','SPM_preprocessing/'};
mask_folder         = {'','GroupMask','NeuroSynth'}; 
basis_folder        = {'HRF','TD','IBS',''};

% set the subject folder
if ( mask == 0 || mask == 2 )
    subject_folder	= {'',''};
elseif ( mask == 1 )
    subject_folder	= {'AllSubject',''};
end


% define the SPM version
SPM_name = {'spm8','spm12','spm8v'};

% set the SPM path and the path to the experiment
if ( onCluster == 1 )
    addpath(['/cluster/home/stefanf/matlab/' SPM_name{SPM_version}]);
    foldername      = ['/cluster/scratch/stefanf/NESDA/' exp_folder{experiment}];
else
    addpath(['/Users/stefanf/Documents/SPM/' SPM_name{SPM_version}]);
    foldername      = ['/Users/stefanf/Documents/ETH Zurich/Projects/NESDA/' exp_folder{experiment}];
end

% define some variables
spm('Defaults','fMRI');

% get the subject list
Subject_List = [dir(fullfile(foldername,'1*')); dir(fullfile(foldername,'2*')); dir(fullfile(foldername,'3*'))];

% random restarts
randRestart_folder = {'','_RandRestart'};


% iterate over subjects
for s = 1:length(Subject_List)

    % get the subject name
    Subject = Subject_List(s).name;
    
    % display progress
    disp(['Compress & Copy: ' Subject])

    % define the folder where the SPM.mat (DCM regressors) is stored
    DCM_regressors_foldername = fullfile(foldername,Subject,['FirstLevel_DCM' randRestart_folder{randRestart+1}]);
        
    % check whether file exists and compress folder
    if ( exist(fullfile(DCM_regressors_foldername,SPM_name{SPM_version},SPM_folder{SPM_prep+1},basis_folder{basis_set+1},mask_folder{mask+1},subject_folder{restrict_sample+1},'DCM_NESDA_model_FF_INTER_001.mat'),'file') )
        
        % go to the directory
        cd(DCM_regressors_foldername)
        
        % compress the folder
        tar([SPM_name{SPM_version} '.tar.gz'],SPM_name{SPM_version});
        
    end
    
    % check whether compressed folder is present
    if ( exist(fullfile(DCM_regressors_foldername,[SPM_name{SPM_version} '.tar.gz']),'file') )
      
        % destination folder 
        to_folder = ['/cluster/scratch/stefanf/NESDA/Transfer/FACES/',Subject,['FirstLevel_DCM' randRestart_folder{randRestart+1}],basis_folder{basis_set+1},mask_folder{mask+1} '/'];
        
        % create the destination folder
        if ( ~exist(to_folder,'dir') )
            mkdir(to_folder)
        end
        
        % move the compressed folder
        movefile(fullfile(DCM_regressors_foldername,[SPM_name{SPM_version} '.tar.gz']),[to_folder, [SPM_name{SPM_version} '.tar.gz']])
        
    end
end

end