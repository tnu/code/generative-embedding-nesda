#! /bin/bash

set -e


# Load the matlab module
module load new matlab/9.1


# create logs folder
mkdir -p logs


# Get the demographic and clinical characteristics of the sample
bsub -W 1:00 -oo logs/sample_characteristics matlab -singleCompThread -r "output_subject_infos_MainExperiment([])"


# check whether groups differ with regard to head movement
bsub -W 1:00 -oo logs/motion_bias matlab -singleCompThread -r "compare_motion_parameters_MainExperiment([],1,1,2)"


# Get the center coordinates of the regions of interest
bsub -W 1:00 -J "job_getVOI" -o log_getVOI matlab -singleCompThread -r "get_VOI_coordinates_MainExperiment(1,0,2,1)"


# Plot all individual center coordinates and show that there are no significant differences
bsub -W 1:00 -w 'ended("job_getVOI")' -oo logs/VOI_coordinates matlab -singleCompThread -r "check_indvidual_coordinates_MainExperiment(1,0,2,1)"


# Get the variance explained by the DCMs
bsub -W 2:00 -J "job_DCM_getvar" -R "rusage[mem=7000]" -o log_DCMaccuracy matlab -singleCompThread -r "get_DCM_fit_accuracy_RandRest_MainExperiment(1,1,0,2)"


# display the variance explained by the DCMs
bsub -W 1:00 -w 'ended("job_DCM_getvar")' -oo logs/results_DCM_fit matlab -singleCompThread -r "check_DCM_fit_accuracy_MainExperiment([],1,1,0)"


# Perform Bayesian model selection amongst the competing DCMs
bsub -W 1:00 -w 'ended("job_DCM_getvar")' -o log_BMS matlab -singleCompThread -r "BMS_DCM_RandRest_MainExperiment(1,1,0,2)"


# Perform group-level inference on DCM parameters
bsub -W 1:00 -oo logs/results_DCM_group matlab -singleCompThread -r "group_analysis_DCM_MainExperiment([],0,1,[],1)"
